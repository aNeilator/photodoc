PhotoDoc
========

The aim of the project is to create a piece of software that recognises image in a way to help the user clean up their photo/picture folder.

The core objectives are to search through a directory and its sub-directories and display:

  - All photos that are exact duplicates and offer to delete them for the user.
  - Groups of photos that are extremely similar (i.e. taken in burst mode) and allow the user to select which ones should be kept
  - All photos that are deemed to be accidents or trash photos (i.e. floor shots, inside of pocket etc.)

Additionally, we aim to provide a service that will allow users to categorise their photos depending upon its content. At the moment some of the options that have been suggested are:

  - Photos with people in 
  - Photos of landscapes and nature
  - Photos of cities, building, industrial areas

these are only suggestions and may be changed, removed or have more added to them.


<h1>CODING STANDARDS</h1>

In order to make the code maintainable, scalable and clean the following standards must be followed when coding.

- variable names are to contain only alpha-numeric characters and must start with a lowercase letter, and be in camelCase
- constants must only contain alpha-numeric characters and be in capitals
- all code should have complementary tests written to ensure that all functionality is tested, for this we will be using JUnit
- before merging any work into the master branch, your work must be code reviewed by another member of the team and when it passes, merged by them
