package image.execution;


public interface ExportListener
{
	public void exp_finishedCopying(int success, int failed, String[] msg);
	public void exp_finishedMoving(int success, int failed, String[] msg);
	public void exp_finishedDeleting(int success, int failed, String[] msg);
}
