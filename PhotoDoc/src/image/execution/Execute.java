package image.execution;

import image.common.IV;
import image.common.ImageDetails;
import image.common.Normalize;
import image.common.Tuple;
import image.common.UserParameters;
import image.operation.EdgeDetection;
import image.operation.FaceDetection;
import image.operation.Feature;
import image.operation.Histogram;
import image.operation.MetaData;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import layout.PhDocListener;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import com.drew.lang.GeoLocation;

public class Execute implements ExecuteListener
{
	// Object used to store information about every image
	public static volatile Map<String, ImageDetails> imgDetails;
	//details about the current progress 
	public static volatile Progress progress;
	//current run number
	public static int runNumber = 0;
	//variables to control class lifecyce 
	public static volatile boolean _stop, _pause;

	//stores information regarding image operations
	private volatile Info info;
	//stores information regarding user settings
	private UserParameters userParams;
	//primary list of filenames of all images
	private ArrayList<String> imageFiles;

	//listeners for referring to other classes 
	private ExecuteListener execls;
	private PhDocListener layoutListener;

	//contains groups of similar/duplicates
	private volatile ArrayList<HashSet<Integer>> groupMembers_Sim;
	private volatile ArrayList<HashSet<Integer>> groupMembers_Dup;

	private long startTime;
	private int numThreads;

	//store mat for quick access
	private static volatile Mat[] histograms;
	private static volatile Mat[] normalised;
	private static volatile Mat[] featDiscriptors;

	//used during file output testing
	@SuppressWarnings("unused")
	private String str_Test;

	//----------------------------------Objects------------------------------------------

	private class Info
	{

		public volatile int totGroup_Sim = 0, totGroup_Dup = 0, totGroup_Event = 0, totGroup_Loc = 0;
		public volatile int totFound_Sim = 0, totFound_Dup = 0,
				totFound_Event = 0, totFound_Loc = 0,
				totFound_Trash = 0, totFound_Faces = 0;

		public boolean tasksOpenCV = true, tasksMetaData = true, taskFeature = false;
		public int imageTotal = 0;
	}

	public class Progress
	{

		public volatile double progMetaData = 0, progOpenCV = 0;
		public volatile int maxIteration = 0;
		public volatile int currNewMat = 0, currIter = 0;

		/**
		 * time in seconds
		 */
		public volatile float estimatedTime = 0;
		public volatile double temp = 0;

	}

	//----------------------------------start------------------------------------------

	/**
	 * Provide root folder. The class will get all image files and send them to technique files. 
	 * @param folder
	 * @param params = userParameters object with appropriate values.
	 */
	public Execute(PhDocListener layoutListener, UserParameters params) {
		this.layoutListener = layoutListener;
		userParams = params;

		init();

		for (File folders : userParams.folders) {
			populateFileList(folders);
		}

	}

	//initialise class variables
	private void init()
	{
		startTime = System.currentTimeMillis();

		execls = this;
		_stop = false;
		_pause = false;
		numThreads = 1;
		runNumber++;

		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		// UNCOMMENT FOR CREATING JAR AND COMMENT OUT ABOVE LINE! 
		/*
		String os = System.getProperty("os.name");
		try {

			if (os.equals("Mac OS X")) {
				System.load(System.getProperty("user.dir") + "/resources/osx/libopencv_java249.dylib");
			}
			else {
				if (System.getProperty("os.arch").equals("x86")) {
					System.load(System.getProperty("user.dir") + File.separator + "resources" + File.separator + "x86" + File.separator + "opencv_java249.dll");
				} else {
					System.load(System.getProperty("user.dir") + File.separator + "resources" + File.separator + "x64" + File.separator + "opencv_java249.dll");
				}
			}
		} catch (UnsatisfiedLinkError e) {
			String msg = "OpenCV library missing!";
			if (os.equals("Mac OS X")) {
				msg="/resources/osx/libopencv_java249.dylib missing!";
			}else {
				if (System.getProperty("os.arch").equals("x86")) {
					msg = "/resources/x86/opencv_java249.dll Missing!";
				} else {
					msg = "/resources/x64/opencv_java249.dll Missing!";
				}
			}
			IV.showErrorDialog(msg);
			System.err.println("DLLs not found");
			System.exit(0);
		}
		*/

		info = new Info();

		progress = null;
		progress = new Progress();

		imageFiles = new ArrayList<String>();

		imgDetails = null;
		imgDetails = Collections.synchronizedMap(new HashMap<String, ImageDetails>());

		groupMembers_Sim = new ArrayList<HashSet<Integer>>();
		groupMembers_Dup = new ArrayList<HashSet<Integer>>();

		populateTaskInfo();

		str_Test = "";
	}

	public void pause()
	{
		System.err.println("Pause Called.");
		_pause = true;
	}

	public void resume()
	{
		System.err.println("Resumed Called.");
		_pause = false;
	}

	public void stop()
	{
		System.err.println("Stop Called.");
		_stop = true;
	}

	//----------------------------------Class Methods------------------------------------------

	/**
	 * Returns an arraylist of fileNames, given a folder.
	 * @param folder
	 * @return
	 */
	private void populateFileList(File folder)
	{
		if (imageFiles == null) {
			imageFiles = new ArrayList<String>();
		}
		imageFiles.addAll(IV.getImageFilesList(folder));
		info.imageTotal = imageFiles.size();

		for (String name : imageFiles) {
			imgDetails.put(name,new ImageDetails());
		}

		int N = imageFiles.size();
		progress.maxIteration = (N * (N + 1)) / 2;

	}

	/**
	 * Call this to run the provided operation. 
	 * Each operation will send callbacks to implemented methods.
	 * eg. Layout.listener.isSimilarTo(path1, path2); <br>
	 * <b>Similar:</b> <br>
	 * Histogram		--> Feature Detection --x <br>
	 * <b>Duplicate:</b> <br>
	 * MetaData --> Background Deficiency --x <br>
	 * Histogram --x <br>
	 * <b>Trash:</b> <br>
	 * Histogram --x <br>
	 * EdgeDetection  --x <br>
	 * <b>Grouping:</b> <br>
	 * MetaData --x <br>
	 * FaceDetection  --x <br>
	 */
	public void run()
	{
		System.out.println();
		System.out.println("_________________________________________");
		System.out.println("                PHOTODOC v0.9            ");
		System.out.println("_________________________________________");
		System.out.println("Run Count          : " + runNumber);
		System.out.println("Total images       : " + info.imageTotal);
		System.out.println("Techniques         : " + getTasksAsString());

		//---------------------------------- META DATA------------------------------------------
		if (info.tasksMetaData) {
			Thread t = new Thread() {
				public void run()
				{
					(new MetaData(execls,new int[] { IV.ALL_MetaData, IV.METADATA },
							imageFiles)).run();
					return;
				}
			};
			t.start();
		} else {
			finished(IV.ALL_MetaData);
		}
		//----------------------------------Threading MATS------------------------------------------

		if (info.tasksOpenCV) {
			executeOpenCVTask();
		} else {
			finished(IV.ALL_OpenCV);
		}
	}

	/**
	 * Prepare threads and imin and imax values, then execute opencv tasks
	 */
	private void executeOpenCVTask()
	{

		histograms = new Mat[info.imageTotal];
		normalised = new Mat[info.imageTotal];
		if (userParams.doSimilar_feature) {
			featDiscriptors = new Mat[info.imageTotal];
		}

		//get optimum number of threads based on cores
		if (userParams.use_multiThreading == true) {
			numThreads = Runtime.getRuntime().availableProcessors();
		}

		double estComTime = info.imageTotal * 0.9 / numThreads;
		System.out.println("Max Iterations     : " + progress.maxIteration);
		System.out.println("Number of Threads  : " + numThreads);
		//only helpful to developer. accuracy dependent on host machine specs
		System.out.println("Est Completion time: " + (int) (estComTime + 1) + " secs");

		ExecutorService exec = Executors.newFixedThreadPool(numThreads);
		for (int i = 0; i < numThreads; i++) {

			exec.execute(new Runnable() {
				public void run()
				{
					final long threadId = Thread.currentThread().getId() % numThreads;

					final int iDiv = (imageFiles.size() / numThreads);
					final int imin = (int) (iDiv * threadId);
					final int imax = (threadId == numThreads - 1) ? imageFiles.size() : (imin + iDiv);

					System.out.println("Thread " + threadId + " - [" + imin + " to " + (imax - 1) + "]");
					runOpencvTasks(imin,imax,threadId);
				}
			});
		}

		exec.shutdown();

		//Wait for all threads to complete.
		try {
			exec.awaitTermination(Long.MAX_VALUE,TimeUnit.NANOSECONDS);
			execls.finished(IV.ALL_OpenCV);
		} catch (InterruptedException e) {}

	}

	/**
	 * runs all opencv operations. sends progress. checks lifecycle. records time.
	 * @param imin i start
	 * @param imax i end
	 * @param threadId thread id
	 */
	private void runOpencvTasks(final int imin, final int imax, final long threadId)
	{

		//[1]----------------------------------Progress Recorders------------------------------------------
		final int maxMatNew = info.taskFeature
				? (int) ((1800 + info.imageTotal) / (0.3 * info.imageTotal + 20))
				: (int) (85 / (1 + Math.pow(2,0.011 * info.imageTotal - 7)) + 10);
		final double changeS1 = maxMatNew / (double) info.imageTotal;
		final double changeS2 = (100 - maxMatNew) / (double)
				(info.tasksOpenCV ? progress.maxIteration : info.imageTotal);

		final int recordAt = (int) Math.sqrt(progress.maxIteration);
		final int recordAtFeat = (int) Math.pow(info.imageTotal,(info.taskFeature ? 1 : 1.3));

		//[2]----------------------------------Time Recorders------------------------------------------

		double time_tmp = 0, time_mat1 = 0, time_mat2 = 0;
		double time_sim = 0, time_trash = 0, time_feat = 0, time_face = 0;

		//[3]----------------------------------Face XML File------------------------------------------

		CascadeClassifier faceDetector = null;
		if (userParams.doFaces) {
			faceDetector = new CascadeClassifier(IV.RES + "xml/lbpcascade_frontalface.xml");
//			faceDetector = new CascadeClassifier("resources" + File.separator + "xml" + File.separator + "lbpcascade_frontalface.xml");
		}

		//[4]----------------------------------Begin Loops------------------------------------------

		int i = 0;
		int j = 0;

//		::::::::::::::::::::::::::::::::::::::LOOP i::::::::::::::::::::::::::::::::::::::::::

		iLoop: for (i = imin; i < imax; i++) {
			if (_stop) break iLoop;
			while (_pause) {
				try {
					if (_stop) break iLoop;
					Thread.sleep(1000);
					System.out.println("waiting... " + threadId);
				} catch (final InterruptedException e) {}
			}

			synchronized (progress) {
				progress.currIter++;
				progress.progOpenCV += changeS2;
				if (progress.currIter % recordAt == 0) {
					sendProgress(true);
				}
			}
			//[5]----------------------------------Retrieve Mat1,Hist1,Progress------------------------
			time_tmp = System.currentTimeMillis();

			try {
				synchronized (histograms) {

					if (histograms[i] == null) {
						histograms[i] = Histogram.createHistogram(imageFiles.get(i),false);
					}
				}
				synchronized (normalised) {

					if (normalised[i] == null) {
						normalised[i] = Highgui.imread(imageFiles.get(i),Highgui.CV_LOAD_IMAGE_GRAYSCALE);
						if (normalised[i].height() > IV.NORM_SIZE && normalised[i].width() > IV.NORM_SIZE) {
							Imgproc.resize(normalised[i],normalised[i],Normalize.getResize(normalised[i].height(),
									normalised[i].width()),0,0,Imgproc.INTER_LINEAR);
						}
						synchronized (progress) {
							progress.currNewMat++;
							progress.progOpenCV += changeS1;
							sendProgress(true);
						}
					}
				}
			} catch (final Exception e) {
				System.err.println("Error Creating Mat1: " + i);
				continue;
			}
			time_mat1 += (System.currentTimeMillis() - time_tmp);

			//::::::::::::::::::::::::::::::::::::::LOOP j ::::::::::::::::::::::::::::::::::::::::::

			if (userParams.doDuplicate || userParams.doSimilar) {

				for (j = i + 1; j < imageFiles.size(); j++) {

					//[7]----------------------------------LifeCycle------------------------------------------
					if (_stop) break iLoop;
					while (_pause) {
						try {
							if (_stop) break iLoop;
							Thread.sleep(1000);
							System.out.println("waiting... " + threadId);
						} catch (final InterruptedException e) {}
					}
					//[8]----------------------------------Retrieve Hist2------------------------------------------
					time_tmp = System.currentTimeMillis();
					try {
						synchronized (histograms) {
							if (histograms[j] == null) {
								histograms[j] = Histogram.createHistogram(imageFiles.get(j),false);
							}
						}
						//[9]----------------------------------Retrieve Mat2------------------------------------------
						synchronized (normalised) {

							if (normalised[j] == null) {
								normalised[j] = Highgui.imread(imageFiles.get(j),Highgui.CV_LOAD_IMAGE_GRAYSCALE);
								if (normalised[j].height() > IV.NORM_SIZE
										&& normalised[j].width() > IV.NORM_SIZE) {
									Imgproc.resize(normalised[j],normalised[j],Normalize.getResize(
											normalised[j].height(),normalised[j].width()),
											0,0,Imgproc.INTER_LINEAR);
								}
								synchronized (progress) {
									progress.currNewMat++;
									progress.progOpenCV += changeS1;
									sendProgress(true);
								}
							}
						}
					} catch (final Exception e) {
						System.err.println("Error Creating Mat2: " + j);
						continue;
					}
					time_mat2 += (System.currentTimeMillis() - time_tmp);

					//[10]----------------------------------Similar/Dup: HIST/BACK/FEAT------------------------
					time_tmp = System.currentTimeMillis();

					int iGroup = imgDetails.get(imageFiles.get(i)).group_Sim;
					int jGroup = imgDetails.get(imageFiles.get(j)).group_Sim;
					final boolean checkSimilar = userParams.doSimilar &&
							((iGroup == -1 || jGroup == -1) || (iGroup != jGroup));

					iGroup = imgDetails.get(imageFiles.get(i)).group_Dup;
					jGroup = imgDetails.get(imageFiles.get(j)).group_Dup;
					final boolean checkDup = userParams.doDuplicate
							&& ((iGroup == -1 || jGroup == -1) || (iGroup != jGroup));

					//[11]----------------------------------Hist Dup/Sim------------------------------------------

					final double bhatta = Imgproc.compareHist(histograms[i],histograms[j],Imgproc.CV_COMP_BHATTACHARYYA);

					final double simCertainty = Histogram.checkSimilarAndDuplicates(
							bhatta,i,j,checkDup,checkSimilar,execls);

					time_sim += (System.currentTimeMillis() - time_tmp);
					//[12]----------------------------------Feature------------------------------------------

					if (simCertainty < IV.CRThresh_Low && checkSimilar && info.taskFeature) {
						time_tmp = System.currentTimeMillis();
						synchronized (featDiscriptors) {
							if (featDiscriptors[i] == null) {
								featDiscriptors[i] = Feature.sift(normalised[i].clone());
							}
							if (featDiscriptors[j] == null) {
								featDiscriptors[j] = Feature.sift(normalised[j].clone());
							}
						}
						Feature.checkSimilar(featDiscriptors[i],featDiscriptors[j],i,j,execls);
						time_feat += (System.currentTimeMillis() - time_tmp);
					}

					//[13]----------------------------------Update Progress-----------------------------------------
					synchronized (progress) {
						progress.currIter++;
						progress.progOpenCV += changeS2;
						setProgress(IV.SIMILAR,progress.progOpenCV);
						setProgress(IV.DUPLICATE,progress.progOpenCV);
						if (progress.currIter % recordAt == 0
								&& progress.currNewMat <= info.imageTotal - 1) {
							sendProgress(true);
						}
						else if (progress.currIter % recordAtFeat == 0) {
							sendProgress(true);
						}

					}
					//----------------------------------------------------------------------------

				}//for j
			}//end sim / dup check
			if (_stop) break iLoop;
			//[14]----------------------------------Trash------------------------------------------

			time_tmp = System.currentTimeMillis();
			if (userParams.doTrash) {

				Mat edges = new Mat();
				//Imgproc.THRESH_OTSU
				Imgproc.Canny(normalised[i],edges,EdgeDetection.lowThresh,
						EdgeDetection.lowThresh * EdgeDetection.kernel_size,
						EdgeDetection.ratio,true);
				final int countP = Core.countNonZero(edges);

				final boolean trashFound = EdgeDetection.checkTrash(
						countP,edges.width(),edges.height(),i,execls);
				edges = null;

				if (!trashFound) {
					Histogram.doTrash(i,histograms[i],execls);
				}
				setProgress(IV.TRASH,(double) (100 / info.imageTotal));
			}
			time_trash += (System.currentTimeMillis() - time_tmp);

			//[15]----------------------------------Face--------------------------------------

			time_tmp = System.currentTimeMillis();
			if (userParams.doFaces) {
				FaceDetection.facedetector(normalised[i],i,faceDetector,execls);
				setProgress(IV.GROUP_FACE,(double) (100 / info.imageTotal));
			}
			time_face += (System.currentTimeMillis() - time_tmp);

			//[16]----------------------------------All End------------------------------------------

			if (progress.currIter == progress.maxIteration) {
				sendProgress(true);
				System.out.printf("--Mat1  : %.2f s\r",time_mat1 / 1000);
				System.out.printf("--Mat2  : %.2f s\r",time_mat2 / 1000);
				System.out.printf("--D/Sim : %.2f s\r",time_sim / 1000);
				System.out.printf("--Feat  : %.2f s\r",time_feat / 1000);
				System.out.printf("--Trash : %.2f s\r",time_trash / 1000);
				System.out.printf("--Face  : %.2f s\r",time_face / 1000);
			}

		}//end i
	}
	//----------------------------------Interface Callbacks-------------------------

	public UserParameters getUserParams()
	{
		return userParams;
	}

	public void sendProgress(final boolean openCVOnly)
	{
		//for recording and storing times in a file
//		str_Test += (System.currentTimeMillis() - startTime) + "," + progress.progOpenCV + "\r";
		if (openCVOnly) {
			//		System.out.println(" c: " + progress.currIter + " new: " + progress.currNewMat);
			System.out.printf("[%.2f %%] \r",(progress.progOpenCV));
			if (userParams.export_showResults) {
				layoutListener.progressUpdate(progress.progOpenCV);

			}
		} else {
			System.out.printf("[%.2f %%] \r",(progress.progMetaData));
			if (userParams.export_showResults) {
				layoutListener.progressUpdate(progress.progMetaData);
			}
		}

	}

	public void setProgress(final int identifier, final double percentage)
	{
		synchronized (progress) {
			switch (identifier) {
			case IV.ALL_MetaData:
				progress.progMetaData = percentage;
				break;
			}
			if (info.tasksOpenCV == false) {
				sendProgress(false);
			}
		}
	}

	@Override
	public void finished(final int identifier)
	{

		final int id0 = identifier;
		switch (id0) {
		case IV.ALL_MetaData:
			progress.progMetaData = 100;
			if (userParams.export_showResults) {
				if (userParams.doEventTime)
					layoutListener.finishedEventTimeSearch();
				if (userParams.doLocation)
					layoutListener.finishedLocationSearch();
				if (userParams.doDuplicate && !info.tasksOpenCV) // && !info.tasksOpenCV)
					layoutListener.finishedDuplicateSearch();
			}
			break;

		case IV.ALL_OpenCV:
			progress.progOpenCV = 100;
			if (userParams.export_showResults) {
				if (userParams.doSimilar)
					layoutListener.finishedSimilarSearch();
				if (userParams.doDuplicate)
					layoutListener.finishedDuplicateSearch();
				if (userParams.doTrash)
					layoutListener.finishedTrashSearch();
				if (userParams.doFaces)
					layoutListener.finishedFacesSearch();
			}
			break;
		}

		//both finished:
		if (progress.progOpenCV >= 100 && progress.progMetaData >= 100) {
			System.out.println(getSummary(false));
			System.out.println("_________________________________________");
			System.out.println("            Run " + runNumber + " finished   ");
			System.out.println("_________________________________________");

			// used to output file with given text
			// IV.writeStrToFile(str_Test,"ProgText" + runNumber + ".csv");
			if (userParams.export_showResults) {
				new Thread() {
					public void run()
					{
						layoutListener.finished_All();
					}
				}.start();
			}
		}

	}

	@Override
	public void imageDuplicateTo(int[] identifier, int i, int j, double certainty)
	{
		if (certainty > IV.CRThresh_High) {
			//----------------------------------new storage------------------------------------------
			int iGroup = imgDetails.get(imageFiles.get(i)).group_Dup;
			int jGroup = imgDetails.get(imageFiles.get(j)).group_Dup;

			//one of them zero. 1) assign non zero or 2)assign new group
			if (iGroup == -1 || jGroup == -1) {
				if (iGroup == -1 && jGroup != -1) {
					addToGroup(IV.DUPLICATE,i,jGroup);
				}
				else if (jGroup == -1 && iGroup != -1) {
					addToGroup(IV.DUPLICATE,j,iGroup);
				}
				else if (iGroup == -1 && jGroup == -1) {
					addNewGroup(IV.DUPLICATE,i,j);
				}
			} else if (iGroup != jGroup) {
				mergeGroups(IV.DUPLICATE,i,j,iGroup,jGroup);
			}
			imgDetails.get(imageFiles.get(i)).cert_Dup = certainty;
			imgDetails.get(imageFiles.get(j)).cert_Dup = certainty;
			//----------------------------------------------------------------------------
//			layoutListener.imageDuplicateTo(imageFiles.get(i),imageFiles.get(j));
		}
	}

	@Override
	public void imageSimilarTo(int[] identifier, int i, int j, double certainty)
	{
		if (certainty > IV.CRThresh_Low) {
			synchronized (groupMembers_Sim) {
				//----------------------------------new storage------------------------------------------
				int iGroup = imgDetails.get(imageFiles.get(i)).group_Sim;
				int jGroup = imgDetails.get(imageFiles.get(j)).group_Sim;

				//one of them zero. 1) assign non zero or 2)assign new group
				if ((iGroup == -1 || jGroup == -1)) {
					if (iGroup == -1 && jGroup != -1) {
						addToGroup(IV.SIMILAR,i,jGroup);
					}
					else if (jGroup == -1 && iGroup != -1) {
						addToGroup(IV.SIMILAR,j,iGroup);
					}
					else if (iGroup == -1 && jGroup == -1) {
						addNewGroup(IV.SIMILAR,i,j);
					}
					setCerts(i,j,certainty);
				} else if (iGroup != jGroup) {
					double iCert = imgDetails.get(imageFiles.get(i)).cert_Sim;
					double jCert = imgDetails.get(imageFiles.get(j)).cert_Sim;
					//only add this file
					if (certainty > IV.CRThresh_High) {
						if (userParams.use_transitiveGrouping == true) {
							mergeGroups(IV.SIMILAR,i,j,iGroup,jGroup);
							setCerts(i,j,certainty);
						}
					}
					else if (iCert < certainty || jCert < certainty) {
						if (userParams.use_transitiveGrouping == true) {
							transferToGroup(IV.SIMILAR,i,j,iGroup,jGroup,certainty);
							setCerts(i,j,certainty);
						}
					}
				}

			}
		}
	}

	@Override
	public void imageTrash(int[] identifier, int i, double certainty)
	{
		if (certainty > IV.CRThresh_High) {
			imgDetails.get(imageFiles.get(i)).cert_Trash = certainty;
			info.totFound_Trash++;
		}
	}

	@Override
	public void faceFound(int[] identifier, int i, int numberOfFaces)
	{
		if (numberOfFaces >= 1) {
			imgDetails.get(imageFiles.get(i)).info_Faces = numberOfFaces;
			info.totFound_Faces++;
		}
	}

	@Override
	public void finished_Event(HashMap<Tuple<Date, Date>, LinkedList<Integer>> sameTimePeriod)
	{
		try {
			//given the object returned from metadata, add them appropriately in the main image object
			for (Entry<Tuple<Date, Date>, LinkedList<Integer>> entry : sameTimePeriod.entrySet()) {
				if (entry.getValue().size() == 1) {
					imgDetails.get(imageFiles.get(entry.getValue().get(0))).info_Event = entry.getKey();
					imgDetails.get(imageFiles.get(entry.getValue().get(0))).group_Event = -1;
				} else {
					for (int imgIndex : entry.getValue()) {
						imgDetails.get(imageFiles.get(imgIndex)).info_Event = entry.getKey();
						imgDetails.get(imageFiles.get(imgIndex)).group_Event = info.totGroup_Event;
						info.totFound_Event++;
					}
					info.totGroup_Event++;
				}
			}
		} catch (Exception e) {}
	}

	@Override
	public void finished_Location(HashMap<GeoLocation, LinkedList<Integer>> sameLocation)
	{
		try {
			//given the object returned from metadata, add them appropriately in the main image object
			for (Entry<GeoLocation, LinkedList<Integer>> entry : sameLocation.entrySet()) {
				for (int imgIndex : entry.getValue()) {
					imgDetails.get(imageFiles.get(imgIndex)).info_Loc = entry.getKey();
					imgDetails.get(imageFiles.get(imgIndex)).group_Loc = info.totGroup_Loc;
					info.totFound_Loc++;
				}
				info.totGroup_Loc++;
			}
		} catch (Exception e) {}
	}
	//----------------------------------clean-------------------------

	/**
	 * create new group with given files
	 * @param type IV.SIMILAR or IV.DUPLICATE
	 * @param i index of file1
	 * @param j index of file2
	 */
	private void addNewGroup(int type, int i, int j)
	{
		HashSet<Integer> hashSet = new HashSet<Integer>();
		hashSet.add(j);
		hashSet.add(i);

		if (type == IV.SIMILAR) {
			groupMembers_Sim.add(hashSet);

			int index = groupMembers_Sim.indexOf(hashSet);
			imgDetails.get(imageFiles.get(i)).group_Sim = index;
			imgDetails.get(imageFiles.get(j)).group_Sim = index;

			info.totFound_Sim += 2;
			info.totGroup_Sim++;
		} else if (type == IV.DUPLICATE) {
			groupMembers_Dup.add(hashSet);

			int index = groupMembers_Dup.indexOf(hashSet);
			imgDetails.get(imageFiles.get(i)).group_Dup = index;
			imgDetails.get(imageFiles.get(j)).group_Dup = index;
			info.totFound_Dup += 2;
			info.totGroup_Dup++;
		}
	}

	/**
	 * add the imageToAdd to group
	 * @param type IV.SIMILAR or IV.DUPLICATE
	 * @param imageToAdd index of file
	 * @param groupID if of group
	 */
	private void addToGroup(int type, int imageToAdd, int groupID)
	{
		if (type == IV.SIMILAR) {
			groupMembers_Sim.get(groupID).add(imageToAdd);
			imgDetails.get(imageFiles.get(imageToAdd)).group_Sim = groupID;
			info.totFound_Sim++;

		} else if (type == IV.DUPLICATE) {
			groupMembers_Dup.get(groupID).add(imageToAdd);
			imgDetails.get(imageFiles.get(imageToAdd)).group_Dup = groupID;
			info.totFound_Dup++;
		}
	}

	/**
	 * transfer one file to another file's group. the file with lower 
	 * current certainty is the one that gets transferred
	 * @param type IV.SIMILAR or IV.DUPLICATE
	 * @param i index of file1
	 * @param j index of file2
	 * @param iGroup id of i's group
	 * @param jGroup if of j's group
	 * @param certainty the certainty just calculated  for these files
	 */
	private void transferToGroup(int type, int i, int j, int iGroup, int jGroup, double certainty)
	{
		if (type == IV.SIMILAR) {
			double iCert = imgDetails.get(imageFiles.get(i)).cert_Sim;
			double jCert = imgDetails.get(imageFiles.get(j)).cert_Sim;
			//only copy j if j has low certainty to begin with an image in that group
			if (jCert < iCert) {
				//copy j to i's group, remove j from group set
				imgDetails.get(imageFiles.get(j)).group_Sim = iGroup;
				if (groupMembers_Sim.get(jGroup).size() <= 2) {
					mergeGroups(IV.SIMILAR,i,j,iGroup,jGroup);
				} else {
					groupMembers_Sim.get(jGroup).remove(j);
					groupMembers_Sim.get(iGroup).add(j);
				}
			} else if (iCert < jCert) {
				imgDetails.get(imageFiles.get(i)).group_Sim = jGroup;
				if (groupMembers_Sim.get(iGroup).size() <= 2) {
					mergeGroups(IV.SIMILAR,i,j,iGroup,jGroup);
				} else {
					groupMembers_Sim.get(iGroup).remove(i);
					groupMembers_Sim.get(jGroup).add(i);
				}
			}
//			RunInfo.last_Sim_Group--;
		}
	}

	/**
	 * Merge the groups of both file.
	 * @param type IV.SIMILAR or IV.DUPLICATE
	 * @param i index of file1
	 * @param j index of file2
	 * @param iGroup id of i's group
	 * @param jGroup if of j's group
	 */
	private void mergeGroups(int type, int i, int j, int iGroup, int jGroup)
	{
		if (type == IV.SIMILAR) {
			int iGroupSize = groupMembers_Sim.get(iGroup).size();
			int jGroupSize = groupMembers_Sim.get(jGroup).size();
			if (iGroupSize >= jGroupSize) {
				//update values for each image with jGroup to iGroup
				for (int imageIndices : groupMembers_Sim.get(jGroup)) {
					imgDetails.get(imageFiles.get(imageIndices)).group_Sim = iGroup;
				}
				//now copy these to iGroup
				groupMembers_Sim.get(iGroup).addAll(groupMembers_Sim.get(jGroup));
				//now remove jGroup set
				groupMembers_Sim.set(jGroup,new HashSet<Integer>());
			} else {
				for (int imageIndices : groupMembers_Sim.get(iGroup)) {
					imgDetails.get(imageFiles.get(imageIndices)).group_Sim = jGroup;
				}
				groupMembers_Sim.get(jGroup).addAll(groupMembers_Sim.get(iGroup));
				groupMembers_Sim.set(iGroup,new HashSet<Integer>());
			}
			info.totGroup_Sim--;
		}
		else if (type == IV.DUPLICATE) {
			int iGroupSize = groupMembers_Dup.get(iGroup).size();
			int jGroupSize = groupMembers_Dup.get(jGroup).size();
			if (iGroupSize >= jGroupSize) {
				//update values for each image with jGroup to iGroup
				for (int imageIndices : groupMembers_Dup.get(jGroup)) {
					imgDetails.get(imageFiles.get(imageIndices)).group_Dup = iGroup;
				}
				//now copy these to iGroup
				groupMembers_Dup.get(iGroup).addAll(groupMembers_Dup.get(jGroup));
				//now remove jGroup set
				groupMembers_Dup.set(jGroup,new HashSet<Integer>());
			} else {
				for (int imageIndices : groupMembers_Dup.get(iGroup)) {
					imgDetails.get(imageFiles.get(imageIndices)).group_Dup = jGroup;
				}
				groupMembers_Dup.get(jGroup).addAll(groupMembers_Dup.get(iGroup));
				groupMembers_Dup.set(iGroup,new HashSet<Integer>());
			}
			info.totGroup_Dup--;
		}

	}

	/**
	 * sets the certainty for given file indices if current certainty is lower.
	 * @param i
	 * @param j
	 * @param certainty
	 */
	private void setCerts(int i, int j, double certainty)
	{
		//Store the largest certainty
		if (imgDetails.get(imageFiles.get(i)).cert_Sim < certainty) {
			imgDetails.get(imageFiles.get(i)).cert_Sim = certainty;
		}
		if (imgDetails.get(imageFiles.get(j)).cert_Sim < certainty) {
			imgDetails.get(imageFiles.get(j)).cert_Sim = certainty;
		}
	}

	//----------------------------------Prints------------------------------------------

	private String getSummary(boolean printDetail)
	{
		String lb = "\r";
		String sum = "";

		sum += "------------Results Summary---------------" + lb;

		sum += "Similar   : [" + info.totGroup_Sim + "] "
				+ info.totFound_Sim + "/" + info.imageTotal + lb;

		sum += "Duplicate : [" + info.totGroup_Dup + "] "
				+ info.totFound_Dup + "/" + info.imageTotal + lb;

		sum += "EventTime : [" + info.totGroup_Event + "] "
				+ info.totFound_Event + "/" + info.imageTotal + lb;

		sum += "Location  : [" + info.totGroup_Loc + "] "
				+ info.totFound_Loc + "/" + info.imageTotal + lb;

		sum += "Trash     : " + info.totFound_Trash + "/" + info.imageTotal + lb;

		sum += "Faces     : " + info.totFound_Faces + "/" + info.imageTotal + lb;

		double endTime = (double) (System.currentTimeMillis() - startTime) / 1000;

		sum += "---------Time Taken: " + endTime + "s---------------";
		return sum;
	}

	private String getTasksAsString()
	{
		String str = "[-";
		if (userParams.doDuplicate) str += (userParams.doDuplicateOpenCV) ? "D(Op)-" : "D-";
		if (userParams.doSimilar) str += (userParams.doSimilar_feature) ? "S(Ft)-" : "S-";
		if (userParams.doTrash) str += "T-";
		if (userParams.doFaces) str += "F-";
		if (userParams.doEventTime) str += "E-";
		if (userParams.doLocation) str += "L-";
		str += "]";
		return str;
	}

	/**
	 * used to populate class info 
	 */
	private void populateTaskInfo()
	{
		//----------------------------------For test------------------------------------------

		//		userParams.doSimilar = true;
		//		userParams.doDuplicate = false;
		//		userParams.doFaces = false;
		//		userParams.doTrash = false;
		//		userParams.doEventTime = false;
		//		userParams.doLocation = false;
		//		
		//		userParams.doSimilar_feature=true;
		//		userParams.doDuplicateOpenCV=true;
		//		
		//		userParams.export_showResults = false;
		//----------------------------------------------------------------------------

		if ((userParams.doSimilar || userParams.doTrash || userParams.doFaces) ||
				(userParams.doDuplicateOpenCV && userParams.doDuplicate)) {
			info.tasksOpenCV = true;
		} else {
			info.tasksOpenCV = false;
		}

		if (userParams.doEventTime || userParams.doLocation || userParams.doDuplicate) {
			info.tasksMetaData = true;
		} else {
			info.tasksMetaData = false;
		}

		info.taskFeature = (userParams.doSimilar || userParams.doDuplicate)
				? userParams.doSimilar_feature : false;
	}

}
