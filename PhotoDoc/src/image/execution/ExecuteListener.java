package image.execution;

import image.common.Tuple;
import image.common.UserParameters;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;

import com.drew.lang.GeoLocation;

public interface ExecuteListener
{

	public UserParameters getUserParams();
	
	public void setProgress(int identifier, double percentage);
	/**
	 * get image under Duplicate everytime a pair is found. 
	 */
	public void imageDuplicateTo(int[] identifier, int image, int imageTo, double certainty);

	/**
	 * get image under similar everytime a pair is found. 
	 */
	public void imageSimilarTo(int[] identifier, int image, int imageTo, double certainty);

	/**
	 * get image under trash everytime one is found.
	 */
	public void imageTrash(int[] identifier, int image, double certainty);

	public void faceFound(int[] identifier, int image, int numberOfFaces);
	
	/**
	 * Call at the end when nothing left to do.
	 * @param identifier
	 */
	public void finished(int identifier);

	public void finished_Location(HashMap<GeoLocation, LinkedList<Integer>> sameLocation);

	public void finished_Event(HashMap<Tuple<Date, Date>, LinkedList<Integer>> sameTimePeriod);

}
