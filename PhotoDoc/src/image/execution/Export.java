package image.execution;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import image.common.IV;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import com.apple.eio.FileManager;
import com.sun.jna.platform.FileUtils;

public class Export
{

	private static int empFoldersDeleted;
	
	/**
	 * Moves the files to the Trash/Recycle Bin depending on the OS.
	 * @param fileList
	 */
	public static void Delete(ArrayList<String> fileList, ExportListener expol)
	{
		// Get OS name.
		String os = System.getProperty("os.name");

		int passed = 0;
		// OSX Only System Call! WILL REQUIRE OS X VERSION OF JAVA!
		if (os.equals("Mac OS X")) {
			for (String name : fileList) {
				File file = new File(name);
				try {
					// Use OS X Java call to move file to trash.
					FileManager.moveToTrash(file);
					passed++;
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
		// Assume Windows
		else {
			FileUtils win = new com.sun.jna.platform.win32.W32FileUtils();
			ArrayList<File> files = new ArrayList<File>();
			for (String name : fileList) {
				files.add(new File(name));
			}
			try {
				// Use JNA to move file to recycle bin.
				win.moveToTrash(files.toArray(new File[files.size()]));
				passed++;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		expol.exp_finishedDeleting(passed,(fileList.size() - passed),null);
	}

	/**
	 * Moves the files to the specified directory.
	 * @param fileList
	 * @param directory
	 */
	public static void Move(ArrayList<String> fileList, String directory, ExportListener expol)
	{
		int passed = 0;
		
		// Record the new paths of the moved files.
		ArrayList<String> updatedPaths = new ArrayList<String>();
		File dir = new File(directory);
		// Check if directory already exists.
		if (!dir.exists()) {
			dir.mkdirs();
		}

		for (String name : fileList) {
			Path exportFrom = Paths.get(name);
			Path exportTo = Paths.get(directory,exportFrom.getFileName().toString());
			try {
				// Check if file name is already used.
				Files.move(exportFrom,exportTo,REPLACE_EXISTING);
				updatedPaths.add(name);
				passed++;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		expol.exp_finishedMoving(passed,(fileList.size() - passed),null);
	}

	/**
	 * Copies the files to the specified directory.
	 * @param fileList
	 * @param directory
	 */
	public static void Copy(ArrayList<String> fileList, String directory, ExportListener expol)
	{
		int passed = 0;
		// Check if directory already exists.
		File dir = new File(directory);
		if (!dir.exists()) {
			dir.mkdirs();
		}

		for (String name : fileList) {
			Path exportFrom = Paths.get(name);
			Path exportTo = Paths.get(directory,exportFrom.getFileName().toString());
			try {
				int i = 1;
				// Check if file name is already used.
				if(exportTo.toFile().exists()) {
					// Append numbers to the end. Increase number until unused name is found.
					while(exportTo.toFile().exists()) {
						String newPath = new String(exportFrom.getFileName().toString());
						String ext = newPath.substring(newPath.lastIndexOf("."), newPath.length());
						newPath = newPath.substring(0, newPath.lastIndexOf("."));
						newPath = newPath.concat("_" + i + ext);
						exportTo = Paths.get(directory,newPath);
						i++;
					}
					Files.copy(exportFrom,exportTo,REPLACE_EXISTING);
					passed++;
				} else {
					Files.copy(exportFrom,exportTo,REPLACE_EXISTING);
					passed++;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		expol.exp_finishedCopying(passed,(fileList.size() - passed),null);
	}

	/**
	 * Deletes empty folders from the given array of folders.
	 * @param folders
	 */
	public static void DeleteEmptyFolders(File[] folders, ExportListener expol)
	{
		empFoldersDeleted = 0;
		for (File folder : folders) {
			if (checkAndDeleteEmptyFolders(folder)) {
				if(Export.Delete(folder)) {
				}
			}
		}
		expol.exp_finishedDeleting(empFoldersDeleted,0,null);
	}

	/**
	 * Moves the specified file to trash/recycle bin.
	 * @param file
	 * @return True if successful.
	 */
	private static boolean Delete(File file)
	{
		// Get OS name.
		String os = System.getProperty("os.name");

		if (os.equals("Mac OS X")) {
			try {
				// Use OS X Java call to move file to trash.
				FileManager.moveToTrash(file);
				empFoldersDeleted++;
				return true;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		// Assume Windows
		else {
			FileUtils win = new com.sun.jna.platform.win32.W32FileUtils();

			try {
				// Use JNA to move file to trash.
				win.moveToTrash(new File[] { file });
				empFoldersDeleted++;
				return true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	/**
	 * Checks if folder is empty and deletes if so.
	 * @param folder
	 * @return True if folder is empty.
	 */
	private static boolean checkAndDeleteEmptyFolders(File folder)
	{
		boolean empty = true;

		for (File file : folder.listFiles()) {
			if (file.isDirectory()) {
				if (checkAndDeleteEmptyFolders(file)) {
					if(Export.Delete(file)) {
					}
				} else {
					empty = false;
				}
			}
			if (!file.isDirectory() && !file.getName().equals("Thumbs.db")) {
				empty = false;
			}
		}

		return empty;
	}

	/**
	 * Finds the string of the type of pane.
	 * @param pane
	 * @return String corresponding to the pane.
	 */
	public static String getFolderName(int pane)
	{
		String type = "";
		switch (pane) {
		case IV.SIMILAR:
			type = "similar_images";
			break;
		case IV.GROUP_EVENT:
			type = "event_images";
			break;
		case IV.GROUP_LOC:
			type = "location_images";
			break;
		case IV.DUPLICATE:
			type = "duplicate_images";
			break;
		case IV.TRASH:
			type = "trash_images";
			break;
		case IV.GROUP_FACE:
			type = "faces_images";
			break;
		}
		return type;
	}

}
