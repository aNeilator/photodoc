package image.operation;

import image.common.IV;
import image.execution.ExecuteListener;

public class EdgeDetection
{
	// this can be changed decided by the user
	public final static int lowThresh = 20;
	public final static int ratio = 3;
	public final static int kernel_size = 3;

	/**
	 * given number of pixels with edges, uses predefined thresholds to determine if image is trash or not
	 * @param countP - number of pixel
	 * @param width - width of image
	 * @param height - height of image 
	 * @param imageIndex - index of image
	 * @param execls - ExecuteListener
	 * @return returns true if image classified as trash.
	 */
	public static boolean checkTrash(int countP, int width, int height, int imageIndex, ExecuteListener execls)
	{
		double maxPixels = (width * height);
		double certainty = (maxPixels - countP) / maxPixels;

		//high certainty mean less edges default: 0.94
		if (certainty > (1 - (0.06 * execls.getUserParams().t_Trash))) {
			execls.imageTrash(new int[] { IV.TRASH, IV.EDGE_DETECTION },imageIndex,certainty);
			return true;
		}
		return false;
	}
}
