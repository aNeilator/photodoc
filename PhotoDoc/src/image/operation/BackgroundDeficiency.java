package image.operation;

import image.common.IV;
import image.execution.ExecuteListener;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

public class BackgroundDeficiency
{
	
	/**
	 * use background deficiency technique to determine duplicate files. returns the output back to 
	 * ExecuteListener
	 * @param model - image 1
	 * @param scene - image 2
	 * @param i - index of image 1
	 * @param j - index of image 2
	 * @param execls ExecuteListener reference.
	 */
	public static void useBackgroundDifference(final Mat model , final Mat scene, final int i, final int j, final ExecuteListener execls)
	{
		try {

			if (model.cols() == scene.cols() && model.rows() == scene.rows()) {
				final Mat diff = new Mat();
				Core.absdiff(model,scene,diff);
				Imgproc.threshold(diff,diff,15,255,Imgproc.THRESH_BINARY);
				final int distortion = Core.countNonZero(diff);
				if (distortion==0) {
					execls.imageSimilarTo(new int[] {IV.DUPLICATE , IV.BACKGROUND_DEF},i ,j,1);
				}
			}
		}
		catch (final Exception e) {
			System.err.println("BACKDEF: error for i: "+i+" ,j:"+j);
		}
	}

}
