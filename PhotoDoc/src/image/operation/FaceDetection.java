package image.operation;

import image.common.IV;
import image.execution.ExecuteListener;

import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.objdetect.CascadeClassifier;

public class FaceDetection
{

	/**
	 * Uses the classifer sent to count the number of faces present in an image
	 * @param mat1 - image matrix
	 * @param i - image index
	 * @param faceDetector - classifier
	 * @param execls - ExecuteListener
	 * @return the number of faces found.
	 */
	public static int facedetector(final Mat mat1, final int i, final CascadeClassifier faceDetector, final ExecuteListener execls)
	{
		int numFaces = 0;
		try {

			final MatOfRect faceDetections = new MatOfRect();
			faceDetector.detectMultiScale(mat1,faceDetections);
			numFaces = faceDetections.toArray().length;
			execls.faceFound(new int[] { IV.GROUP_FACE, IV.FACE_DETECTION },i,numFaces);
		} catch (final Exception e) {
			System.err.println("FACE: error " + e.getMessage() +" on "+i);
		}
		return numFaces;
	}

}
