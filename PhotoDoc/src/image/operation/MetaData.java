package image.operation;

import image.common.IV;
import image.common.Tuple;
import image.execution.Execute;
import image.execution.ExecuteListener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.lang.GeoLocation;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.drew.metadata.exif.GpsDirectory;

public class MetaData
{
	
	//sent by execute but can be determined
	private int[] identifier;
	private ArrayList<String> imageFiles;

	//Objects to store each of the 3 operations
	private HashMap<GeoLocation, LinkedList<Integer>> sameLocation;
	private HashMap<Tuple<Date, Date>, LinkedList<Integer>> sameTimePeriod;
	private HashMap<Date, Integer> duplicateMap;

	//used to send execute information back
	private ExecuteListener execls;
	//threshold from user settings
	private double thresh_Time, thresh_Loc;

	@SuppressWarnings("unused")
	private double ts = 0, t_date = 0, t_dup = 0, t_gps = 0;

	public MetaData(ExecuteListener execls, int[] identifier, ArrayList<String> imageFiles) {
		this.execls = execls;
		this.identifier = identifier;
		this.imageFiles = imageFiles;

		thresh_Time = execls.getUserParams().t_Time;
		thresh_Loc = Math.pow(10,execls.getUserParams().t_Location) * 50;

		sameLocation = new HashMap<GeoLocation, LinkedList<Integer>>();
		sameTimePeriod = new HashMap<Tuple<Date, Date>, LinkedList<Integer>>();
		duplicateMap = new HashMap<Date, Integer>();
	}

	public void run()
	{

		int size = imageFiles.size();
		double sqrt = (int) Math.sqrt(size);
		double iter = 100 / sqrt;
		double prog = 0;

		//main loop
		for (int i = 0; i < size; i++) {
			// if stop called, return immidiately. 
			if (Execute._stop) break;
			readMetaData(i);
			if (i % sqrt == 0) {
				//send progress in case opencv tasks are not selected.
				execls.setProgress(IV.ALL_MetaData,prog);
				prog += iter;
			}
		}

		//send to execute event data
		if (execls.getUserParams().doEventTime) {
			execls.finished_Event(sameTimePeriod);
		}
		
		//send to execute location data
		if (execls.getUserParams().doLocation) {
			execls.finished_Location(sameLocation);
		}
		
		//finalise everything
		execls.finished(IV.ALL_MetaData);
	}

	public void readMetaData(int imgIndex)
	{
		String fileName = imageFiles.get(imgIndex);

		try {
			File file = new File(fileName);
			Metadata mdlib;
			mdlib = ImageMetadataReader.readMetadata(file);
			if (mdlib == null) { return; }
			
			//Debugging print 
			/*System.out.println("----------------------------------" + fileName);
			for (Directory directory : mdlib.getDirectories()) {
				for (Tag tag : directory.getTags()) {
					System.out.println(tag);
				}
			}*/
			
			if (execls.getUserParams().doEventTime || execls.getUserParams().doDuplicate) {
				try {
					ExifIFD0Directory dir = null;
					dir = mdlib.getDirectory(ExifIFD0Directory.class);

					if (dir == null) return;
					Date dateTaken = dir.getDate(ExifIFD0Directory.TAG_DATETIME);
					if (dateTaken == null) return;
					//any of these objects can be null. must test all of them
					
					if (execls.getUserParams().doEventTime) {
						ts = System.currentTimeMillis();
						addToDatePool(dateTaken,imgIndex);
						t_date += System.currentTimeMillis() - ts;
					}

					long fileSize = file.length();
					//use the already computed datetaken value to find duplicates 
					if (execls.getUserParams().doDuplicate) {
						ts = System.currentTimeMillis();
						findDuplicates(fileSize,dateTaken,fileName,imgIndex);
						t_dup += System.currentTimeMillis() - ts;
					}

				} catch (Exception e) {}
			}

			if (execls.getUserParams().doLocation) {
				try {
					ts = System.currentTimeMillis();
					GpsDirectory dirGPS = mdlib.getDirectory(GpsDirectory.class);
					if (dirGPS == null) return;
					GeoLocation geoLocation = dirGPS.getGeoLocation();
					if (geoLocation == null) return;
					//if no location info store, exit.
					addToGPSPool(geoLocation,imgIndex);
					t_gps += System.currentTimeMillis() - ts;
				} catch (Exception e) {}
			}
		} catch (ImageProcessingException | IOException e1) {
			System.err.println("Cant read properties for file " + fileName);
		}
	}

	//----------------------------------operations------------------------------------------

	/**
	 * finds and adds images to their appropriate date group
	 * @param dateTaken
	 * @param fileName
	 */
	private void addToDatePool(Date dateTaken, int fileName)
	{
		boolean asserted = false;

		long time_diff = (long) (60000 * (10 * thresh_Time)); //1 min * (10*t)

		for (Entry<Tuple<Date, Date>, LinkedList<Integer>> entry : sameTimePeriod.entrySet()) {

			Date refFrom = entry.getKey().var1;
			Date refTo = entry.getKey().var2;

			if (dateTaken == null) return;
			long thisTime = dateTaken.getTime();
			double difFrom = thisTime - refFrom.getTime();
			double difTo = thisTime - refTo.getTime();

			int lowHigh = difFrom > 1 ? -1 : difTo > 1 ? 1 : 0;

			if ((-time_diff < difFrom && difFrom < time_diff)
					|| (-time_diff < difTo && difTo < time_diff)) {
				LinkedList<Integer> ll = entry.getValue();
				ll.add(fileName);
				Tuple<Date, Date> t = new Tuple<Date, Date>(dateTaken,dateTaken);
				if (lowHigh == -1) {
					t = new Tuple<Date, Date>(refFrom,dateTaken);
				} else if (lowHigh == 1) {
					t = new Tuple<Date, Date>(dateTaken,refTo);
				}
				sameTimePeriod.remove(entry.getKey());
				sameTimePeriod.put(t,ll);
				asserted = true;
				break;
			}
		}

		if (!asserted) {
			LinkedList<Integer> ll = new LinkedList<Integer>();
			ll.add(fileName);
			Tuple<Date, Date> t = new Tuple<Date, Date>(dateTaken,dateTaken);
			sameTimePeriod.put(t,ll);
		}

	}
	
	/**
	 * Given filedize and date, calculates whether file contains a duplicate 
	 * @param fileSize 
	 * @param dateTaken
	 * @param fileName
	 * @param index
	 * @throws Exception
	 */
	private void findDuplicates(long fileSize, Date dateTaken, String fileName, int index) throws Exception
	{
		if (!duplicateMap.containsKey(dateTaken)) {
			duplicateMap.put(dateTaken,index);
		} else {
			int i = duplicateMap.get(dateTaken);

			String nameParent = imageFiles.get(i);
			long fileSizeParent = (new File(nameParent)).length();

			if (fileSize == fileSizeParent) {
				Metadata mdlib = ImageMetadataReader.readMetadata(new File(nameParent));
				ExifIFD0Directory dir = mdlib.getDirectory(ExifIFD0Directory.class);
				Date dateT = dir.getDate(ExifIFD0Directory.TAG_DATETIME);
				if (dateT.equals(dateTaken)) {
					//duplicate file data sent
					execls.imageDuplicateTo(identifier,index,i,1);
					return;
				}
			}
		}

	}

	/**
	 * given geolocation, finds other images with similar location
	 * @param geoLocation
	 * @param fileIndex
	 */
	private void addToGPSPool(GeoLocation geoLocation, int fileIndex)
	{
		double latGroup = geoLocation.getLatitude();
		double longGroup = geoLocation.getLongitude();

		boolean asserted = false;

		for (Entry<GeoLocation, LinkedList<Integer>> entry : sameLocation.entrySet()) {
			double latFile = entry.getKey().getLatitude();
			double longFile = entry.getKey().getLongitude();

			double distance = distance(latGroup,longGroup,latFile,longFile);
//			System.out.printf("Distance: %.2f meters\n\r",distance);

			//with thresh[0,1,2,3,4,5], dist=[50m,500m,5km,50km,500km,5000km]
			if (distance < thresh_Loc) {
				LinkedList<Integer> ll = entry.getValue();
				ll.add(fileIndex);
				sameLocation.put(entry.getKey(),ll);
				asserted = true;
				break;
			}
		}

		if (!asserted) {
			LinkedList<Integer> ll = new LinkedList<Integer>();
			ll.add(fileIndex);
			sameLocation.put(geoLocation,ll);
		}

	}

	/**
	 * used from 
	 * <a href="http://stackoverflow.com/questions/3694380/calculating-distance-between-two-points-using-latitude-longitude-what-am-i-doi">
	 * StackOverflow - Distance between Coordinates</a>
	 * @param lat1 latitude of file1
	 * @param lon1 longitude of file1
	 * @param lat2 latitude of file2
	 * @param lon2 longitude of file2
	 * @return distance between the 2 coordinates in meters 
	 */
	private double distance(double lat1, double lon1, double lat2, double lon2)
	{
		final int R = 6371; // Radius of the earth

		Double latDistance = deg2rad(lat2 - lat1);
		Double lonDistance = deg2rad(lon2 - lon1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
				+ Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2))
				* Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a),Math.sqrt(1 - a));
		double distance = R * c * 1000; // convert to meters
		return (distance);
	}

	/**
	 * degree to radians
	 * @param deg
	 * @return
	 */
	private double deg2rad(double deg)
	{
		return (deg * Math.PI / 180.0);
	}

	//-----------------------------------------------------------------------------------
	
	/**
	 * prints the sorting of images. DEBUG.
	 */
	@SuppressWarnings("unused")
	private void printSumamry()
	{
		System.out.println("::::::::::::::::METADATA::::::::::::::");
		System.out.println("Total: " + imageFiles.size() + "--------------");
		for (Entry<GeoLocation, LinkedList<Integer>> entry : sameLocation.entrySet()) {
			System.out.println("Location: " + entry.getKey().toString() + " , count: "
					+ entry.getValue().size());
		}

		System.out.println("Time range group <found: [from,to]>: ");

		int singletons = 0;
		for (Entry<Tuple<Date, Date>, LinkedList<Integer>> entry : sameTimePeriod.entrySet()) {
			if (entry.getValue().size() == 1) {
				singletons++;
			} else {
				System.out.println("   " + entry.getValue().size() + " images in range: " +
						entry.getKey().toString());
			}
		}
		System.out.println("Not in a group: " + singletons);
//		System.out.println("No DateTime Metadata: " + noDateTime.size());
		System.out.println(":::::::::::::MetaData finish::::::::::::::");
	}

}
