package image.operation;

import image.common.IV;
import image.execution.ExecuteListener;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

public class Histogram
{

	/**
	 * Determines if images are similar and/or duplicates
	 * @param bhatta
	 * @param i - image index1
	 * @param j - image index2
	 * @param doDuplicate
	 * @param doSimilar
	 * @param execls
	 * @return false only when similar not found
	 */
	public static double checkSimilarAndDuplicates(double bhatta, int i, int j,
			boolean doDuplicate, boolean doSimilar, ExecuteListener execls)
	{
		double thresh = execls.getUserParams().t_Similar;
		// Duplicate found.
		if (bhatta == 0 & doDuplicate) {
			execls.imageDuplicateTo(new int[] { IV.DUPLICATE, IV.HISTOGRAM },i,j,1);
			return 1;
		}
		else if(doSimilar) {
			// Calculate certainty of being similar.
			double certainty = Math.pow((0.005 * thresh),bhatta); 
			execls.imageSimilarTo(new int[] { IV.SIMILAR, IV.HISTOGRAM },i,j,certainty);
			return certainty;
		}
		return 0.0;
	}


	/**
	 * Determines if image can be classified as trash.
	 * @param i - image index
	 * @param img1Hist
	 * @param execls
	 * @return true if trash.
	 */
	public static boolean doTrash(int i, Mat img1Hist, ExecuteListener execls)
	{
		double skewness = 0;
		double mean = 0;
		double stdDev = 0;
		double top = 0;
		double sum = 0;
		
		boolean biased = false;
		double THRESHOLD = Math.pow(0.365,execls.getUserParams().t_Trash);
		
		// Get the mean of the histogram.
		mean = calcMean(img1Hist);
		// Get the standard deviation.
		stdDev = calcStdDev(img1Hist);

		// Step 1 of calculating skewness.
		for (int k = 0; k < img1Hist.rows(); k++) {
			top += Math.pow((k + 1) - mean,3) * img1Hist.get(k,0)[0];
			sum += img1Hist.get(k,0)[0];
		}

		// Negative skewness = peak on the right
		// Positive skewness = peak on the left
		// Step 2 of calculating skewness.
		skewness = (top == 0) ? 0.0 : top / ((sum - 1) * Math.pow(stdDev,3));

		double maxfound = 0.0;
		
		// Count the number of pixels for in a colour range of 25.
		for (int k = 0; k < img1Hist.rows() - 25; k++) {
			double sample = 0;
			for (int m = k; m < k + 25; m++) {
				sample += img1Hist.get(m,0)[0];
			}
			
			// More than TRESHOLD% of colours occur in ~10% of the range of colours.
			if (sample > 0 & (sample / sum) > THRESHOLD) {
				biased = true;	
				maxfound = Math.max(sample/sum,maxfound);
			}
		}

		// Skewed histogram.
		if(Math.abs(skewness) > (1+THRESHOLD)) {
			int[] identifier = { IV.TRASH, IV.HISTOGRAM };
			execls.imageTrash(identifier,i,Math.pow(1-(0.18*(1+THRESHOLD)),Math.abs(skewness)));
			return true;
		} else if(biased) {
			// Biased histogram.
			int[] identifier = { IV.TRASH, IV.HISTOGRAM };
			execls.imageTrash(identifier,i,Math.pow(1-(0.06*THRESHOLD),maxfound));
			return true;
		}
		return false;
	}

	/**
	 * Creates a single RGB histogram of the given image.
	 * @param img - The image to create a histogram of.
	 * @param normalize - Whether or not to normalize the histogram.
	 * @return Histogram (Mat).
	 */
	public static Mat createHistogram(String img, Boolean normalize)
	{
		// Read image.
		Mat image = Highgui.imread(img);
		
		Mat rHist = new Mat();
		Mat gHist = new Mat();
		Mat bHist = new Mat();

		List<Mat> imageList = new ArrayList<Mat>();
		MatOfInt histSize = new MatOfInt(256);
		final MatOfFloat histRange = new MatOfFloat(0f,255f);

		imageList.add(image);
		// Calculate histogram.
		Imgproc.calcHist(imageList,new MatOfInt(0),new Mat(),bHist,histSize,histRange);
		Imgproc.calcHist(imageList,new MatOfInt(1),new Mat(),gHist,histSize,histRange);
		Imgproc.calcHist(imageList,new MatOfInt(2),new Mat(),rHist,histSize,histRange);
		
		// Merge histograms.
		Core.add(bHist, gHist, bHist);
		Core.add(bHist, rHist, bHist);

		if (normalize) {
			Core.normalize(bHist,bHist,0,1,Core.NORM_MINMAX,-1,new Mat());
		}
		
		return bHist;
	}
	
	/**
	 * Calculates the mean of a histogram
	 * @param histogram (Mat)
	 * @return Average value of pixels in the histogram (double)
	 */
	public static double calcMean(Mat histogram)
	{
		double sum = 0;
		double pixels = 0;

		// Iterate through the histogram bins.
		for (int i = 0; i < histogram.rows(); i++) {
			sum = sum + (histogram.get(i,0)[0] * (i+1));
			pixels = pixels + (histogram.get(i,0)[0]);
		}
		if(sum == 0 || pixels == 0) {
			return 0.0;
		}
		return sum / pixels;
	}

	/**
	 * Calculates the variance of a histogram.
	 * @param histogram (Mat)
	 * @return Variance of pixels in the histogram (double)
	 */
	public static double calcVariance(Mat histogram)
	{
		double mean = calcMean(histogram);
		double temp = 0;
		double sum = 0;
		// Iterate through histogram bins.
		for (int i = 0; i < histogram.rows(); i++) {
			temp += Math.pow((i+1) - mean,2) * (histogram.get(i,0)[0]);
			sum += histogram.get(i,0)[0];
		}
		if(sum == 0 || temp == 0) {
			return 0.0;
		}
		return temp / sum;
	}

	/**
	 * Calculates the standard deviation of a histogram.
	 * @param histogram (Mat)
	 * @return Standard deviation of pixels in the histogram (double)
	 */
	public static double calcStdDev(Mat histogram)
	{
		return Math.sqrt(calcVariance(histogram));
	}

}
