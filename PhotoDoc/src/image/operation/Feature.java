package image.operation;

import image.common.IV;
import image.execution.ExecuteListener;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.features2d.DMatch;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;

public class Feature
{
	// Ratio of total matches to matches that pass Lowe's ratio test.
	static double THRESHOLD = 0.25;


	/**
	 * Find similar images using feature matching.
	 * @param mat1
	 * @param mat2
	 * @param i
	 * @param j
	 */
	public static void checkSimilar(final Mat mat1, final Mat mat2, final int i, final int j, final ExecuteListener execls)
	{
		double matchRatio = 0;
		try {
			final double thresh = execls.getUserParams().t_Similar;
			matchRatio = match(mat1,mat2,execls.getUserParams().use_Feature_Flann);
			// If matchratio exceeds THRESHOLD, then classify similar.
			if (matchRatio > THRESHOLD) {
				final double certainty = Math.pow((0.63 * thresh),(1 - matchRatio));
				execls.imageSimilarTo(new int[] { IV.SIMILAR, IV.FACE_DETECTION },i,j,certainty);
			}
		} catch (final Exception e) {
			System.err.println("FEAT: error on " + i + "," + j);
			e.printStackTrace();
		}
	}
	/**
	 * Calculate key points using SIFT.
	 * @param image (Mat - greyscale since SIFT is designed for greyscale images!)
	 * @return Descriptors (Mat) - Set of descriptors for found key points.
	 */
	public static Mat sift(final Mat image)
	{
		Mat descriptors = new Mat();

		// Set up SIFT.
		FeatureDetector sift = FeatureDetector.create(FeatureDetector.SIFT);

		// Keypoints.
		MatOfKeyPoint kp = new MatOfKeyPoint();

		// Get Keypoints using SIFT.
		sift.detect(image,kp);

		// Convert points into SIFT descriptors.
		DescriptorExtractor de = DescriptorExtractor.create(DescriptorExtractor.SIFT);
		de.compute(image,kp,descriptors);

		return descriptors;
	}

	/**
	 * Match key points across images.
	 * @param image1 (File)
	 * @param image2 (File)
	 * @param orig1 
	 * @return int - number of matches found.
	 */
	public static double match(final Mat img1, final Mat img2, boolean useFlann)
	{
		if(img1.rows() > 1 && img2.rows() > 1) {
			List<MatOfDMatch> matMatchesList = new ArrayList<MatOfDMatch>();
			List<DMatch> matchesList = new ArrayList<DMatch>();
		
			// Pick a matcher.
			DescriptorMatcher flann = null;
			if(useFlann) {
				flann = DescriptorMatcher.create(DescriptorMatcher.FLANNBASED);
			} else {
				flann = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE);
			}
			flann.knnMatch(img1,img2,matMatchesList,2);

			// Lowe's ratio test between two nearest neighbours. Paper says distance ratio of less then 0.8.
			for (int i = 0; i < matMatchesList.size(); i++) {
				for (int j = 0; j < matMatchesList.get(i).toArray().length - 1; j++) {
					if (matMatchesList.get(i).toArray()[j].distance
							/ matMatchesList.get(i).toArray()[j + 1].distance < 0.8) {
						matchesList.add(matMatchesList.get(i).toArray()[j]);
					}
				}
			}
		
			// Ratio of all matches to matches that pass Lowe's ratio test.
			// Divided by 2 since 2-nearest-neighbours was used.
			final double ratio = matchesList.size() / (double) (matMatchesList.size() / 2);

			flann = null;
			matMatchesList = null;
			matchesList = null;

			return ratio;
		} else {
			System.err.println("[FEATURE] AN IMAGE HAS NO KEY POINTS!");
			return 0.0;
		}
	}
}