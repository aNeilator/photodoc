package image.common;

import java.util.Date;

import com.drew.lang.GeoLocation;

public class ImageDetails
{
	/**
	 * 0 means no group
	 * integer >= 1 corresponds to group id.
	 */
	public int group_Sim = -1;
	/**
	 * -1 means no group
	 * integer >= 1 corresponds to group id.
	 */
	public int group_Dup = -1;
	/**
	 * -1 means no group
	 * integer >= 1 corresponds to group id.
	 */
	public int group_Event = -1;
	/**
	 * -1 means no group
	 * integer >= 1 corresponds to group id.
	 */
	public int group_Loc = -1;

	/**
	 * 0 when image not in a group.
	 * 1 is maximum. 0.7 is minimum.
	 */
	public double cert_Sim = 0;
	/**
	 * 0 when image not in a group.
	 * 1 is maximum. 0.7 is minimum.
	 */
	public double cert_Dup = 0;
	/**
	 * 0 when image not in a group.
	 * 1 is maximum. 0.7 is minimum.
	 */
	public double cert_Trash = 0;
	
	/**
	 * Tuple of start time and end time.
	 * null when does not belong in event group. 
	 */
	public Tuple<Date, Date> info_Event = null;
	/**
	 * Geolocation library that tells the coordinates.
	 * null when does not belong in location group. 
	 */
	public GeoLocation info_Loc = null;
	/**
	 * 0 means no Faces Found for this image
	 */
	public int info_Faces = 0;

}
