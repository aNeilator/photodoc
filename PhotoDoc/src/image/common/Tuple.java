package image.common;


public class Tuple<A, B>
{
	public final A var1;
	public final B var2;

	public Tuple(A x, B y) {
		this.var1 = x;
		this.var2 = y;
	}

	public String toString()
	{
		return "[" + var1.toString() + ", " + var2.toString() + "]";
	}
}
