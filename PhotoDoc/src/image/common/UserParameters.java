package image.common;

import java.io.File;

public class UserParameters
{
	public File[] folders;
	
	public boolean doSimilar = true;
	public boolean doLocation = true;
	public boolean doEventTime = true;

	public boolean doFaces = true;
	public boolean doDuplicate = true;
	public boolean doTrash = true;
	
	public boolean doSimilar_feature = false;
	public boolean doDuplicateOpenCV= true;

	public double t_Similar = 1;
	public double t_Location = 1;
	public double t_Time = 1;
	public double t_Trash = 1;
	

	public boolean use_transitiveGrouping = true;
	public boolean use_multiThreading =  true;
	// Feature matching method. Default is brute force.
	public boolean use_Feature_Flann = false;
	
	//To be taken care from Layout File. 
	public boolean export_showResults = true;
}
