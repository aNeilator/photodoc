package image.common;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import org.opencv.core.Mat;

public class IV
{

	//Image operation ids.
	public static final int SIMILAR = 0, DUPLICATE = 1, TRASH = 2;
	public static final int GROUP_FACE = 3, GROUP_EVENT = 4, GROUP_LOC = 5;
	public static final int HISTOGRAM = 11, TEMPLATE_MATCHING = 12, FEATURE_POINT = 13;
	public static final int METADATA = 21, BACKGROUND_DEF = 22;
	public static final int EDGE_DETECTION = 31, FACE_DETECTION = 41;
	public static final int ALL_OpenCV = 101, ALL_MetaData = 100, ALL = 102;
	
	//shows where resource folder is
	public static final String RES = "res/", RESIMG = "res/images/",TEST_OUTPUT = "test/output/";

	//allowed image types
	private static final String[] allowedImageTypes = { "jpg", "jpeg", "png", "tiff", "tif" };
	
	//File normatlisation size
	public static final int NORM_SIZE = 512;
	
	//Threshold values
	public static final double CRThresh_High = 0.78, CRThresh_Low = 0.66, CRThresh = 0.7;
	
	//Default thumb size of image labels
	public static final int THUMB_SIZE = 100;
	
	//Color values
	public static final Color color_transparent = new Color(0,0,0,0);
	public static final Color color_translucent_white = new Color(250,250,250,50);
	public static final Color color_translucent_white_90 = new Color(250,250,250,90);
	public static final Color color_darker_background = new Color(228,228,228);
	public static final Color color_darker_background_darker = new Color(218,218,218);
	public static final Color color_background_lighter = new Color(244,244,244);
	public static final Color color_text = new Color(40,40,40);
	public static final Color color_text_highl = new Color(110,110,110);
	public static final Color color_white_t_90 = new Color(255,255,255,200);
	public static final Color color_text_20 = new Color(20,20,20,20);

	//Export ids
	public static final int IM_LOADED = 200, IM_DELETED = 201, IM_MOVED = 202, 
			IM_COPIED = 203,IM_DELETE_EMP_FOLDERS = 204;
	/**
	 * is this file an image?
	 * @param file
	 * @return
	 */
	public static boolean isImage(File file)
	{
		String extension = "";

		int i = file.getName().lastIndexOf('.');
		if (i > 0) {
			extension = file.getName().substring(i + 1);
		}

		for (String ext : allowedImageTypes) {
			if (ext.equalsIgnoreCase(extension)) { return true; }
		}
		return false;
	}

	/**
	 * creates a folder at the specified path. if it already exists, it does not create any.
	 * @param path
	 */
	public static void createFolder(String path)
	{
		File f = new File(path);
		if (!f.isDirectory()) {
			f.mkdirs();
		}
	}

	/**
	 * Displays a given image in a new window.
	 * @param img2
	 */
	public static void displayImage(Image img2)
	{
		//BufferedImage img=ImageIO.read(new File("/HelloOpenCV/lena.png"));
		ImageIcon icon = new ImageIcon(img2);
		JFrame frame = new JFrame();
		frame.setLayout(new FlowLayout());
		frame.setSize(img2.getWidth(null) + 50,img2.getHeight(null) + 50);
		JLabel lbl = new JLabel();
		lbl.setIcon(icon);
		frame.add(lbl);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/**
	 * Converts MAT to image. used during testing
	 * @param m
	 * @return
	 */
	public static Image toBufferedImage(Mat m)
	{
		int type = BufferedImage.TYPE_BYTE_GRAY;
		if (m.channels() > 1) {
			type = BufferedImage.TYPE_3BYTE_BGR;
		}
		int bufferSize = m.channels() * m.cols() * m.rows();
		byte[] b = new byte[bufferSize];
		m.get(0,0,b); // get all the pixels
		BufferedImage image = new BufferedImage(m.cols(),m.rows(),type);
		final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		System.arraycopy(b,0,targetPixels,0,b.length);
		return image;

	}

	/**
	 * returns a list of image files from all directories
	 * @param folder
	 * @return
	 */
	public static ArrayList<String> getImageFilesList(File folder)
	{
		ArrayList<String> files = new ArrayList<String>();

		for (File file : folder.listFiles()) {
			if (IV.isImage(file)) {
				String pdimg = file.getAbsolutePath();
				files.add(pdimg);
			}
			if (file.isDirectory()) {
				files.addAll(getImageFilesList(file));
			}
		}
		return files;
	}

	/**
	 * writes string to file. used during testing
	 * @param str
	 * @param fileName
	 */
	public static void writeStrToFile(String str, String fileName)
	{
		BufferedWriter writer = null;
		try {
			String path = TEST_OUTPUT + fileName;
			if (!new File(TEST_OUTPUT).exists()) {
				new File(TEST_OUTPUT).mkdirs();
			}
			writer = new BufferedWriter(new FileWriter(path));
			writer.write(str);
			System.out.println("File written to: " + path);
		} catch (IOException e) {
			System.err.println("Could not write file");
		} finally {
			try {
				if (writer != null)
					writer.close();
			} catch (IOException e) {
				System.err.println("Could not close file");
			}
		}
	}

	/**
	 * shows a java error box with given string
	 * @param str
	 */
	public static void showErrorDialog(String str)
	{
		JOptionPane.showMessageDialog(new JFrame(),str,"Dialog",
				JOptionPane.ERROR_MESSAGE);

	}

	/**
	 * Converts int id types to string to be used in Layout
	 * @param type
	 * @return
	 */
	public static String getTypeStr(int type)
	{
		String name = "unknown";
		switch (type) {
		case IV.SIMILAR:
			name = "sim";
			break;
		case IV.DUPLICATE:
			name = "dup";
			break;
		case IV.TRASH:
			name = "trash";
			break;
		case IV.GROUP_EVENT:
			name = "event";
			break;
		case IV.GROUP_FACE:
			name = "face";
			break;
		case IV.GROUP_LOC:
			name = "loc";
			break;
		}
		return name;
	}
	
	
	public static String getTypeStrFull(int type)
	{
		String name = "unknown";
		switch (type) {
		case IV.SIMILAR:
			name = "Similar";
			break;
		case IV.DUPLICATE:
			name = "Duplicate";
			break;
		case IV.TRASH:
			name = "Trash";
			break;
		case IV.GROUP_EVENT:
			name = "Event";
			break;
		case IV.GROUP_FACE:
			name = "Face";
			break;
		case IV.GROUP_LOC:
			name = "Location";
			break;
		}
		return name;
	}

	/**
	 * Launches the help page
	 */
	public static void launchHelpPage() {
		String path = System.getProperty("user.dir")
				+ File.separator + "resources"
				+ File.separator + "guide"
				+ File.separator + "index.html";
		try {
			Desktop.getDesktop().open(new File(path));
		} catch (IOException | IllegalArgumentException e) {
			showErrorDialog(path+" is missing!");
		}

	}
	
}
