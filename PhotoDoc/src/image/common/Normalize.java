package image.common;

import java.io.File;
import java.util.*;

import org.opencv.core.Size;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

public class Normalize
{

	/**
	 * Default constructor.
	 */
	public Normalize() {}

	/**
	 * 
	 * @param image (File).
	 * @return image with max dimension of 512.
	 */
	public static Mat normalize(File image)
	{
		Mat normImage = Highgui.imread(image.getAbsolutePath());

		Size dsize = null;

		if (normImage.height() <= 512 && normImage.width() <= 512) {
			// Image is already small enough.
		}
		else {
			if (normImage.height() > normImage.width()) {
				dsize = new Size(normImage.width() / ((double) normImage.height() / 512),512);
			}
			else {
				dsize = new Size(512,normImage.height() / ((double) normImage.width() / 512));
			}

			// Use bilinear interpolation
			Imgproc.resize(normImage,normImage,dsize,0,0,Imgproc.INTER_LINEAR);
		}
		return normImage;
	}

	/**
	 * 
	 * @param image (Mat)
	 * @return image with max dimension of 512.
	 */
	public static Mat normalize(Mat image)
	{

		Size dsize = null;

		if (image.height() <= 512 && image.width() <= 512) {
			// Image is already small enough.
		}
		else {
			if (image.height() > image.width()) {
				dsize = new Size(image.width() / ((double) image.height() / 512),512);
			}
			else {
				dsize = new Size(512,image.height() / ((double) image.width() / 512));
			}

			// Use bilinear interpolation
			Imgproc.resize(image,image,dsize,0,0,Imgproc.INTER_LINEAR);
		}
		return image;
	}

	public static Size getResize(int height, int width)
	{

		Size dsize = null;

//		if(height <= IV.NORM_SIZE && width <= IV.NORM_SIZE) {
		if (height > width) {
			dsize = new Size(width / ((double) height / IV.NORM_SIZE),IV.NORM_SIZE);
		}
		else {
			dsize = new Size(IV.NORM_SIZE,height / ((double) width / IV.NORM_SIZE));
		}
		return dsize;
	}

	/**
	 * 
	 * @param images (ArrayList<File>).
	 * @return array of images with max dimension of 512.
	 */
	public static ArrayList<Mat> normalize(ArrayList<File> images)
	{
		ArrayList<Mat> normImages = new ArrayList<Mat>();
		for (int i = 0; i < images.size(); i++) {
			normImages.add(normalize(images.get(i)));
		}
		return normImages;
	}

	/**
	 * 
	 * @param image (File)
	 * @return Greyscale image.
	 */
	public static Mat desaturate(File image)
	{
		Mat desatImage = new Mat();
		Mat inputImage = Highgui.imread(image.getAbsolutePath());
		Imgproc.cvtColor(inputImage,desatImage,Imgproc.COLOR_BGR2GRAY,3);
		return desatImage;
	}

	/**
	 * 
	 * @param image (Mat)
	 * @return Greyscale image.
	 */
	public static Mat desaturate(final Mat image)
	{
		Mat desatImage = new Mat();
		Imgproc.cvtColor(image,desatImage,Imgproc.COLOR_BGR2GRAY,3);
		return desatImage;
	}

	/**
	 * 
	 * @param images (ArrayList<File>)
	 * @return array list of greyscale images.
	 */
	public static ArrayList<Mat> desaturate(ArrayList<File> images)
	{
		ArrayList<Mat> desatImages = new ArrayList<Mat>();
		for (int i = 0; i < images.size(); i++) {
			desatImages.add(desaturate(images.get(i)));
		}
		return desatImages;
	}

}
