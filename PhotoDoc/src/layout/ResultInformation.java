package layout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class ResultInformation {
	public int numberOfImages;

	public int numberOfSimilarImages;
	public int numberOfEventImages;
	public int numberOfLocationImages;
	public int numberOfDuplicateImages;
	public int numberOfTrashImages;
	public int numberOfFacesImages;

	public int numberOfSimilarGroups;
	public int numberOfEventGroups;
	public int numberOfLocationGroups;
	public int numberOfDuplicateGroups;

	public HashMap<Integer, HashSet<String>> similarGroups;
	public HashMap<Integer, HashSet<String>> eventGroups;
	public HashMap<Integer, HashSet<String>> locationGroups;
	public HashMap<Integer, HashSet<String>> duplicateGroups;

	public Set<String> trashImages;
	public Set<String> faceImages;

	public ResultInformation() {
		reset();
	}

	public void reset() {
		numberOfImages = 0;
		numberOfSimilarImages = 0;
		numberOfEventImages = 0;
		numberOfLocationImages = 0;
		numberOfDuplicateImages = 0;
		numberOfTrashImages = 0;
		numberOfFacesImages = 0;
		numberOfSimilarGroups = 0;
		numberOfEventGroups = 0;
		numberOfLocationGroups = 0;
		numberOfDuplicateGroups = 0;

		similarGroups = new HashMap<Integer, HashSet<String>>();
		eventGroups = new HashMap<Integer, HashSet<String>>();
		locationGroups = new HashMap<Integer, HashSet<String>>();
		duplicateGroups = new HashMap<Integer, HashSet<String>>();
		trashImages = new HashSet<String>();
		faceImages = new HashSet<String>();

	}

	/*
	 * deletes images from all the categories so that the update is accross all tabs
	 */
	public void DeleteImages(ArrayList<String> FilesToDelete) {
		numberOfImages -= FilesToDelete.size();
		int numImages = 0;

		Iterator<Entry<Integer, HashSet<String>>> iterator = duplicateGroups
				.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<Integer, HashSet<String>> entry = iterator.next();
			for (String file : FilesToDelete) {
				entry.getValue().remove(file);
			}
			if (entry.getValue().size() > 1) {
				numImages += entry.getValue().size();
			} else {
				iterator.remove();
			}
		}
		numberOfDuplicateGroups = duplicateGroups.size();
		numberOfDuplicateImages = numImages;

		numImages = 0;
		iterator = similarGroups.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<Integer, HashSet<String>> entry = iterator.next();
			for (String file : FilesToDelete) {
				entry.getValue().remove(file);
			}
			if (entry.getValue().size() > 1) {
				HashSet<String> group = entry.getValue();
				numImages += group.size();
			} else {
				iterator.remove();
			}
		}
		numberOfSimilarGroups = similarGroups.size();
		numberOfSimilarImages = numImages;

		numImages = 0;
		iterator = eventGroups.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<Integer, HashSet<String>> entry = iterator.next();
			for (String file : FilesToDelete) {
				entry.getValue().remove(file);
			}
			if (entry.getValue().size() > 1) {
				HashSet<String> group = entry.getValue();
				numImages += group.size();
			} else {
				iterator.remove();
			}
		}
		numberOfEventGroups = eventGroups.size();
		numberOfEventImages = numImages;

		numImages = 0;
		iterator = locationGroups.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<Integer, HashSet<String>> entry = iterator.next();
			for (String file : FilesToDelete) {
				entry.getValue().remove(file);
			}
			if (entry.getValue().size() > 1) {
				HashSet<String> group = entry.getValue();
				numImages += group.size();
			} else {
				iterator.remove();
			}
		}
		numberOfLocationGroups = locationGroups.size();
		numberOfLocationImages = numImages;

		Iterator<String> stringIterator = trashImages.iterator();
		while (iterator.hasNext()) {
			String file = stringIterator.next();
			if (FilesToDelete.contains(file)) {
				iterator.remove();
			}
		}
		numberOfTrashImages = trashImages.size();

		stringIterator = faceImages.iterator();
		while (iterator.hasNext()) {
			String file = stringIterator.next();
			if (FilesToDelete.contains(file)) {
				iterator.remove();
			}
		}
		numberOfFacesImages = faceImages.size();
	}
}
