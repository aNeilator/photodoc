package layout.components;

import image.common.IV;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class CustomButton extends JButton {

	 private Color hoverBackgroundColor = IV.color_darker_background;
    private Color pressedBackgroundColor = IV.color_darker_background_darker;
    
	public CustomButton() {
		setNoBorder();
		
	}

	@Override
   protected void paintComponent(Graphics g) {
       if (getModel().isPressed()) {
           g.setColor(pressedBackgroundColor);
       } else if (getModel().isRollover()) {
           g.setColor(hoverBackgroundColor);
       } else {
           g.setColor(getBackground());
       }
       g.fillRect(0, 0, getWidth(), getHeight());
       super.paintComponent(g);
   }
	
	public void setNoBorder() {
		setBorder(BorderFactory.createEmptyBorder());
//		setBorderPainted(false);
		setContentAreaFilled(false);
		setFocusPainted(false);
	}
	
}
