package layout;

import image.common.UserParameters;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.LayoutStyle;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

@SuppressWarnings("serial")
public class AdvancedFrame extends JDialog {

	public boolean newValuesAccepted;
	public UserParameters newUserSettings;

	private UserParameters userSettings;
	private JSlider similarSlider;
	private JSlider eventSlider;
	private JSlider trashSlider;
	private JSlider locationSlider;
	private JCheckBox extensiveCheckBox;
	private JCheckBox multiCheckBox;
	private JCheckBox featureCheckBox;
	private JRadioButton bruteRadioButton;
	private JRadioButton flannRadioButton;
	private JLabel methodLabel;
	private JLabel similarThreshold;
	private JLabel eventThreshold;
	private JLabel trashThreshold;
	private JLabel locationThreshold;

	public AdvancedFrame(UserParameters settings) {
		this.userSettings = settings;

		initComponents();
		setVisible(true);
	}

	/*
	 * Initializes the components of the JDialog
	 */
	private void initComponents() {
		windowSettings();

		newUserSettings = new UserParameters();
		newValuesAccepted = false;
		JSeparator threshSeparator = new JSeparator();
		JSeparator bottomSeparator = new JSeparator();
		JLabel titleLabel = new JLabel();
		JLabel similarLabel = new JLabel();
		JLabel eventLabel = new JLabel();
		JLabel trashLabel = new JLabel();
		JLabel locationLabel = new JLabel();
		JLabel extensiveLabel = new JLabel();
		JLabel multiLabel = new JLabel();
		JLabel featureLabel = new JLabel();
		methodLabel = new JLabel();

		titleLabel.setText("Threshold Values:");
		similarLabel.setText("Similar");
		eventLabel.setText("Event");
		trashLabel.setText("Trash");
		locationLabel.setText("Location");
		extensiveLabel.setText("Extensive Duplicate Search:");
		multiLabel.setText("Multi-Thread:");
		featureLabel.setText("Use Feature Matching:");
		methodLabel.setText("Feature Matching Method:");

		setupSliders();
		setupCheckBoxes();
		setupRadioButtons();

		JButton okButton = new JButton();
		okButton.setText("OK");
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				okButtonActionPerformed(evt);
			}
		});

		JButton cancelButton = new JButton();
		cancelButton.setText("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				cancelButtonActionPerformed(evt);
			}
		});

		JButton resetButton = new JButton();
		resetButton.setText("Reset Defaults");
		resetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				resetButtonActionPerformed(evt);
			}
		});

		updateFeatureTypeSelection();

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addContainerGap()
								.addGroup(
										layout.createParallelGroup(
												GroupLayout.Alignment.LEADING)
												.addComponent(threshSeparator)
												.addGroup(
														layout.createSequentialGroup()
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.LEADING)
																				.addComponent(
																						titleLabel)
																				.addGroup(
																						layout.createSequentialGroup()
																								.addGroup(
																										layout.createParallelGroup(
																												GroupLayout.Alignment.LEADING)
																												.addComponent(
																														locationLabel)
																												.addComponent(
																														trashLabel)
																												.addComponent(
																														eventLabel)
																												.addComponent(
																														similarLabel))
																								.addGap(18,
																										18,
																										18)
																								.addGroup(
																										layout.createParallelGroup(
																												GroupLayout.Alignment.LEADING)
																												.addGroup(
																														layout.createSequentialGroup()
																																.addComponent(
																																		similarSlider,
																																		GroupLayout.PREFERRED_SIZE,
																																		144,
																																		GroupLayout.PREFERRED_SIZE)
																																.addGap(18,
																																		18,
																																		18)
																																.addComponent(
																																		similarThreshold))
																												.addGroup(
																														GroupLayout.Alignment.TRAILING,
																														layout.createSequentialGroup()
																																.addComponent(
																																		eventSlider,
																																		GroupLayout.PREFERRED_SIZE,
																																		144,
																																		GroupLayout.PREFERRED_SIZE)
																																.addGap(18,
																																		18,
																																		18)
																																.addComponent(
																																		eventThreshold))
																												.addGroup(
																														layout.createSequentialGroup()
																																.addComponent(
																																		trashSlider,
																																		GroupLayout.PREFERRED_SIZE,
																																		144,
																																		GroupLayout.PREFERRED_SIZE)
																																.addGap(18,
																																		18,
																																		18)
																																.addComponent(
																																		trashThreshold))
																												.addGroup(
																														layout.createSequentialGroup()
																																.addComponent(
																																		locationSlider,
																																		GroupLayout.PREFERRED_SIZE,
																																		144,
																																		GroupLayout.PREFERRED_SIZE)
																																.addGap(18,
																																		18,
																																		18)
																																.addComponent(
																																		locationThreshold)))))
																.addGap(0,
																		0,
																		Short.MAX_VALUE))
												.addGroup(
														layout.createSequentialGroup()
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.TRAILING)
																				.addComponent(
																						extensiveLabel,
																						GroupLayout.Alignment.LEADING)
																				.addComponent(
																						multiLabel,
																						GroupLayout.Alignment.LEADING)
																				.addComponent(
																						featureLabel,
																						GroupLayout.Alignment.LEADING))
																.addGap(0,
																		0,
																		Short.MAX_VALUE)
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.LEADING)
																				.addComponent(
																						featureCheckBox)
																				.addComponent(
																						multiCheckBox)
																				.addComponent(
																						extensiveCheckBox))
																.addGap(0,
																		0,
																		Short.MAX_VALUE))
												.addGroup(
														layout.createSequentialGroup()
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.TRAILING,
																				false)
																				.addComponent(
																						cancelButton,
																						GroupLayout.Alignment.LEADING,
																						GroupLayout.DEFAULT_SIZE,
																						GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE)
																				.addComponent(
																						resetButton,
																						GroupLayout.Alignment.LEADING,
																						GroupLayout.DEFAULT_SIZE,
																						GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE))
																.addPreferredGap(
																		LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.LEADING)
																				.addComponent(
																						okButton,
																						GroupLayout.PREFERRED_SIZE,
																						138,
																						GroupLayout.PREFERRED_SIZE))
																.addGap(0,
																		0,
																		Short.MAX_VALUE))
												.addGroup(
														layout.createSequentialGroup()
																.addComponent(
																		methodLabel)
																.addGap(0,
																		0,
																		Short.MAX_VALUE))
												.addGroup(
														layout.createSequentialGroup()
																.addComponent(
																		bruteRadioButton)
																.addGap(18, 18,
																		18)
																.addComponent(
																		flannRadioButton)
																.addGap(0,
																		0,
																		Short.MAX_VALUE))
												.addComponent(
														bottomSeparator,
														GroupLayout.Alignment.TRAILING))
								.addContainerGap()));
		layout.setVerticalGroup(layout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addContainerGap()
								.addComponent(titleLabel)
								.addGroup(
										layout.createParallelGroup(
												GroupLayout.Alignment.TRAILING)
												.addGroup(
														layout.createSequentialGroup()
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.TRAILING)
																				.addGroup(
																						layout.createSequentialGroup()
																								.addComponent(
																										similarLabel)
																								.addGap(19,
																										19,
																										19)
																								.addComponent(
																										eventLabel)
																								.addGap(19,
																										19,
																										19)
																								.addComponent(
																										trashLabel)
																								.addGap(19,
																										19,
																										19)
																								.addComponent(
																										locationLabel))
																				.addGroup(
																						layout.createSequentialGroup()
																								.addGroup(
																										layout.createParallelGroup(
																												GroupLayout.Alignment.TRAILING)
																												.addComponent(
																														similarThreshold)
																												.addComponent(
																														similarSlider,
																														GroupLayout.PREFERRED_SIZE,
																														GroupLayout.DEFAULT_SIZE,
																														GroupLayout.PREFERRED_SIZE))
																								.addPreferredGap(
																										LayoutStyle.ComponentPlacement.RELATED)
																								.addGroup(
																										layout.createParallelGroup(
																												GroupLayout.Alignment.TRAILING)
																												.addComponent(
																														eventThreshold)
																												.addComponent(
																														eventSlider,
																														GroupLayout.PREFERRED_SIZE,
																														GroupLayout.DEFAULT_SIZE,
																														GroupLayout.PREFERRED_SIZE))
																								.addPreferredGap(
																										LayoutStyle.ComponentPlacement.RELATED)
																								.addGroup(
																										layout.createParallelGroup(
																												GroupLayout.Alignment.TRAILING)
																												.addComponent(
																														trashThreshold)
																												.addComponent(
																														trashSlider,
																														GroupLayout.PREFERRED_SIZE,
																														GroupLayout.DEFAULT_SIZE,
																														GroupLayout.PREFERRED_SIZE))
																								.addPreferredGap(
																										LayoutStyle.ComponentPlacement.RELATED)
																								.addGroup(
																										layout.createParallelGroup(
																												GroupLayout.Alignment.TRAILING)
																												.addComponent(
																														locationThreshold)
																												.addComponent(
																														locationSlider,
																														GroupLayout.PREFERRED_SIZE,
																														GroupLayout.DEFAULT_SIZE,
																														GroupLayout.PREFERRED_SIZE))))
																.addGap(18, 18,
																		18)
																.addComponent(
																		threshSeparator,
																		GroupLayout.PREFERRED_SIZE,
																		10,
																		GroupLayout.PREFERRED_SIZE)
																.addGap(18, 18,
																		18)
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.TRAILING)
																				.addComponent(
																						extensiveLabel)
																				.addComponent(
																						extensiveCheckBox))
																.addPreferredGap(
																		LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.TRAILING)
																				.addComponent(
																						multiLabel)
																				.addComponent(
																						multiCheckBox))
																.addGap(12, 12,
																		12)
																.addComponent(
																		featureLabel))
												.addComponent(featureCheckBox))
								.addPreferredGap(
										LayoutStyle.ComponentPlacement.UNRELATED)
								.addComponent(methodLabel)
								.addPreferredGap(
										LayoutStyle.ComponentPlacement.UNRELATED)
								.addGroup(
										layout.createParallelGroup(
												GroupLayout.Alignment.BASELINE)
												.addComponent(bruteRadioButton)
												.addComponent(flannRadioButton))
								.addPreferredGap(
										LayoutStyle.ComponentPlacement.UNRELATED)
								.addComponent(bottomSeparator,
										GroupLayout.PREFERRED_SIZE, 10,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(
										LayoutStyle.ComponentPlacement.RELATED,
										18, Short.MAX_VALUE)
								.addGroup(
										layout.createParallelGroup(
												GroupLayout.Alignment.BASELINE)
												.addComponent(resetButton))
								.addPreferredGap(
										LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(
										layout.createParallelGroup(
												GroupLayout.Alignment.BASELINE)
												.addComponent(okButton)
												.addComponent(cancelButton))
								.addContainerGap()));

		pack();
	}

	private void windowSettings() {
		setTitle("Advanced Settings");
		setResizable(false);
		setBackground(new Color(0, 0, 0));
		setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		setMinimumSize(new Dimension(300, 480));
		setMaximumSize(new Dimension(300, 480));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	/*
	 * sets up the sliders used for the thresholds
	 */
	private void setupSliders() {
		similarThreshold = new JLabel();
		eventThreshold = new JLabel();
		trashThreshold = new JLabel();
		locationThreshold = new JLabel();

		similarSlider = new JSlider();
		similarSlider.setValue((int) (userSettings.t_Similar * 20));
		similarSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent evt) {
				JSlider slider = (JSlider) evt.getSource();
				if (!slider.getValueIsAdjusting()) {
					double value = slider.getValue();
					similarThreshold.setText(String.format("%.1f", value / 20));
					newUserSettings.t_Similar = Double.valueOf(similarThreshold
							.getText());
				}
			}
		});

		eventSlider = new JSlider();
		eventSlider.setValue((int) (userSettings.t_Time * 20));
		eventSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent evt) {
				JSlider slider = (JSlider) evt.getSource();
				if (!slider.getValueIsAdjusting()) {
					double value = slider.getValue();
					eventThreshold.setText(String.format("%.1f", value / 20));
					newUserSettings.t_Time = Double.valueOf(eventThreshold
							.getText());
				}
			}
		});

		trashSlider = new JSlider();
		trashSlider.setValue((int) (userSettings.t_Trash * 20));
		trashSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent evt) {
				JSlider slider = (JSlider) evt.getSource();
				if (!slider.getValueIsAdjusting()) {
					double value = slider.getValue();
					trashThreshold.setText(String.format("%.1f", value / 20));
					newUserSettings.t_Trash = Double.valueOf(trashThreshold
							.getText());
				}
			}
		});

		locationSlider = new JSlider();
		locationSlider.setValue((int) (userSettings.t_Location * 20));
		locationSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent evt) {
				JSlider slider = (JSlider) evt.getSource();
				if (!slider.getValueIsAdjusting()) {
					double value = slider.getValue();
					locationThreshold.setText(String.format("%.1f", value / 20));
					newUserSettings.t_Location = Double
							.valueOf(locationThreshold.getText());
				}
			}
		});

		similarThreshold.setText(String.format("%.1f", userSettings.t_Similar));
		eventThreshold.setText(String.format("%.1f", userSettings.t_Time));
		trashThreshold.setText(String.format("%.1f", userSettings.t_Trash));
		locationThreshold.setText(String
				.format("%.1f", userSettings.t_Location));
	}

	/*
	 * sets up all the checks boxes
	 */
	private void setupCheckBoxes() {
		extensiveCheckBox = new JCheckBox();
		extensiveCheckBox.setSelected(userSettings.doDuplicateOpenCV);
		extensiveCheckBox.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent evt) {
				newUserSettings.doDuplicateOpenCV = extensiveCheckBox
						.isSelected();
			}
		});

		multiCheckBox = new JCheckBox();
		multiCheckBox.setSelected(userSettings.use_multiThreading);
		multiCheckBox.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent evt) {
				newUserSettings.use_multiThreading = multiCheckBox.isSelected();
			}
		});

		featureCheckBox = new JCheckBox();
		featureCheckBox.setSelected(userSettings.doSimilar_feature);
		featureCheckBox.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent evt) {
				updateFeatureTypeSelection();
			}
		});
	}

	/*
	 * sets up the radio buttons
	 */
	private void setupRadioButtons() {
		bruteRadioButton = new JRadioButton();
		flannRadioButton = new JRadioButton();

		bruteRadioButton.setText("Brute Force");
		bruteRadioButton.setSelected(!userSettings.use_Feature_Flann);
		bruteRadioButton.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent evt) {
				newUserSettings.use_Feature_Flann = !bruteRadioButton
						.isSelected();
				flannRadioButton.setSelected(newUserSettings.use_Feature_Flann);
			}
		});

		flannRadioButton.setText("Flann");
		flannRadioButton.setSelected(userSettings.use_Feature_Flann);
		flannRadioButton.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent evt) {
				newUserSettings.use_Feature_Flann = flannRadioButton
						.isSelected();
				bruteRadioButton
				.setSelected(!newUserSettings.use_Feature_Flann);
			}
		});
	}

	/*
	 * called to make sure that either flann or brute force is active
	 * there must always be one and only one active
	 */
	private void updateFeatureTypeSelection() {
		if (featureCheckBox.isSelected()) {
			newUserSettings.doSimilar_feature = true;
			bruteRadioButton.setEnabled(true);
			flannRadioButton.setEnabled(true);
			methodLabel.setEnabled(true);
		} else {
			newUserSettings.doSimilar_feature = false;
			bruteRadioButton.setEnabled(false);
			flannRadioButton.setEnabled(false);
			methodLabel.setEnabled(false);
		}
	}

	/*
	 * when ok button pressed new values can be accepted so a public variable is set for the listener
	 * in layout to check. the advances settings are then disposed of.
	 */
	private void okButtonActionPerformed(ActionEvent evt) {
		newValuesAccepted = true;
		dispose();
	}

	/*
	 * advanced settings are disposed of
	 */
	private void cancelButtonActionPerformed(ActionEvent evt) {
		dispose();
	}

	/*
	 * when reset is pressed all values are returned to their default values
	 */
	private void resetButtonActionPerformed(ActionEvent evt) {
		similarSlider.setValue(1 * 20);
		eventSlider.setValue(1 * 20);
		trashSlider.setValue(1 * 20);
		locationSlider.setValue(1 * 20);

		extensiveCheckBox.setSelected(true);
		multiCheckBox.setSelected(true);
		featureCheckBox.setSelected(false);

		flannRadioButton.setSelected(false);
	}
}
