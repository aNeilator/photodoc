package layout;

import image.common.IV;
import image.common.ImageDetails;
import image.common.UserParameters;
import image.execution.Execute;
import image.execution.Export;
import image.execution.ExportListener;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import layout.components.CustomButton;
import net.coobird.thumbnailator.Thumbnails;

@SuppressWarnings("serial")
public class Layout extends JFrame implements PhDocListener, ExportListener {

	public PhDocListener listener;
	public ExportListener expo;

	private static volatile ResultInformation resultData;
	private UserParameters userSettings;
	private CustomButton addButton;
	private CustomButton removeButton;
	private CustomButton advancedButton;
	private CustomButton startButton;
	private CustomButton helpButton;
	private CustomButton pauseButton;
	private CustomButton stopButton;
	private JButton similarLabel;
	private JButton eventLabel;
	private JButton locationLabel;
	private JButton duplicatesLabel;
	private JButton trashLabel;
	private JButton facesLabel;
	private JScrollPane foldersScrollPane;
	private JTable foldersTable;
	private JLabel groupByLabel;
	private JLabel findLabel;
	private JLabel resultTitleLabel;
	private JLabel resultDebugLabel;
	private JLabel percentageLabel;
	private JCheckBox duplicatesCheckBox;
	private JCheckBox eventCheckBox;
	private JCheckBox facesCheckBox;
	private JCheckBox locationCheckBox;
	private JCheckBox similarCheckBox;
	private JCheckBox trashCheckBox;
	private Set<File> foldersSet;
	private Set<String> trashImages;
	private Set<String> facesImages;
	private JPanel resultPanel;
	private JPanel startPanel;
	private ResultDisplayPane similarPane;
	private ResultDisplayPane eventPane;
	private ResultDisplayPane locationPane;
	private ResultDisplayPane duplicatesPane;
	private ResultDisplayPane trashPane;
	private ResultDisplayPane facesPane;
	private JSeparator simSeparator;
	private JSeparator eventSeparator;
	private JSeparator locSeparator;
	private JSeparator dupSeparator;
	private JSeparator trashSeparator;
	private JSeparator facesSeparator;

	private Execute execute;

	private Font bigFont;

	public static Color color_backgrd = new Color(238, 238, 238);
	public static Color color_Sim = new Color(240, 98, 146);
	public static Color color_Dup = new Color(100, 181, 246);
	public static Color color_Trash = new Color(165, 214, 167);
	public static Color color_Event = new Color(149, 117, 205);
	public static Color color_Loc = new Color(128, 222, 234);
	public static Color color_Face = new Color(255, 191, 145);

	public static final String SIM = "sim", DUP = "dup", TRASH = "trash",
			EVENT = "event", LOC = "loc", FACE = "face";

	public Layout() {
		initComponents();
	}

	public UserParameters getUserParams() {
		return userSettings;
	}

	/*
	 * initiate components on start of application
	 */
	private void initComponents() {
		listener = this;
		expo = this;
		execute = null;

		// --------------------------------------------------------------------------------
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			System.setProperty("awt.useSystemAAFontSettings", "on");
			System.setProperty("swing.aatext", "true");
		} catch (Exception e) {
		}

		// ----------------------------------------------------------------------------

		resultDebugLabel = new JLabel();
		resultTitleLabel = new JLabel();
		groupByLabel = new JLabel();
		findLabel = new JLabel();
		percentageLabel = new JLabel();

		resultData = new ResultInformation();
		userSettings = new UserParameters();
		userSettings.folders = new File[0];
		foldersScrollPane = new JScrollPane();
		foldersTable = new JTable();
		foldersSet = new HashSet<File>();

		similarCheckBox = new JCheckBox();
		eventCheckBox = new JCheckBox();
		locationCheckBox = new JCheckBox();
		duplicatesCheckBox = new JCheckBox();
		trashCheckBox = new JCheckBox();
		facesCheckBox = new JCheckBox();

		advancedButton = new CustomButton();
		startButton = new CustomButton();
		addButton = new CustomButton();
		removeButton = new CustomButton();
		helpButton = new CustomButton();
		pauseButton = new CustomButton();
		stopButton = new CustomButton();

		startPanel = new JPanel(new GridBagLayout());
		resultPanel = new JPanel();
		similarPane = new ResultDisplayPane(IV.SIMILAR, this);
		eventPane = new ResultDisplayPane(IV.GROUP_EVENT, this);
		locationPane = new ResultDisplayPane(IV.GROUP_LOC, this);
		duplicatesPane = new ResultDisplayPane(IV.DUPLICATE, this);
		trashPane = new ResultDisplayPane(IV.TRASH, this);
		facesPane = new ResultDisplayPane(IV.GROUP_FACE, this);

		simSeparator = getSeparator();
		eventSeparator = getSeparator();
		locSeparator = getSeparator();
		dupSeparator = getSeparator();
		trashSeparator = getSeparator();
		facesSeparator = getSeparator();

		Font tabFont = null;
		Font tabGroupFont = null;

		JLabel titleLabel = new JLabel();
		JLabel iconLabel = getIconLabel();

		// ----------------------------------Fonts------------------------------------------
		try {
			Font.createFont(Font.TRUETYPE_FONT,
					new File(IV.RES + "fonts/SEGOE-SemiBold.TTF")).deriveFont(
							Font.PLAIN, 16f);
			tabFont = Font.createFont(Font.TRUETYPE_FONT,
					new File(IV.RES + "fonts/SEGOE.TTF")).deriveFont(
							Font.PLAIN, 20f);
			tabGroupFont = Font.createFont(Font.TRUETYPE_FONT,
					new File(IV.RES + "fonts/SEGOE-Bold.TTF")).deriveFont(
							Font.PLAIN, 15f);
			bigFont = Font.createFont(Font.TRUETYPE_FONT,
					new File(IV.RES + "fonts/SEGOE.TTF")).deriveFont(
							Font.PLAIN, 48f);
			Font title = Font.createFont(Font.TRUETYPE_FONT,
					new File(IV.RES + "fonts/AKADORA.TTF")).deriveFont(
							Font.PLAIN, 42f);
			titleLabel.setFont(title);
		} catch (FontFormatException | IOException e1) {
			try {
				Font.createFont(
						Font.TRUETYPE_FONT,
						this.getClass()
						.getClassLoader()
						.getResourceAsStream("fonts/SEGOE-SemiBold.TTF"))
						.deriveFont(Font.PLAIN, 16f);
				tabFont = Font.createFont(
						Font.TRUETYPE_FONT,
						this.getClass().getClassLoader()
						.getResourceAsStream("fonts/SEGOE.TTF"))
						.deriveFont(Font.PLAIN, 19f);
				tabGroupFont = Font.createFont(
						Font.TRUETYPE_FONT,
						this.getClass().getClassLoader()
						.getResourceAsStream("fonts/SEGOE-Bold.TTF"))
						.deriveFont(Font.PLAIN, 15f);
				bigFont = Font.createFont(
						Font.TRUETYPE_FONT,
						this.getClass().getClassLoader()
						.getResourceAsStream("fonts/SEGOE.TTF"))
						.deriveFont(Font.PLAIN, 48f);
				Font title = Font.createFont(
						Font.TRUETYPE_FONT,
						this.getClass().getClassLoader()
						.getResourceAsStream("fonts/AKADORA.TTF"))
						.deriveFont(Font.PLAIN, 42f);
				titleLabel.setFont(title);
			} catch (FontFormatException | IOException e2) {
				tabFont = new Font("Calibri", 0, 22);
				tabGroupFont = new Font("Calibri", 0, 14);
				bigFont = new Font("Calibri", 0, 48);
			}
		}

		// ----------------------------------------------------------------------------
//		percentageLabel.setText("0 %");
		progressUpdate(0);
		percentageLabel.setFont(bigFont);
		percentageLabel.setPreferredSize(new Dimension(120, 92));

		groupByLabel.setFont(tabGroupFont);
		groupByLabel.setForeground(new Color(150, 150, 150));
		groupByLabel.setText("Group By:");

		findLabel.setFont(tabGroupFont);
		findLabel.setForeground(new Color(150, 150, 150));
		findLabel.setText("Find:");

		titleLabel.setText("PhotoDoc");
		titleLabel.setForeground(IV.color_text);

		simSeparator.setBackground(color_Sim);
		simSeparator.setForeground(color_Sim);

		// ----------------------------------------------------------------------------
		setupWindow();
		setupFoldersTable();

		setupResultsPanel();
		setupResultTitleLabels();
		setupResultsTabsLabels(tabFont);

		setupCheckBoxes();
		setupButtons();

		// ---------------------------------------------------------------------------

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addContainerGap()
								.addGroup(
										layout.createParallelGroup(
												GroupLayout.Alignment.LEADING)
												.addGroup(
														layout.createSequentialGroup()
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.LEADING)
																				.addGroup(
																						layout.createSequentialGroup()
																								.addComponent(
																										resultTitleLabel,
																										GroupLayout.DEFAULT_SIZE,
																										537,
																										Short.MAX_VALUE)
																								.addGap(0,
																										0,
																										0)
																								.addComponent(
																										resultDebugLabel,
																										GroupLayout.DEFAULT_SIZE,
																										229,
																										Short.MAX_VALUE)
																								.addGap(0,
																										0,
																										0))
																				.addComponent(
																						resultPanel,
																						GroupLayout.DEFAULT_SIZE,
																						GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE))
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.LEADING,
																				false)
																				.addGroup(
																						layout.createSequentialGroup()
																								.addPreferredGap(
																										LayoutStyle.ComponentPlacement.RELATED)
																								.addGroup(
																										layout.createParallelGroup(
																												GroupLayout.Alignment.LEADING)
																												.addComponent(
																														findLabel)))
																				.addGroup(
																						layout.createSequentialGroup()
																								.addComponent(
																										duplicatesLabel)
																								.addPreferredGap(
																										LayoutStyle.ComponentPlacement.RELATED,
																										GroupLayout.DEFAULT_SIZE,
																										Short.MAX_VALUE)
																								.addComponent(
																										duplicatesCheckBox))
																				.addGroup(
																						layout.createSequentialGroup()
																								.addGroup(
																										layout.createParallelGroup(
																												GroupLayout.Alignment.TRAILING,
																												false)
																												.addComponent(
																														facesSeparator,
																														GroupLayout.Alignment.LEADING)
																												.addComponent(
																														trashSeparator,
																														GroupLayout.Alignment.LEADING)
																												.addComponent(
																														dupSeparator,
																														GroupLayout.Alignment.LEADING)
																												.addComponent(
																														simSeparator,
																														GroupLayout.Alignment.LEADING)
																												.addComponent(
																														eventSeparator,
																														GroupLayout.Alignment.LEADING)
																												.addComponent(
																														locSeparator,
																														GroupLayout.Alignment.LEADING)
																												.addGroup(
																														layout.createSequentialGroup()
																																.addGap(0,
																																		0,
																																		0)
																																.addGroup(
																																		layout.createParallelGroup(
																																				GroupLayout.Alignment.TRAILING)
																																				.addGroup(
																																						layout.createSequentialGroup()
																																								.addGap(0,
																																										0,
																																										Short.MAX_VALUE)
																																								.addComponent(
																																										facesLabel))
																																				.addGroup(
																																						GroupLayout.Alignment.LEADING,
																																						layout.createSequentialGroup()
																																								.addGroup(
																																										layout.createParallelGroup(
																																												GroupLayout.Alignment.TRAILING)
																																												.addComponent(
																																														trashLabel,
																																														GroupLayout.Alignment.LEADING)
																																												.addComponent(
																																														eventLabel,
																																														GroupLayout.Alignment.LEADING)
																																												.addComponent(
																																														locationLabel,
																																														GroupLayout.Alignment.LEADING))
																																								.addGap(0,
																																										0,
																																										Short.MAX_VALUE))))
																												.addGroup(
																														layout.createSequentialGroup()
																																.addGroup(
																																		layout.createParallelGroup(
																																				GroupLayout.Alignment.LEADING)
																																				.addGroup(
																																						layout.createSequentialGroup()
																																								.addPreferredGap(
																																										LayoutStyle.ComponentPlacement.RELATED)
																																								.addComponent(
																																										groupByLabel)
																																								.addGap(17,
																																										17,
																																										17))
																																				.addComponent(
																																						similarLabel,
																																						Alignment.LEADING))
																																.addGap(0,
																																		0,
																																		Short.MAX_VALUE)))
																								.addGap(20,
																										20,
																										20)
																								.addGroup(
																										layout.createParallelGroup(
																												GroupLayout.Alignment.LEADING)
																												.addComponent(
																														similarCheckBox,
																														GroupLayout.Alignment.TRAILING)
																												.addComponent(
																														eventCheckBox,
																														GroupLayout.Alignment.TRAILING)
																												.addComponent(
																														locationCheckBox,
																														GroupLayout.Alignment.TRAILING)
																												.addComponent(
																														trashCheckBox,
																														GroupLayout.Alignment.TRAILING)
																												.addComponent(
																														facesCheckBox,
																														GroupLayout.Alignment.TRAILING)))))
												.addGroup(
														layout.createSequentialGroup()
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.LEADING)
																				.addComponent(
																						foldersScrollPane)
																				.addGroup(
																						layout.createSequentialGroup()
																								.addGap(1,
																										1,
																										1)
																								.addComponent(
																										iconLabel,
																										GroupLayout.PREFERRED_SIZE,
																										64,
																										GroupLayout.PREFERRED_SIZE)
																								.addGap(18,
																										18,
																										18)
																								.addComponent(
																										titleLabel,
																										GroupLayout.PREFERRED_SIZE,
																										205,
																										GroupLayout.PREFERRED_SIZE)
																								.addGap(379,
																										379,
																										379)))
																.addPreferredGap(
																		LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.LEADING,
																				false)
																				.addComponent(
																						addButton,
																						GroupLayout.PREFERRED_SIZE,
																						60,
																						GroupLayout.PREFERRED_SIZE)
																				.addComponent(
																						removeButton,
																						GroupLayout.PREFERRED_SIZE,
																						0,
																						Short.MAX_VALUE))
																.addPreferredGap(
																		LayoutStyle.ComponentPlacement.RELATED,
																		GroupLayout.DEFAULT_SIZE,
																		Short.MAX_VALUE)
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.LEADING)
																				.addGroup(
																						GroupLayout.Alignment.TRAILING,
																						layout.createParallelGroup(
																								GroupLayout.Alignment.LEADING)
																								.addComponent(
																										startPanel,
																										GroupLayout.PREFERRED_SIZE,
																										171,
																										GroupLayout.PREFERRED_SIZE)
																								.addComponent(
																										advancedButton,
																										GroupLayout.Alignment.TRAILING,
																										GroupLayout.PREFERRED_SIZE,
																										171,
																										GroupLayout.PREFERRED_SIZE))
																				.addComponent(
																						helpButton,
																						GroupLayout.Alignment.TRAILING,
																						GroupLayout.PREFERRED_SIZE,
																						90,
																						GroupLayout.PREFERRED_SIZE))))
								.addContainerGap()));
		layout.setVerticalGroup(layout
				.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addGroup(
										layout.createParallelGroup(
												GroupLayout.Alignment.LEADING)
												.addGroup(
														layout.createSequentialGroup()
																.addContainerGap()
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.TRAILING)
																				.addGroup(
																						layout.createParallelGroup(
																								GroupLayout.Alignment.LEADING)
																								.addComponent(
																										helpButton,
																										GroupLayout.PREFERRED_SIZE,
																										40,
																										GroupLayout.PREFERRED_SIZE)
																								.addComponent(
																										titleLabel,
																										GroupLayout.PREFERRED_SIZE,
																										56,
																										GroupLayout.PREFERRED_SIZE))
																				.addComponent(
																						iconLabel,
																						GroupLayout.PREFERRED_SIZE,
																						58,
																						GroupLayout.PREFERRED_SIZE))
																.addGap(30, 30,
																		30)
																.addComponent(
																		foldersScrollPane,
																		GroupLayout.PREFERRED_SIZE,
																		124,
																		GroupLayout.PREFERRED_SIZE))
												.addGroup(
														layout.createSequentialGroup()
																.addGap(100,
																		100,
																		100)
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.LEADING,
																				false)
																				.addComponent(
																						advancedButton,
																						GroupLayout.DEFAULT_SIZE,
																						GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE)
																				.addComponent(
																						removeButton,
																						GroupLayout.DEFAULT_SIZE,
																						GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE))
																.addPreferredGap(
																		LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.LEADING)
																				.addComponent(
																						addButton,
																						GroupLayout.PREFERRED_SIZE,
																						92,
																						GroupLayout.PREFERRED_SIZE)
																				.addComponent(
																						startPanel,
																						GroupLayout.PREFERRED_SIZE,
																						92,
																						GroupLayout.PREFERRED_SIZE))))
								.addGap(15, 15, 15)
								.addGroup(
										layout.createParallelGroup(
												GroupLayout.Alignment.TRAILING)
												.addComponent(
														resultTitleLabel,
														GroupLayout.PREFERRED_SIZE,
														32,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(
														resultDebugLabel,
														GroupLayout.PREFERRED_SIZE,
														32,
														GroupLayout.PREFERRED_SIZE))
								.addGap(0, 0, 0)
								.addGroup(
										layout.createParallelGroup(
												GroupLayout.Alignment.LEADING)
												.addGroup(
														layout.createSequentialGroup()
																.addComponent(
																		groupByLabel)
																.addPreferredGap(
																		LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.LEADING)
																				.addComponent(
																						similarLabel,
																						GroupLayout.Alignment.TRAILING)
																				.addComponent(
																						similarCheckBox,
																						GroupLayout.Alignment.TRAILING))
																.addComponent(
																		simSeparator,
																		GroupLayout.PREFERRED_SIZE,
																		2,
																		GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(
																		LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.LEADING,
																				false)
																				.addComponent(
																						eventCheckBox,
																						GroupLayout.DEFAULT_SIZE,
																						GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE)
																				.addComponent(
																						eventLabel))
																.addComponent(
																		eventSeparator,
																		GroupLayout.PREFERRED_SIZE,
																		2,
																		GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(
																		LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.LEADING,
																				false)
																				.addComponent(
																						locationCheckBox,
																						GroupLayout.DEFAULT_SIZE,
																						GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE)
																				.addComponent(
																						locationLabel))
																.addComponent(
																		locSeparator,
																		GroupLayout.PREFERRED_SIZE,
																		2,
																		GroupLayout.PREFERRED_SIZE)
																.addGap(18, 18,
																		18)
																.addComponent(
																		findLabel)
																.addPreferredGap(
																		LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.LEADING,
																				false)
																				.addComponent(
																						duplicatesCheckBox,
																						GroupLayout.DEFAULT_SIZE,
																						GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE)
																				.addComponent(
																						duplicatesLabel))
																.addComponent(
																		dupSeparator,
																		GroupLayout.PREFERRED_SIZE,
																		2,
																		GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(
																		LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.LEADING,
																				false)
																				.addComponent(
																						trashCheckBox,
																						GroupLayout.DEFAULT_SIZE,
																						GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE)
																				.addComponent(
																						trashLabel))
																.addComponent(
																		trashSeparator,
																		GroupLayout.PREFERRED_SIZE,
																		2,
																		GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(
																		LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				GroupLayout.Alignment.LEADING,
																				false)
																				.addComponent(
																						facesCheckBox,
																						GroupLayout.DEFAULT_SIZE,
																						GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE)
																				.addComponent(
																						facesLabel))
																.addComponent(
																		facesSeparator,
																		GroupLayout.PREFERRED_SIZE,
																		2,
																		GroupLayout.PREFERRED_SIZE))
												.addComponent(
														resultPanel,
														GroupLayout.DEFAULT_SIZE,
														GroupLayout.DEFAULT_SIZE,
														Short.MAX_VALUE))
								.addContainerGap()));

		pack();
	}

	/*
	 * sets any window settings
	 */
	private void setupWindow() {
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setTitle("PhotoDoc - The Image Management Software");
		setBackground(new Color(0, 0, 0));
		setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		setForeground(Color.pink);
		setMinimumSize(new Dimension(920, 626));
	}

	/*
	 * sets up all checkboxes in the GUI
	 */
	private void setupCheckBoxes() {
		similarCheckBox.setSelected(true);
		similarCheckBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				checkBoxActionPerformed(evt);

				if (similarCheckBox.isSelected() == true) {
					similarLabel.setForeground(Color.black);
					similarLabel.setEnabled(true);
				} else {
					similarLabel.setEnabled(false);
					similarLabel.setForeground(Color.gray);
				}
			}

		});

		eventCheckBox.setSelected(true);
		eventCheckBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				checkBoxActionPerformed(evt);
				if (eventCheckBox.isSelected() == true) {
					eventLabel.setForeground(Color.black);
					eventLabel.setEnabled(true);
				} else {
					eventLabel.setEnabled(false);
					eventLabel.setForeground(Color.gray);
				}
			}
		});

		locationCheckBox.setSelected(true);
		locationCheckBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				checkBoxActionPerformed(evt);
				if (locationCheckBox.isSelected() == true) {
					locationLabel.setForeground(Color.black);
					locationLabel.setEnabled(true);
				} else {
					locationLabel.setEnabled(false);
					locationLabel.setForeground(Color.gray);
				}
			}
		});

		duplicatesCheckBox.setSelected(true);
		duplicatesCheckBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				checkBoxActionPerformed(evt);
				if (duplicatesCheckBox.isSelected() == true) {
					duplicatesLabel.setForeground(Color.black);
					duplicatesLabel.setEnabled(true);
				} else {
					duplicatesLabel.setEnabled(false);
					duplicatesLabel.setForeground(Color.gray);
				}
			}
		});

		trashCheckBox.setSelected(true);
		trashCheckBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				checkBoxActionPerformed(evt);
				if (trashCheckBox.isSelected() == true) {
					trashLabel.setForeground(Color.black);
					trashLabel.setEnabled(true);
				} else {
					trashLabel.setEnabled(false);
					trashLabel.setForeground(Color.gray);
				}
			}
		});

		facesCheckBox.setSelected(true);
		facesCheckBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				checkBoxActionPerformed(evt);
				if (facesCheckBox.isSelected() == true) {
					facesLabel.setForeground(Color.black);
					facesLabel.setEnabled(true);
				} else {
					facesLabel.setEnabled(false);
					facesLabel.setForeground(Color.gray);
				}
			}
		});
	}

	/*
	 * called to set up the buttons within the GUI
	 */
	private void setupButtons() {
		helpButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IV.launchHelpPage();
			}
		});
		helpButton.setNoBorder();
		setIcon(helpButton, "help.png", 40);
		helpButton.setText("Help");
		helpButton.setIconTextGap(0);

		pauseButton.setNoBorder();
		pauseButton.setToolTipText("Pause");
		setIcon(pauseButton, "pause.png", 25);
		pauseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				pauseButtonActionPerformed(evt);
			}
		});

		stopButton.setNoBorder();
		stopButton.setToolTipText("Stop");
		setIcon(stopButton, "stop.png", 25);
		stopButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				stopButtonActionPerformed(evt);
			}
		});

		advancedButton.setText("Advanced Settings");
		setIcon(advancedButton, "settings.png", 25);
		advancedButton.setHorizontalTextPosition(JLabel.LEADING);
		advancedButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				advancedButtonActionPerformed(evt);
			}
		});

		startButton.setText("Start");
		startButton.setFont(bigFont);
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				startButtonActionPerformed(evt);
			}
		});

		setIcon(addButton, "plus.png", 30);
		addButton.setVerticalAlignment(JLabel.TOP);
		addButton.setHorizontalAlignment(JLabel.CENTER);
		addButton.setVerticalTextPosition(JLabel.BOTTOM);
		addButton.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));

		addButton.setMargin(new Insets(0, -30, 0, -30));
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				addButtonActionPerformed(evt);
			}
		});

		removeButton.setText("");
		setIcon(removeButton, "minus.png", 30);
		removeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				removeButtonActionPerformed(evt, foldersTable.getSelectedRow());
			}
		});

		updateStartPanel(false);
	}

	/*
	 * creates the tabs for the custom tab system.
	 */
	private void setupResultsTabsLabels(Font tabFont) {

		similarLabel = getTabPanel(tabFont, "Similar");
		similarLabel.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				if (similarLabel.getForeground() != Color.gray) {
					displayPanel(SIM);
				}
			}

			@Override
			public void mouseEntered(java.awt.event.MouseEvent evt) {
				if (similarLabel.getForeground() != Color.gray) {
					simSeparator.setBackground(color_Sim);
					simSeparator.setForeground(color_Sim);
				}
			}

			@Override
			public void mouseExited(java.awt.event.MouseEvent evt) {
				if (similarLabel.getForeground() != Color.gray
						&& !resultPanel.getName().equals(SIM)) {
					simSeparator.setBackground(color_backgrd);
					simSeparator.setForeground(color_backgrd);
				}
			}

		});

		eventLabel = getTabPanel(tabFont, "Event");
		eventLabel.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				if (eventLabel.getForeground() != Color.gray) {
					displayPanel(EVENT);
				}
			}

			@Override
			public void mouseEntered(java.awt.event.MouseEvent evt) {
				if (eventLabel.getForeground() != Color.gray) {
					eventSeparator.setBackground(color_Event);
					eventSeparator.setForeground(color_Event);
				}
			}

			@Override
			public void mouseExited(java.awt.event.MouseEvent evt) {
				if (eventLabel.getForeground() != Color.gray
						&& !resultPanel.getName().equals(EVENT)) {
					eventSeparator.setBackground(color_backgrd);
					eventSeparator.setForeground(color_backgrd);
				}
			}
		});

		locationLabel = getTabPanel(tabFont, "Location");
		locationLabel.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				if (locationLabel.getForeground() != Color.gray) {
					displayPanel(LOC);
				}
			}

			@Override
			public void mouseEntered(java.awt.event.MouseEvent evt) {
				if (locationLabel.getForeground() != Color.gray) {
					locSeparator.setBackground(color_Loc);
					locSeparator.setForeground(color_Loc);
				}
			}

			@Override
			public void mouseExited(java.awt.event.MouseEvent evt) {
				if (locationLabel.getForeground() != Color.gray
						&& !resultPanel.getName().equals(LOC)) {
					locSeparator.setBackground(color_backgrd);
					locSeparator.setForeground(color_backgrd);
				}
			}
		});

		duplicatesLabel = getTabPanel(tabFont, "Duplicate");
		duplicatesLabel.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				if (duplicatesLabel.getForeground() != Color.gray) {
					displayPanel(DUP);
					;
				}
			}

			@Override
			public void mouseEntered(java.awt.event.MouseEvent evt) {
				if (duplicatesLabel.getForeground() != Color.gray) {
					dupSeparator.setBackground(color_Dup);
					dupSeparator.setForeground(color_Dup);
				}
			}

			@Override
			public void mouseExited(java.awt.event.MouseEvent evt) {
				if (duplicatesLabel.getForeground() != Color.gray
						&& !resultPanel.getName().equals(DUP)) {
					dupSeparator.setBackground(color_backgrd);
					dupSeparator.setForeground(color_backgrd);
				}
			}
		});

		trashLabel = getTabPanel(tabFont, "Trash");
		trashLabel.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				if (trashLabel.getForeground() != Color.gray) {
					// displayTrashPanel();
					displayPanel(TRASH);
				}
			}

			@Override
			public void mouseEntered(java.awt.event.MouseEvent evt) {
				if (trashLabel.getForeground() != Color.gray) {
					trashSeparator.setBackground(color_Trash);
					trashSeparator.setForeground(color_Trash);
				}
			}

			@Override
			public void mouseExited(java.awt.event.MouseEvent evt) {
				if (trashLabel.getForeground() != Color.gray
						&& !resultPanel.getName().equals(TRASH)) {
					trashSeparator.setBackground(color_backgrd);
					trashSeparator.setForeground(color_backgrd);
				}
			}
		});

		facesLabel = getTabPanel(tabFont, "Face");
		facesLabel.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				if (facesLabel.getForeground() != Color.gray) {
					displayPanel(FACE);
				}
			}

			@Override
			public void mouseEntered(java.awt.event.MouseEvent evt) {
				if (facesLabel.getForeground() != Color.gray) {
					facesSeparator.setBackground(color_Face);
					facesSeparator.setForeground(color_Face);
				}
			}

			@Override
			public void mouseExited(java.awt.event.MouseEvent evt) {
				if (facesLabel.getForeground() != Color.gray
						&& !resultPanel.getName().equals(FACE)) {
					facesSeparator.setBackground(color_backgrd);
					facesSeparator.setForeground(color_backgrd);
				}
			}
		});
	}

	/*
	 * creates the title labels for the top of the results display
	 */
	private void setupResultTitleLabels() {
		resultDebugLabel.setForeground(IV.color_white_t_90);
		resultDebugLabel.setBackground(color_Sim);
		resultDebugLabel.setFont(new Font("Calibri", Font.BOLD, 18));
		resultDebugLabel.setText("");
		resultDebugLabel.setBorder(BorderFactory
				.createEmptyBorder(5, 15, 2, 15));
		resultDebugLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		resultDebugLabel.setVerticalTextPosition(JLabel.CENTER);
		resultDebugLabel.setOpaque(true);

		resultTitleLabel.setForeground(Color.white);
		resultTitleLabel.setBackground(color_Sim);
		resultTitleLabel.setFont(new Font("Calibri", Font.BOLD, 18));
		resultTitleLabel.setText("Similar Images - ("
				+ resultData.numberOfSimilarImages + '/'
				+ resultData.numberOfImages + " images) - "
				+ resultData.numberOfSimilarGroups + " similar groups found.");
		resultTitleLabel.setBorder(BorderFactory
				.createEmptyBorder(5, 15, 2, 15));
		resultTitleLabel.setVerticalTextPosition(JLabel.CENTER);
		resultTitleLabel.setOpaque(true);
	}

	/*
	 * creates the folder table which displays the selected folders on which to run
	 * the application
	 */
	private void setupFoldersTable() {
		foldersTable.addMouseListener(new MouseAdapter() {
			private JPopupMenu popup;
			private int row;

			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popUp(e);
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popUp(e);
				}
			}

			private void popUp(MouseEvent e) {
				if (popup == null)
					buildPopup();
				JTable source = (JTable) e.getSource();
				row = source.rowAtPoint(e.getPoint());
				int column = source.columnAtPoint(e.getPoint());

				if (!source.isRowSelected(row))
					source.changeSelection(row, column, false, false);
				popup.show(e.getComponent(), e.getX(), e.getY());
			}

			private void buildPopup() {
				popup = new JPopupMenu();
				JMenuItem menuItem = new JMenuItem("Remove");
				menuItem.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						int[] selected = foldersTable.getSelectedRows();
						for (int i = 0; i < selected.length; i++) {
							removeButtonActionPerformed(e, selected[i]);
						}
					}
				});
				JMenuItem menuItem1 = new JMenuItem("Refresh");
				menuItem1.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						int[] selected = foldersTable.getSelectedRows();
						for (int i = 0; i < selected.length; i++) {
							System.out.println("row: " + selected[i]);
							DefaultTableModel model = (DefaultTableModel) foldersTable
									.getModel();
							model.setValueAt("...", selected[i], 1);
							model.setValueAt("...", selected[i], 2);

							final String filePath = (String) foldersTable
									.getValueAt(selected[i], 0);
							final int index = selected[i];
							Thread t = new Thread() {
								public void run() {
									int[] imAndFol = getImagesAndFolders(new File(
											filePath));
									DefaultTableModel model = (DefaultTableModel) foldersTable
											.getModel();
									model.setValueAt(imAndFol[1], index, 1);
									model.setValueAt(imAndFol[0], index, 2);
								}
							};
							t.start();

						}
					}
				});
				JMenuItem menuItem2 = new JMenuItem("Open Folder");
				menuItem2.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						try {
							int[] selected = foldersTable.getSelectedRows();
							for (int i = 0; i < selected.length; i++) {
								String filePath = (String) foldersTable
										.getValueAt(selected[i], 0);
								Desktop.getDesktop().open(new File(filePath));
							}
						} catch (IOException ex) {
							ex.printStackTrace();
						}
					}
				});
				JMenuItem menuItem3 = new JMenuItem(
						"Extract all images into...");
				menuItem3.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						final JFileChooser fileDialog = new JFileChooser();
						fileDialog
						.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
						fileDialog.setDialogTitle("Select save location");

						int returnVal = fileDialog.showSaveDialog(Layout.this);
						if (returnVal == JFileChooser.APPROVE_OPTION) {
							File directory = fileDialog.getSelectedFile();
							try {
								int[] selected = foldersTable.getSelectedRows();
								for (int i = 0; i < selected.length; i++) {
									String filePath = (String) foldersTable
											.getValueAt(selected[i], 0);
									updateInfoMsg(IV.IM_MOVED,
											resultPanel.getName(), null,
											"Moving...");

									Export.Move(IV.getImageFilesList(new File(
											filePath)), directory
											.getAbsolutePath(), expo);
									
									setAllFilePathinPanes(directory.getAbsolutePath());
								}
							} catch (Exception ex) {
								ex.printStackTrace();
							}
						}
					}
				});
				JMenuItem menuItem4 = new JMenuItem("Delete Empty folders");
				menuItem4.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						try {
							int[] selected = foldersTable.getSelectedRows();
							File[] fl = new File[selected.length];
							for (int i = 0; i < selected.length; i++) {
								fl[i] = new File((String) foldersTable
										.getValueAt(selected[i], 0));
							}
							updateInfoMsg(IV.IM_DELETED, resultPanel.getName(),
									null, "Deleting...");
							Export.DeleteEmptyFolders(fl, expo);
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
				});
				popup.add(menuItem2);
				popup.add(menuItem1);
				popup.add(menuItem);
				popup.add(menuItem3);
				popup.add(menuItem4);
			}
		});

		foldersTable.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "Folder Path", "Subfolders", "Images" }) {
			@SuppressWarnings("rawtypes")
			Class[] types = new Class[] { String.class, Integer.class,
				Integer.class };

			@SuppressWarnings({ "rawtypes", "unchecked" })
			public Class getColumnClass(int columnIndex) {
				return types[columnIndex];
			}

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		});

		setTableStyle();
	}

	/*
	 * sets up the results panel, this is where all images will be shown once
	 * categorized
	 */
	private void setupResultsPanel() {
		resultPanel.setBorder(BorderFactory.createLineBorder(color_Sim, 2));
		resultPanel.setOpaque(false);
		resultPanel.setLayout(new CardLayout());
		resultPanel.setName(SIM);

		similarPane.setSize(725, 330);
		eventPane.setSize(725, 330);
		locationPane.setSize(725, 330);
		duplicatesPane.setSize(725, 330);
		trashPane.setSize(725, 330);
		facesPane.setSize(725, 330);

		resultPanel.add(similarPane, SIM);
		resultPanel.add(eventPane, EVENT);
		resultPanel.add(locationPane, LOC);
		resultPanel.add(duplicatesPane, DUP);
		resultPanel.add(trashPane, TRASH);
		resultPanel.add(facesPane, FACE);
	}

	/*
	 * sets all the custom settings for the folders table
	 */
	private void setTableStyle() {
		foldersTable.setAutoscrolls(false);
		foldersTable.setShowVerticalLines(false);
		foldersTable.getTableHeader().setReorderingAllowed(false);
		foldersTable.getColumnModel().getColumn(0).setPreferredWidth(400);
		foldersTable.getColumnModel().getColumn(1).setPreferredWidth(20);
		foldersTable.getColumnModel().getColumn(2).setPreferredWidth(20);
		foldersTable.getColumnModel().setColumnMargin(0);

		foldersTable.getTableHeader().setOpaque(false);
		foldersTable.getTableHeader()
		.setBackground(IV.color_background_lighter);
		foldersTable.getTableHeader().setForeground(Color.GRAY);
		foldersTable.setOpaque(false);
		foldersTable.setBackground(null);
		foldersTable.setForeground(Color.DARK_GRAY);

		foldersTable.setSelectionBackground(IV.color_darker_background);
		foldersTable.setSelectionForeground(Color.DARK_GRAY);

		foldersTable.setRowHeight(24);
		foldersTable.setRowMargin(0);
		foldersTable.setBorder(BorderFactory.createEmptyBorder(5, 15, 5, 15));
		foldersTable.setBorder(new CompoundBorder(new EmptyBorder(new Insets(1,
				14, 1, 14)), foldersTable.getBorder()));

		DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
		renderer.setHorizontalAlignment(JLabel.LEFT);
		renderer.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		foldersTable.getColumnModel().getColumn(0).setCellRenderer(renderer);
		foldersTable.getColumnModel().getColumn(1).setCellRenderer(renderer);
		foldersTable.getColumnModel().getColumn(2).setCellRenderer(renderer);

		foldersScrollPane.setViewportView(foldersTable);
		foldersScrollPane.setBorder(BorderFactory.createLineBorder(
				IV.color_darker_background_darker, 2));
		foldersScrollPane.setBackground(IV.color_translucent_white);
		foldersScrollPane.getViewport().setBackground(
				IV.color_background_lighter);
	}

	/*
	 * makes all separators invisible, these are the lines underneath the tabs
	 */
	private void setSeparatorInvisible() {
		simSeparator.setBackground(color_backgrd);
		simSeparator.setForeground(color_backgrd);
		eventSeparator.setBackground(color_backgrd);
		eventSeparator.setForeground(color_backgrd);
		locSeparator.setBackground(color_backgrd);
		locSeparator.setForeground(color_backgrd);
		dupSeparator.setBackground(color_backgrd);
		dupSeparator.setForeground(color_backgrd);
		trashSeparator.setBackground(color_backgrd);
		trashSeparator.setForeground(color_backgrd);
		facesSeparator.setBackground(color_backgrd);
		facesSeparator.setForeground(color_backgrd);
	}

	/*
	 * returns a label containing the PhotoDoc icon
	 */
	private JLabel getIconLabel() {
		JLabel iconLabel = new JLabel();
		BufferedImage myPicture;
		try {
			setIconImage((new ImageIcon(IV.RES + "images/logo-big.png"))
					.getImage());
			myPicture = ImageIO.read(new File(IV.RESIMG + "logo-black.png"));

			myPicture = Thumbnails.of(myPicture).size(42, 42).asBufferedImage();
			myPicture = Thumbnails.of(myPicture).size(54, 54).asBufferedImage();
			iconLabel = new JLabel(new ImageIcon(myPicture));
		} catch (IOException e) {
			System.out
			.println("Image not found! Trying jar location instead. (logo-big.png)");
			try {
				setIconImage((new ImageIcon(this.getClass().getClassLoader()
						.getResource("images/logo-big.png"))).getImage());
				myPicture = ImageIO.read(this.getClass().getClassLoader()
						.getResource("images/logo-black.png"));
				myPicture = Thumbnails.of(myPicture).size(54, 54)
						.asBufferedImage();
				iconLabel = new JLabel(new ImageIcon(myPicture));
			} catch (IOException ee) {
				ee.printStackTrace();
			}
		}
		return iconLabel;
	}

	/*
	 * returns a separator for use underneath a tab
	 */
	private JSeparator getSeparator() {
		JSeparator sep = new JSeparator();
		sep.setToolTipText("");
		sep.setOpaque(true);
		sep.setBackground(color_backgrd);
		sep.setForeground(color_backgrd);
		sep.setVerifyInputWhenFocusTarget(false);
		return sep;
	}

	/*
	 * returns a JButton to act as a tab for the custom tab system
	 */

	private JButton getTabPanel(Font tabFont, String name) {
		JButton label = new CustomButton();
		label.setMinimumSize(new Dimension(150, 30));
		label.setHorizontalAlignment(SwingConstants.LEFT);
		label.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
		label.setFont(tabFont);
		label.setText(name);
		return label;
	}


	/*
	 * sets the button recieved to show the image sent with path "name"
	 */
	public void setIcon(CustomButton butt, String name, int size) {
		try {
			BufferedImage image = Thumbnails.of(IV.RESIMG + name)
					.size((int) size, (int) size).asBufferedImage();
			butt.setIcon(new ImageIcon(image));
		} catch (IOException e) {
			System.out.println("Image not found! Trying jar location instead: "
					+ name);
			try {
				BufferedImage image = Thumbnails
						.of(this.getClass().getClassLoader()
								.getResource("images/" + name))
								.size((int) size, (int) size).asBufferedImage();
				butt.setIcon(new ImageIcon(image));
			} catch (IOException ee) {
				ee.printStackTrace();
			}
		}
	}


	/*
	 * when the advanced button is pressed displays a JDialog for advanced settings
	 */
	private void advancedButtonActionPerformed(ActionEvent evt) {
		this.setEnabled(false);
		final AdvancedFrame advancedSettings = new AdvancedFrame(userSettings);
		advancedSettings.addWindowListener(new WindowAdapter() {
			public void windowClosed(WindowEvent e) {
				setVisible(true);
				if (advancedSettings.newValuesAccepted) {
					userSettings = advancedSettings.newUserSettings;
				}
				activateFinished();
			}
		});
	}


	/*
	 * called to re-enable this frame
	 */
	private void activateFinished() {
		this.setEnabled(true);
	}


	/*
	 * returns the total number of images contained within all folders in the folders table
	 */
	public int getNumberOfImages() {
		int count = 0;

		for (int i = 0; i < foldersTable.getRowCount(); i++) {
			while (true) {
				if (foldersTable.getModel().getValueAt(i, 2) instanceof Integer) {
					break;
				}
			}
			int numImages = (int) foldersTable.getModel().getValueAt(i, 2);
			count += numImages;
		}

		return count;
	}

	/*
	 * disables buttons that cannot be used whilst exection is in progress
	 * and removes the start button and replaces with a percentage label
	 * and a pause and stop button.
	 * then starts the execute class and runs the classification of selected images
	 */
	private void startButtonActionPerformed(ActionEvent evt) {
		reset();
		enableWhenNotRunning(false);

		folderSetToUserSettings();
		resultData.numberOfImages = getNumberOfImages();

		Thread t = new Thread() {

			public void run() {
				execute = new Execute(listener, userSettings);
				progressUpdate(0);
				execute.run();
				return;
			}
		};
		t.start();
	}

	/*
	 * change the contents of start panel depending on whether the application
	 * is running classification or not.
	 */
	private void updateStartPanel(boolean running) {
		startPanel.removeAll();
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1d;
		c.weighty = 1d;
		c.anchor = GridBagConstraints.CENTER;
		if (running) {
			c.gridx = 0;
			c.gridy = 0;
			c.gridheight = 2;
			c.gridwidth = 2;
			startPanel.add(percentageLabel, c);

			c.gridheight = 1;
			c.gridwidth = 1;
			c.gridx = 2;
			startPanel.add(pauseButton, c);

			c.gridy = 1;
			startPanel.add(stopButton, c);
		} else {
			startPanel.add(startButton, c);
		}
		startPanel.repaint();
	}

	/*
	 * translates the contents of the folders table to be in the user settings
	 * so they can be passed to execute
	 */
	private void folderSetToUserSettings() {
		File[] temp = new File[foldersSet.size()];
		int k = 0;
		for (File file : foldersSet) {
			temp[k] = file;
			k++;
		}
		userSettings.folders = temp;
	}

	/*
	 * resets the GUI, called when starting execution in case any data is left
	 * from the previous run
	 */
	private void reset() {
		setIcon(pauseButton, "pause.png", 25);
		progressUpdate(0);
//		percentageLabel.setText("0 %");

		stopPanels(false);
		pausePanels(false);

		resultData.reset();
		resultData = new ResultInformation();

		trashImages = new HashSet<String>();
		facesImages = new HashSet<String>();

		similarPane.resetPane();
		eventPane.resetPane();
		locationPane.resetPane();
		duplicatesPane.resetPane();
		trashPane.resetPane();
		facesPane.resetPane();
	}

	/*
	 * creates an instance of a JFileChooser when the add button is pressed
	 */
	private void addButtonActionPerformed(ActionEvent evt) {
		final JFileChooser fileDialog = new JFileChooser();
		fileDialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileDialog.setMultiSelectionEnabled(true);

		int returnVal = fileDialog.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			final File[] files = fileDialog.getSelectedFiles();
			for (int i = 0; i < files.length; i++) {
				final File file = files[i];
				// ----------------------------------if already
				// exist------------------------------------------
				boolean found = false;
				for (int j = 0; j < foldersTable.getRowCount(); j++) {
					if (foldersTable.getModel().getValueAt(j, 0)
							.equals(file.getAbsolutePath())) {
						found = true;
						break;
					}
				}
				// ----------------------------------evt------------------------------------------
				if (!found) {
					foldersSet.add(file);
					DefaultTableModel model = (DefaultTableModel) foldersTable
							.getModel();
					model.addRow(new Object[] { file.getAbsolutePath(), "...",
					"..." });
					final int index = foldersTable.getRowCount() - 1;

					Thread t = new Thread() {

						public void run() {
							int[] imAndFol = getImagesAndFolders(file);
							DefaultTableModel model = (DefaultTableModel) foldersTable
									.getModel();
							model.setValueAt(imAndFol[1], index, 1);
							model.setValueAt(imAndFol[0], index, 2);

							return;
						}
					};
					t.start();
				}
			}
		}
	}

	/*
	 * removes the currently selected row from the folder table
	 */
	private void removeButtonActionPerformed(ActionEvent evt, int row) {
		if (row != -1) {
			String filePath = (String) foldersTable.getValueAt(row, 0);
			File file = new File(filePath);
			foldersSet.remove(file);

			((DefaultTableModel) foldersTable.getModel()).removeRow(row);
		}
	}

	/*
	 * pauses execution or resumes execution depending on whether it is currently paused
	 * or not
	 */
	private void pauseButtonActionPerformed(ActionEvent evt) {
		if (pauseButton.getToolTipText().equals("Resume")) {
			execute.resume();
			pausePanels(false);
			pauseButton.setToolTipText("Pause");
			setIcon(pauseButton, "pause.png", 25);
		} else if (pauseButton.getToolTipText().equals("Pause")) {
			execute.pause();
			pausePanels(true);
			pauseButton.setToolTipText("Resume");
			setIcon(pauseButton, "resume.png", 25);
		}
	}

	/*
	 * depending on whether pausing or resuming this pausing or resumes any panels that
	 * are currently painting images
	 */
	private void pausePanels(boolean paused) {
		similarPane.paused = paused;
		eventPane.paused = paused;
		locationPane.paused = paused;
		duplicatesPane.paused = paused;
		trashPane.paused = paused;
		facesPane.paused = paused;
	}

	/*
	 * when stop press this stops panels paints, this is permanent and stops the exectuion
	 * entirely
	 */
	private void stopPanels(boolean stop) {
		similarPane.stop = stop;
		eventPane.stop = stop;
		locationPane.stop = stop;
		duplicatesPane.stop = stop;
		trashPane.stop = stop;
		facesPane.stop = stop;
	}

	/*
	 * stops execution and painting of images, cannot be resumed after this
	 */
	private void stopButtonActionPerformed(ActionEvent evt) {
		execute.stop();
		stopPanels(true);
	}

	/*
	 * returns the number of images and folders contained with the given folder
	 * and all of its sub folders
	 */
	private int[] getImagesAndFolders(File folder) {
		int[] results = new int[2];
		for (File file : folder.listFiles()) {
			if (IV.isImage(file)) {
				results[0]++;
			}
			if (file.isDirectory()) {
				int[] tmp = getImagesAndFolders(file);
				results[0] += tmp[0];
				results[1] += tmp[1] + 1;
			}
		}
		return results;
	}

	/*
	 * updates usersettings to contain the up to date version of which categories should be
	 * used when classifying
	 */
	private void checkBoxActionPerformed(ActionEvent e) {
		userSettings.doSimilar = similarCheckBox.isSelected();
		userSettings.doEventTime = eventCheckBox.isSelected();
		userSettings.doLocation = locationCheckBox.isSelected();
		userSettings.doDuplicate = duplicatesCheckBox.isSelected();
		userSettings.doTrash = trashCheckBox.isSelected();
		userSettings.doFaces = facesCheckBox.isSelected();
	}

	public static void main(String args[]) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				new Layout().setVisible(true);

			}
		});
	}

	// ----------------------------------callback
	// methods------------------------------------------
	/*
	 * listener for execute to send a progress update, updates value on percentage label
	 */
	@Override
	public void progressUpdate(final double progOpenCV) {
		Thread t = new Thread() {

			public void run() {
				String text;
				if (progOpenCV >= 100) {
					text = "100";
				} else {
					text = String.format("%.1f", progOpenCV) + "%";
				}

				percentageLabel.setText(text);

				return;
			}
		};
		t.start();
	}

	/*
	 * listens for execute to finish similar classification, populates pane with results
	 */
	@Override
	public void finishedSimilarSearch() {
		final HashMap<Integer, HashSet<String>> imageGroups = getGroupAsMap(
				Execute.imgDetails, IV.SIMILAR);

		int numImages = 0;
		for (Map.Entry<Integer, HashSet<String>> entry : imageGroups.entrySet()) {
			HashSet<String> group = entry.getValue();
			numImages += group.size();
		}
		resultData.numberOfSimilarGroups = imageGroups.size();
		resultData.numberOfSimilarImages = numImages;
		resultData.similarGroups = imageGroups;

		Thread t = new Thread() {

			public void run() {
				try {
					similarPane.addGroupedImages(imageGroups);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				return;
			}
		};
		t.start();
	}

	/*
	 * listens for execute to finish duplicate classification, populates pane with results
	 */
	@Override
	public void finishedDuplicateSearch() {
		final HashMap<Integer, HashSet<String>> imageGroups = getGroupAsMap(
				Execute.imgDetails, IV.DUPLICATE);

		int numImages = 0;
		for (Map.Entry<Integer, HashSet<String>> entry : imageGroups.entrySet()) {
			HashSet<String> group = entry.getValue();
			numImages += group.size();
		}
		resultData.numberOfDuplicateGroups = imageGroups.size();
		resultData.numberOfDuplicateImages = numImages;
		resultData.duplicateGroups = imageGroups;

		Thread t = new Thread() {

			public void run() {
				try {
					duplicatesPane.addGroupedImages(imageGroups);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				return;
			}
		};
		t.start();
	}

	/*
	 * listens for execute to finish trash classification, populates pane with results
	 */
	@Override
	public void finishedTrashSearch() {
		for (Map.Entry<String, ImageDetails> entry : Execute.imgDetails
				.entrySet()) {
			String image = entry.getKey();

			if (entry.getValue().cert_Trash > IV.CRThresh) {
				trashImages.add(image);
			}
		}

		resultData.numberOfTrashImages = trashImages.size();
		resultData.trashImages = trashImages;

		trashPane.addImages(trashImages);
	}

	/*
	 * listens for execute to finish event classification, populates pane with results
	 */
	@Override
	public void finishedEventTimeSearch() {
		final HashMap<Integer, HashSet<String>> imageGroups = getGroupAsMap(
				Execute.imgDetails, IV.GROUP_EVENT);

		int numImages = 0;
		for (Map.Entry<Integer, HashSet<String>> entry : imageGroups.entrySet()) {
			HashSet<String> group = entry.getValue();
			numImages += group.size();
		}
		resultData.numberOfEventGroups = imageGroups.size();
		resultData.numberOfEventImages = numImages;
		resultData.eventGroups = imageGroups;

		Thread t = new Thread() {

			public void run() {
				try {
					eventPane.addGroupedImages(imageGroups);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				return;
			}
		};
		t.start();
	}

	/*
	 * listens for execute to finish location classification, populates pane with results
	 */
	@Override
	public void finishedLocationSearch() {
		final HashMap<Integer, HashSet<String>> imageGroups = getGroupAsMap(
				Execute.imgDetails, IV.GROUP_LOC);

		int numImages = 0;
		for (Map.Entry<Integer, HashSet<String>> entry : imageGroups.entrySet()) {
			HashSet<String> group = entry.getValue();
			numImages += group.size();
		}
		resultData.numberOfLocationGroups = imageGroups.size();
		resultData.numberOfLocationImages = numImages;
		resultData.locationGroups = imageGroups;

		Thread t = new Thread() {

			public void run() {
				try {
					locationPane.addGroupedImages(imageGroups);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				return;
			}
		};
		t.start();
	}

	/*
	 * listens for execute to finish faces classification, populates pane with results
	 */
	@Override
	public void finishedFacesSearch() {
		for (Map.Entry<String, ImageDetails> entry : Execute.imgDetails
				.entrySet()) {
			String image = entry.getKey();

			if (entry.getValue().info_Faces > 0) {
				facesImages.add(image);
			}
		}

		resultData.numberOfFacesImages = facesImages.size();
		resultData.faceImages = facesImages;

		Thread t = new Thread() {

			public void run() {
				facesPane.addImages(facesImages);
				return;
			}
		};
		t.start();
	}

	/*
	 * listens for execute to finish everything. this then enables options and removes
	 * any run time buttons or labels
	 */
	@Override
	public void finished_All() {
		enableWhenNotRunning(true);

		refreshResultPanel();
	}

	/*
	 * enables/disables buttons depending on run state
	 */
	private void enableWhenNotRunning(boolean enable) {
		updateStartPanel(!enable);

		removeButton.setEnabled(enable);
		addButton.setEnabled(enable);
		advancedButton.setEnabled(enable);

		similarCheckBox.setEnabled(enable);
		eventCheckBox.setEnabled(enable);
		locationCheckBox.setEnabled(enable);
		duplicatesCheckBox.setEnabled(enable);
		trashCheckBox.setEnabled(enable);
		facesCheckBox.setEnabled(enable);
	}

	// ----------------------------------callbacks------------------------------------------

	/*
	 * returns groups of images as a hashmap of hashsets
	 */
	private HashMap<Integer, HashSet<String>> getGroupAsMap(
			Map<String, ImageDetails> imDetails, final int type) {
		HashMap<Integer, HashSet<String>> groupMembers = new HashMap<Integer, HashSet<String>>();

		for (Map.Entry<String, ImageDetails> entry : imDetails.entrySet()) {
			int imgGroup = entry.getValue().group_Sim;
			switch (type) {
			case IV.DUPLICATE:
				imgGroup = entry.getValue().group_Dup;
				break;
			case IV.SIMILAR:
				imgGroup = entry.getValue().group_Sim;
				break;
			case IV.GROUP_EVENT:
				imgGroup = entry.getValue().group_Event;
				break;
			case IV.GROUP_LOC:
				imgGroup = entry.getValue().group_Loc;
				break;
			}

			String fileName = entry.getKey();
			if (imgGroup != -1) {
				if (groupMembers.containsKey(imgGroup)) {
					groupMembers.get(imgGroup).add(fileName);
				} else {
					HashSet<String> nameSet = new HashSet<String>();
					nameSet.add(fileName);
					groupMembers.put(imgGroup, nameSet);
				}
			}
		}
		return groupMembers;
	}

	/*
	 * listener to inform that export has finished moving an image
	 */
	@Override
	public void exp_finishedMoving() {
	}

	/*
	 * listener to inform that export has finished deleting an image
	 */
	@Override
	public void exp_finishedDeleting() {
	}

	/*
	 * listener to inform that export could not find an image
	 */
	@Override
	public void exp_imageNotFound(String image) {
	}

	/*
	 * returns an arraylist of all images within a directory and its subdirectories
	 */
	public ArrayList<String> getAllImages(File directory) {
		ArrayList<String> images = new ArrayList<String>();

		for (File file : directory.listFiles()) {
			if (IV.isImage(file)) {
				images.add(file.getAbsolutePath());
			}
			if (file.isDirectory()) {
				ArrayList<String> tmp = getAllImages(file);
				images.addAll(tmp);
			}
		}
		return images;
	}

	/*
	 * change the currently displayed panel in the results panel
	 * tag is the name of the category type
	 */
	public void displayPanel(String tag) {
		resultPanel.setName(tag);

		CardLayout resultLayout = (CardLayout) resultPanel.getLayout();
		resultLayout.show(resultPanel, tag);

		setSeparatorInvisible();

		Color color = getCurrPaneColor();

		getCurrPaneSeparator().setBackground(color);
		getCurrPaneSeparator().setForeground(color);
		resultTitleLabel.setBackground(color);
		resultDebugLabel.setBackground(color);
		resultPanel.setBorder(BorderFactory.createLineBorder(color, 2));

		setCurrTitleText();

	}

	/*
	 *  sets the text in the title labels of the result panel depending on the
	 *  currently selected tab
	 */
	private void setCurrTitleText() {
		resultDebugLabel.setText("");
		if (resultPanel.getName().equals(SIM)) {

			resultTitleLabel.setText("Similar Images - ("
					+ resultData.numberOfSimilarImages + '/'
					+ resultData.numberOfImages + " images) - "
					+ resultData.numberOfSimilarGroups
					+ " similar groups found.");

		} else if (resultPanel.getName().equals(DUP)) {

			resultTitleLabel
			.setText("Duplicate Images - ("
					+ resultData.numberOfDuplicateImages
					+ '/'
					+ resultData.numberOfImages
					+ " images) - "
					+ (resultData.numberOfDuplicateImages - resultData.numberOfDuplicateGroups)
					+ " duplicate images found.");
		} else if (resultPanel.getName().equals(EVENT)) {
			resultTitleLabel.setText("Images by Events - ("
					+ resultData.numberOfEventImages + '/'
					+ resultData.numberOfImages + " images) - "
					+ resultData.numberOfEventGroups + " events found.");
		} else if (resultPanel.getName().equals(LOC)) {
			resultTitleLabel.setText("Images by Location - ("
					+ resultData.numberOfLocationImages + '/'
					+ resultData.numberOfImages + " images) - "
					+ resultData.numberOfLocationGroups + " locations found.");
		} else if (resultPanel.getName().equals(TRASH)) {
			resultTitleLabel.setText("Trash Images - ("
					+ resultData.numberOfTrashImages + '/'
					+ resultData.numberOfImages + " images)");

		} else if (resultPanel.getName().equals(FACE)) {
			resultTitleLabel.setText("Images Containing Faces - ("
					+ resultData.numberOfFacesImages + '/'
					+ resultData.numberOfImages + " image with faces found)");
		} else {
		}

	}

	/*
	 * returns the color that is associated with the currently selected tab
	 */
	private Color getCurrPaneColor() {
		if (resultPanel.getName().equals(SIM))
			return color_Sim;
		if (resultPanel.getName().equals(DUP))
			return color_Dup;
		if (resultPanel.getName().equals(EVENT))
			return color_Event;
		if (resultPanel.getName().equals(LOC))
			return color_Loc;
		if (resultPanel.getName().equals(TRASH))
			return color_Trash;
		if (resultPanel.getName().equals(FACE))
			return color_Face;
		return null;
	}

	/*
	 * returns the separator below the currently active tab
	 */
	private JSeparator getCurrPaneSeparator() {
		if (resultPanel.getName().equals(SIM))
			return simSeparator;
		if (resultPanel.getName().equals(DUP))
			return dupSeparator;
		if (resultPanel.getName().equals(EVENT))
			return eventSeparator;
		if (resultPanel.getName().equals(LOC))
			return locSeparator;
		if (resultPanel.getName().equals(TRASH))
			return trashSeparator;
		if (resultPanel.getName().equals(FACE))
			return facesSeparator;
		return null;
	}

	/*
	 * sets the display panel to show the currently active tab
	 */
	public void refreshResultPanel() {
		if (resultPanel.getName().equals(SIM))
			displayPanel(SIM);
		if (resultPanel.getName().equals(EVENT))
			displayPanel(EVENT);
		if (resultPanel.getName().equals(LOC))
			displayPanel(LOC);
		if (resultPanel.getName().equals(DUP))
			displayPanel(DUP);
		if (resultPanel.getName().equals(TRASH))
			displayPanel(TRASH);
		if (resultPanel.getName().equals(FACE))
			displayPanel(FACE);
	}

	/*
	 *  updates the informational label on the right of the result display title area
	 */
	public void updateInfoMsg(int msgType, final String op, final int num[],
			final String msg) {
		if (msgType == IV.IM_LOADED) {
			if (resultPanel.getName().equals(op)) {
				resultDebugLabel.setText("Loaded " + num[0] + "/"
						+ getCurrPaneTotImages());
				resultDebugLabel.repaint();
			}
		} else {
			if (resultPanel.getName().equals(op)) {
				resultDebugLabel.setText(msg);
				resultDebugLabel.repaint();
			}
			// automatically destroy message after 5 seconds.
			Thread t = new Thread() {
				public void run() {
					try {
						Thread.sleep(5000);
						if (resultDebugLabel.getText().equals(msg)) {
							resultDebugLabel.setText("");
						}
					} catch (InterruptedException e) {
					}

				}
			};
			t.start();
		}

	}

	/*
	 * returns the number of total images shown in the currently active tabs corresponding pane
	 */
	private int getCurrPaneTotImages() {
		if (resultPanel.getName().equals(SIM))
			return resultData.numberOfSimilarImages;
		if (resultPanel.getName().equals(DUP))
			return resultData.numberOfDuplicateImages;
		if (resultPanel.getName().equals(EVENT))
			return resultData.numberOfEventImages;
		if (resultPanel.getName().equals(LOC))
			return resultData.numberOfLocationImages;
		if (resultPanel.getName().equals(TRASH))
			return resultData.numberOfTrashImages;
		if (resultPanel.getName().equals(FACE))
			return resultData.numberOfFacesImages;
		return 0;
	}

	private void setAllFilePathinPanes(final String path) {
		try {
			 similarPane.updatePathToImages(path);
			 duplicatesPane.updatePathToImages(path);
			 eventPane.updatePathToImages(path);
			 locationPane.updatePathToImages(path);
			 trashPane.updatePathToImages(path);
			 facesPane.updatePathToImages(path);
		} catch (Exception e) {
		}
	}
	
	/*
	 * updates the results shown in other categories after any number of images are deleted
	 */
	@SuppressWarnings("rawtypes")
	public void updateResults(ArrayList<String> filesToDelete) {
		resultData.DeleteImages(filesToDelete);
		Thread t = new Thread() {
			public void run() {
				try {
					if (!resultPanel.getName().equals(SIM))
						similarPane.addGroupedImages(resultData.similarGroups);
					if (!resultPanel.getName().equals(DUP))
						duplicatesPane
						.addGroupedImages(resultData.duplicateGroups);
					if (!resultPanel.getName().equals(EVENT))
						eventPane.addGroupedImages(resultData.eventGroups);
					if (!resultPanel.getName().equals(LOC))
						locationPane
						.addGroupedImages(resultData.locationGroups);
					if (!resultPanel.getName().equals(TRASH))
						trashPane.addImages(resultData.trashImages);
					if (!resultPanel.getName().equals(FACE))
						facesPane.addImages(resultData.faceImages);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
		t.start();
		refreshResultPanel();
		DefaultTableModel model = (DefaultTableModel) foldersTable.getModel();

		Vector data = model.getDataVector();
		for (int i = 0; i < data.size(); i++) {
			Vector obj = (Vector) data.elementAt(i);
			File file = new File((String) obj.elementAt(0));
			int[] imAndFol = getImagesAndFolders(file);
			model.setValueAt(imAndFol[1], i, 1);
			model.setValueAt(imAndFol[0], i, 2);
		}
	}

	@Override
	public void exp_finishedCopying(int success, int failed, String[] msg) {
		String buildmsg = "Copied " + success + " of " + (success + failed)
				+ "";
		updateInfoMsg(IV.IM_COPIED, resultPanel.getName(), null, buildmsg);
	}

	@Override
	public void exp_finishedMoving(int success, int failed, String[] msg) {
		String buildmsg = "Moved " + success + " of " + (success + failed) + "";
		updateInfoMsg(IV.IM_MOVED, resultPanel.getName(), null, buildmsg);
	}

	@Override
	public void exp_finishedDeleting(int success, int failed, String[] msg) {
		String buildmsg = "Deleted " + success + " of " + (success + failed)
				+ "";
		updateInfoMsg(IV.IM_DELETED, resultPanel.getName(), null, buildmsg);
	}
}
