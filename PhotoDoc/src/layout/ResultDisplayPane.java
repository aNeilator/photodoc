package layout;

import image.common.IV;
import image.execution.Execute;
import image.execution.Export;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import layout.components.CustomButton;
import net.coobird.thumbnailator.Thumbnails;

@SuppressWarnings("serial")
public class ResultDisplayPane extends JScrollPane implements
image.execution.ExportListener {
	public Set<JLabel> selectedImages;
	public Set<JLabel> allImages;
	public boolean paused;
	public boolean stop;

	private Layout layout;
	private JCheckBox selectAllCheckBox;
	private JLabel selectedNumText;
	private JPanel displayPanel;
	private int type;
	private int totalLoaded;

	public ResultDisplayPane(int type, Layout layout) {
		this.type = type;
		this.layout = layout;

		initComponents();
	}

	/*
	 * called to reset the pane and restore values to start.
	 */
	public void resetPane() {
		displayPanel.removeAll();
		selectedImages = new HashSet<JLabel>();
		allImages = new HashSet<JLabel>();
		selectedNumText.setText("0 selected");
		totalLoaded = 0;
	}

	/*
	 * used to add groups of images to the frame, takes in a linked list of groups
	 * and adds each group in its own panel to the pane
	 */
	public void addGroupedImages(HashMap<Integer, HashSet<String>> groupedImages)
			throws InterruptedException {
		resetPane();
		addOptionsPanel();

		for (final Map.Entry<Integer, HashSet<String>> entry : groupedImages
				.entrySet()) {
			if (checkRunStatusStop()) {
				break;
			}

			HashSet<String> group = entry.getValue();
			getPanelForImages(group);
		}
		addLabelClickListeners();
		displayPanel.repaint();
	}

	private void addLabelClickListeners() {
		for (final JLabel thisLabel : allImages) {
			thisLabel.getMouseListeners();
//			if (thisLabel.getMouseListeners()==null && 
			
			thisLabel.addMouseListener(new MouseAdapter() {
				
				private String getPath() {
					return thisLabel.getToolTipText();
				}
				
				@Override
				public void mouseEntered(MouseEvent evt) {
					if (!selectedImages.contains(thisLabel)) {
						thisLabel.setBorder(getSelectBorder(true));
					}
				}

				@Override
				public void mouseExited(MouseEvent evt) {
					if (!selectedImages.contains(thisLabel)) {
						thisLabel.setBorder(null);
					}
				}

				public void mousePressed(MouseEvent evt) {
					if (evt.isPopupTrigger()) {
						doPop(evt);
					}
				}

				public void mouseReleased(MouseEvent evt) {
					if (evt.isPopupTrigger()) {
						doPop(evt);
					} else {

						Border red = getSelectBorder(false);

						if (evt.getClickCount() == 2) {
							openFile(getPath());
							return;
						}

						if (selectedImages.contains(thisLabel)) {
							thisLabel.setBorder(null);

							selectedImages.remove(thisLabel);
							selectAllCheckBox.setSelected(false);
						} else {
							thisLabel.setBorder(red);
							thisLabel.setForeground(IV.color_translucent_white);

							selectedImages.add(thisLabel);
							if (selectedImages.size() == allImages
									.size()) {
								selectAllCheckBox.setSelected(true);
							}
						}
						selectedNumText.setText(selectedImages.size()
								+ " selected");
					}

				}

				private void doPop(MouseEvent e) {
					PopUpDemo menu = new PopUpDemo();
					menu.show(e.getComponent(), e.getX(), e.getY());
					menu.addPopupMenuListener(new PopupMenuListener() {

						@Override
						public void popupMenuWillBecomeVisible(
								PopupMenuEvent arg0) {

						}

						@Override
						public void popupMenuWillBecomeInvisible(
								PopupMenuEvent arg0) {

						}

						@Override
						public void popupMenuCanceled(
								PopupMenuEvent arg0) {
							removeRightClickHighlight();
						}
					});
				}

				private void removeRightClickHighlight() {
					if (selectedImages.contains(thisLabel)) {
						thisLabel.setBorder(getSelectBorder(false));
					} else {
						thisLabel.setBorder(null);
					}
				}

				class PopUpDemo extends JPopupMenu {

					public PopUpDemo() {
						JMenuItem openFile, openFolder;
						openFile = new JMenuItem("Open File");
						openFolder = new JMenuItem(
								"Open Folder location");

						add(openFile);
						add(openFolder);

						openFile.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								openFile(getPath());
								removeRightClickHighlight();
							}
						});
						openFolder
						.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(
									ActionEvent e) {
								String directory = (new File(
										getPath()).getParent());
								openFile(directory);
								removeRightClickHighlight();
							}
						});
					}
				}

			});
		}
	}
	
	/*
	 * adds a set of images in one single group, ie in one panel together
	 */
	public void addImages(Set<String> images) {
		resetPane();
		addOptionsPanel();
		getPanelForImages(images);
		addLabelClickListeners();
		displayPanel.repaint();
	}

	/*
	 * initialise the components of the pane on start
	 */
	private void initComponents() {
		paused = false;
		stop = false;

		selectedNumText = new JLabel("0 Selected");
		selectedImages = new HashSet<JLabel>();
		allImages = new HashSet<JLabel>();

		displayPanel = new JPanel();
		displayPanel
		.setLayout(new BoxLayout(displayPanel, BoxLayout.PAGE_AXIS));
		displayPanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 10, 10));
		displayPanel.setBackground(IV.color_background_lighter);

		setViewportView(displayPanel);
		getVerticalScrollBar().setUnitIncrement(16);
		getHorizontalScrollBar().setUnitIncrement(16);
		setBorder(null);
	}

	/*
	 * returns a newly created JPanel set up for images to be added
	 */
	private JPanel createNewPanelForGroup() {
		WrapLayout panelLayout = new WrapLayout(FlowLayout.LEADING);
		JPanel newPanel = new JPanel(panelLayout);
		newPanel.setBackground(null);
		newPanel.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
				} else {
					selectImageGroupOnly((JPanel) e.getSource());
				}
			}

			private void selectImageGroupOnly(JPanel row) {
				boolean allSelected = true;
				for (Component item : ((JPanel) row).getComponents()) {
					if (selectedImages.contains(item)) {
					} else {
						allSelected = false;
						break;
					}
				}
				if (allSelected) {
					for (Component item : ((JPanel) row).getComponents()) {
						JLabel image = (JLabel) item;
						selectedImages.remove(image);
						image.setBorder(null);
					}
				} else {
					for (Component item : ((JPanel) row).getComponents()) {
						JLabel image = (JLabel) item;
						selectedImages.add(image);

						Border red = getSelectBorder(false);
						image.setBorder(red);
					}
				}
				selectedNumText.setText(selectedImages.size() + " selected");
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
			}

			@Override
			public void mouseEntered(MouseEvent evt) {
				((Component) evt.getSource())
				.setBackground(IV.color_darker_background);
			}

			@Override
			public void mouseExited(MouseEvent evt) {
				((Component) evt.getSource()).setBackground(null);
			}

			@Override
			public void mouseClicked(MouseEvent arg0) {
			}
		});

		return newPanel;
	}

	/*
	 * given a set of images creates a new JPanel for them and adds it to the pane
	 */
	private void getPanelForImages(final Set<String> images) {
		final int runWas = Execute.runNumber;
		BufferedImage myPicture;

		JPanel newPanel = createNewPanelForGroup();
		displayPanel.add(newPanel, Component.TOP_ALIGNMENT);

		Iterator<String> imageIterate = images.iterator();
		while (imageIterate.hasNext()) {
			if (checkRunStatusStop()) {
				break;
			}

			final String image = imageIterate.next();
//			if (!(new File(image).exists())) {
//				  // file is not exist
//				continue;
//			}
			layout.updateInfoMsg(IV.IM_LOADED, IV.getTypeStr(type),
					new int[] { ++totalLoaded }, null);

			try {

				myPicture = ImageIO.read(new File(image));

				double w = myPicture.getWidth();
				double ratio = w / myPicture.getHeight();
				double h = IV.THUMB_SIZE;
				w = (int) (h * ratio);
				// Extreme PORTRAIT
				if (w < h * 0.75) {
					h /= ratio;
					w = IV.THUMB_SIZE;
				}
				double ww = w;
				// Extreme LANDSCAPE
				if (w > h * 1.5) {
					ww = IV.THUMB_SIZE * 1.5;
				}

				myPicture = Thumbnails.of(myPicture).size((int) w, (int) h)
						.asBufferedImage();
				JLabel picLabel = new JLabel(new ImageIcon(myPicture));
				picLabel.setHorizontalAlignment(SwingConstants.CENTER);
				picLabel.setVerticalAlignment(SwingConstants.CENTER);
				picLabel.setPreferredSize(new Dimension((int) ww, IV.THUMB_SIZE));

				if (runWas == Execute.runNumber) {
					picLabel.setToolTipText(image);
					newPanel.add(picLabel);
					allImages.add(picLabel);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * adds the options panel to the top of the pane
	 * this is then panel containing select options and save/save all/delete
	 * is only added once
	 */
	private void addOptionsPanel() {
		JPanel parentPanel = new JPanel(new GridLayout(1, 2));
		JPanel infoPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
		JPanel optionsPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING));

		JButton invertButton = new JButton();
		CustomButton deleteButton = new CustomButton();
		CustomButton exportButton = new CustomButton();
		CustomButton saveButton = new CustomButton();
		selectAllCheckBox = new JCheckBox();

		parentPanel.setName("OptionsPanel");

		selectAllCheckBox.setText("Select All");
		invertButton.setText("Invert Selection");

		layout.setIcon(saveButton, "save.png", 30);
		layout.setIcon(exportButton, "save-all.png", 28);
		layout.setIcon(deleteButton, "delete.png", 30);

		saveButton.setToolTipText("Save Selected Files at...");
		exportButton.setToolTipText("Save Selected Groups at...");
		deleteButton.setToolTipText("Delete Selected");
		saveButton.setAlignmentY(SwingConstants.RIGHT);

		selectAllCheckBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				selectAllCheckBoxActionPerformed(evt);
			}
		});

		invertButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				invertButtonActionPerformed(evt);
			}
		});

		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				deleteButtonActionPerformed(evt);
			}
		});

		exportButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				exportButtonActionPerformed(evt);
			}
		});

		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				saveButtonActionPerformed(evt);
			}
		});

		infoPanel.add(selectedNumText);
		infoPanel.add(selectAllCheckBox);
		infoPanel.add(invertButton);

		optionsPanel.add(saveButton);
		if (type != IV.TRASH && type != IV.GROUP_FACE) {
			optionsPanel.add(exportButton);
		}
		optionsPanel.add(deleteButton);

		parentPanel.add(infoPanel);
		parentPanel.add(optionsPanel);

		selectAllCheckBox.setBackground(null);
		saveButton.setBackground(null);
		exportButton.setBackground(null);
		deleteButton.setBackground(null);
		parentPanel.setBackground(null);
		infoPanel.setBackground(null);
		optionsPanel.setBackground(null);

		displayPanel.add(parentPanel);
	}

	/*
	 * saves groups as folders in the selected output directory
	 * ie each group is in their own folder
	 */
	private void saveGroupsAsFolders(String outputDirectory,
			ArrayList<ArrayList<String>> groups) {
		String mainFolder = getFolderPath(outputDirectory, true);

		if (groups.size() == 1) {
			carryExport(IV.IM_COPIED, groups.get(0), mainFolder);
		} else {
			int groupCount = 1;
			for (ArrayList<String> group : groups) {
				String groupFolder = mainFolder + "group_"
						+ String.valueOf(groupCount);
				carryExport(IV.IM_COPIED, group, groupFolder);
				groupCount++;
			}
		}
	}

	/*
	 * opens the current image in the computers default image viewer
	 */
	private void openFile(String image) {
		try {
			Desktop.getDesktop().open(new File(image));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * used to check if the painting of images should be pause/resumed or stopped
	 */
	private boolean checkRunStatusStop() {
		while (paused) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			if (stop) {
				break;
			}
		}

		return (stop ? true : false);
	}

	/*
	 * returns a border used for if an image is hovered over or selected
	 */
	private Border getSelectBorder(boolean highlightOnly) {
		Color color = Color.RED;
		int width = (highlightOnly ? 3 : 5);

		switch (type) {
		case IV.SIMILAR:
			color = Layout.color_Sim;
			break;
		case IV.DUPLICATE:
			color = Layout.color_Dup;
			break;
		case IV.TRASH:
			color = Layout.color_Trash;
			break;
		case IV.GROUP_FACE:
			color = Layout.color_Face;
			break;
		case IV.GROUP_EVENT:
			color = Layout.color_Event;
			break;
		case IV.GROUP_LOC:
			color = Layout.color_Loc;
			break;
		}

		return BorderFactory.createLineBorder(color, width);
	}

	/*
	 * returns a string representation of the output directory combined with the date and the
	 * type of images being saved in the directory
	 */
	private String getFolderPath(String outputDirectory, boolean useType) {
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
		.format(new Date());
		date = date.replace(" ", "_");
		date = date.replace(":", "-");
		String info = (useType ? Export.getFolderName(type) + "_" : "Results_");

		return outputDirectory + File.separator + "PhotoDoc_" + info + date
				+ File.separator;
	}

	/*
	 * converts a set of strings to an arraylist of strings
	 */
	private ArrayList<String> getArraylistOfPathsFromLabelSet(Set<JLabel> theSet) {
		ArrayList<String> array = new ArrayList<String>();

		for (JLabel item : theSet) {
			String file = item.getToolTipText();
			array.add(file);
		}
		return array;
	}

	/*
	 * returns groups containing only selected images within each group
	 */
	private ArrayList<ArrayList<String>> getGroups() {
		ArrayList<ArrayList<String>> imageGroups = new ArrayList<ArrayList<String>>();

		Component[] children = displayPanel.getComponents();

		for (Component child : children) {
			if (child.getName() == "OptionsPanel") {
				// ignore this panel, no images here
			} else {
				ArrayList<String> temp = new ArrayList<String>();
				boolean newGroup=false;
				JPanel tmp = (JPanel) child;
				for (Component item : tmp.getComponents()) {
					JLabel image = (JLabel) item;
					if (selectedImages.contains(image)) {
						temp.add(image.getToolTipText());
						newGroup=true;
					}
				}
				if (newGroup) {
					imageGroups.add(temp);
				}
			}
		}

		return imageGroups;
	}

	/*
	 * exports the given images depending upon the type given
	 */
	private void carryExport(int type, ArrayList<String> list, String directory) {
		switch (type) {
		case IV.IM_COPIED:
			layout.updateInfoMsg(IV.IM_COPIED, IV.getTypeStr(type), null,
					"Copying...");
			Export.Copy(list, directory, this);
			break;
		case IV.IM_MOVED:
			layout.updateInfoMsg(IV.IM_MOVED, IV.getTypeStr(type), null,
					"Moving...");
			Export.Move(list, directory, this);
			break;
		case IV.IM_DELETED:
			layout.updateInfoMsg(IV.IM_DELETED, IV.getTypeStr(type), null,
					"Deleting...");
			Export.Delete(list, this);
			break;
		}
	}

	/*
	 * when select all is activated, all images selected, when deactivated all deselected
	 */
	private void selectAllCheckBoxActionPerformed(ActionEvent evt) {
		if (selectAllCheckBox.isSelected()) {
			selectedImages.addAll(allImages);

			for (JLabel label : selectedImages) {
				Border red = getSelectBorder(false);
				label.setBorder(red);
			}
		} else {
			selectedImages = new HashSet<JLabel>();

			for (JLabel label : allImages) {
				label.setBorder(null);
			}
		}

		for (JLabel label : allImages) {
			Dimension prev = label.getSize();
			prev.height = 30;
			label.repaint();
		}
		selectedNumText.setText(selectedImages.size() + " selected");
	}

	/*
	 * inverts the selected images, ie all selected become deselected,
	 * all deselected become selected
	 */
	private void invertButtonActionPerformed(ActionEvent evt) {
		for (JLabel label : selectedImages) {
			label.setBorder(null);
		}

		Set<JLabel> symmetricDiff = new HashSet<JLabel>(allImages);
		symmetricDiff.addAll(selectedImages);

		Set<JLabel> tmp = new HashSet<JLabel>(allImages);
		tmp.retainAll(selectedImages);

		symmetricDiff.removeAll(tmp);
		selectedImages = symmetricDiff;
		selectedNumText.setText(selectedImages.size() + " selected");

		for (JLabel label : selectedImages) {
			Border red = getSelectBorder(false);
			label.setBorder(red);
		}
	}

	/*
	 * deletes the currently selected images within the pane
	 */
	private void deleteButtonActionPerformed(ActionEvent evt) {
		int dialogResult = JOptionPane.showConfirmDialog(this,
				"Are you sure you want to delete " + selectedImages.size()
				+ " file(s)?", "Confirm Delete",
				JOptionPane.YES_NO_OPTION);
		if (dialogResult == 0) {
			try {

				ArrayList<String> filesToDelete = getArraylistOfPathsFromLabelSet(selectedImages);
				carryExport(IV.IM_DELETED, filesToDelete, null);
				layout.updateResults(filesToDelete);

				/*Iterator<JLabel> selected = selectedImages.iterator();
				while (selected.hasNext()) {
					JLabel item = selected.next();
					for (MouseListener listener : item.getMouseListeners()) {
						item.removeMouseListener(listener);
					}
					Container parent = item.getParent();
					parent.remove(item);

					selected.remove();
					selectedNumText
					.setText(selectedImages.size() + " selected");
					allImages.remove(item);

					parent.repaint();
				}*/
			} catch (Exception e) {
				exp_finishedDeleting(0, 0, new String[] { "Deletion Error" });
			}
		}
	}

	/*
	 * updates paths for label images.
	 */
	public void updatePathToImages(String path)
		{
			for (JLabel image : allImages) {
				File imageFile = new File(image.getToolTipText());
				String name = imageFile.getName();
	
				String newPath = path + File.separator + name;
				image.setToolTipText(newPath);
			}
		}
	
	
	
	/*
	 * creates a JFileChooser and allows the user to save the selected groups of images to a folder
	 */
	private void exportButtonActionPerformed(ActionEvent evt) {
		this.setEnabled(false);
		final JFileChooser fileDialog = new JFileChooser();
		fileDialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileDialog.setDialogTitle("Select Group save location");

		int returnVal = fileDialog.showSaveDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			final File outputDirectory = fileDialog.getSelectedFile();
			saveGroupsAsFolders(outputDirectory.getAbsolutePath(),
					getGroups());
		}
	}

	/*
	 * saves the selected images to a folder selected from a JFileChooser
	 */
	private void saveButtonActionPerformed(ActionEvent evt) {
		final JFileChooser fileDialog = new JFileChooser();
		fileDialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileDialog.setDialogTitle("Select save location");

		int returnVal = fileDialog.showSaveDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			final File file = fileDialog.getSelectedFile();
			ArrayList<String> filesToCopy = getArraylistOfPathsFromLabelSet(selectedImages);
			carryExport(IV.IM_COPIED, filesToCopy, file.getAbsolutePath());
		}
	}

	@Override
	public void exp_finishedCopying(int success, int failed, String[] msg) {
		String buildmsg = "Copied " + success + " of " + (success + failed)
				+ "";
		layout.updateInfoMsg(IV.IM_COPIED, IV.getTypeStr(type), null, buildmsg);
	}

	@Override
	public void exp_finishedMoving(int success, int failed, String[] msg) {
		String buildmsg = "Moved " + success + " of " + (success + failed) + "";
		layout.updateInfoMsg(IV.IM_MOVED, IV.getTypeStr(type), null, buildmsg);
	}

	@Override
	public void exp_finishedDeleting(int success, int failed, String[] msg) {
		String buildmsg = null;
		if (msg == null) {
			buildmsg = "Deleted " + success + " of " + (success + failed) + "";
		} else {
			buildmsg = msg[0];
		}
		
		Iterator<JLabel> selected = selectedImages.iterator();
		while (selected.hasNext()) {
			JLabel item = selected.next();
			for (MouseListener listener : item.getMouseListeners()) {
				item.removeMouseListener(listener);
			}
			Container parent = item.getParent();
			parent.remove(item);

			selected.remove();
			selectedNumText
			.setText(selectedImages.size() + " selected");
			allImages.remove(item);

			parent.repaint();
		}
		
		layout.updateInfoMsg(IV.IM_DELETED, IV.getTypeStr(type), null, buildmsg);
	}
}
