package layout;


public interface PhDocListener
{

	public void progressUpdate(final double progOpenCV);
	
	public void finishedSimilarSearch();
	public void finishedDuplicateSearch();
	public void finishedEventTimeSearch();
	public void finishedLocationSearch();
	public void finishedTrashSearch();
	public void finishedFacesSearch();
	
	public void finished_All();
	
	/**
	 * Export callbacks.
	 */
	public void exp_finishedMoving();
	public void exp_finishedDeleting();
	public void exp_imageNotFound(String image);

}
