package layout;

import image.common.IV;
import image.common.ImageDetails;
import image.common.UserParameters;
import image.execution.Execute;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.Timer;
import javax.swing.UIManager;

@SuppressWarnings("serial")
public class Layout_Dev extends JApplet implements PhDocListener
{

	private PhDocListener listener;
	private static Timer timer;

	private static JFrame mainFrame;
	private static JPanel controlPanel;
	private static JPanel duplicatePanel;
	private static JPanel similarityPanel;
	private static JPanel trashPanel;

	private static JLabel status1Label;
	private static JLabel status2Label;
	private static JLabel dupStatus;
	private static JLabel simStatus;
	private static JLabel trashStatus;

	private static String chosenFolder;
	private static int imageCount;
	private static int imageTotal;

	private static JProgressBar progressBar;
	private JScrollPane simScroll;
	private FlowLayout fl;
	private GridBagConstraints cGrid;
	private JTabbedPane tabbedPane;
	private JComponent[] panels_tab;
	private UserParameters userP;
	private JTextField textTrashT;
	private JTextField textSimT;
	private JButton runButton;
	private Execute execute;

	public static void main(String[] args)
	{
		final Layout_Dev lay = new Layout_Dev();
		lay.init();
//		lay.sendExecute();
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run()
			{
				lay.prepareGUI();
			}
		});

	}

	public void init()
	{
		listener = this;
		userP = new UserParameters();
	}

	public void sendExecute()
	{
		if (userP.folders == null) {
//			userParam = new UserParameters();
			userP.folders = new File[1];
			userP.folders[0] = new File("C:/Users/Snehil/Dropbox/Family Share/Pics/Neil bday");
//			userP.folders[0] = new File("C:/Users/Snehil/Documents/Digital Art/Collection");
//			userP.folders[0] = new File("C:/Users/Snehil/Dropbox/Camera Uploads");
//			userP.folders[0] = new File(IV.RES);
		}
		try {
			userP.t_Similar = Double.parseDouble(textSimT.getText());
			userP.t_Trash = Double.parseDouble(textTrashT.getText());
		} catch (Exception e) {}

		execute = new Execute(listener,userP);
		Thread t = new Thread() {
			public void run()
			{
				if (runButton != null) runButton.setText("Pause");
				execute.run();
			}
		};
		t.start();
	}

	private void setUpLabels()
	{
		status1Label = new JLabel("",JLabel.LEFT);
		status2Label = new JLabel(" ",JLabel.LEFT);
		dupStatus = new JLabel(" ",JLabel.LEFT);
		simStatus = new JLabel(" ",JLabel.LEFT);
		trashStatus = new JLabel(" ",JLabel.LEFT);

		status1Label.setSize(350,50);
		status2Label.setSize(350,50);
		dupStatus.setSize(350,50);
		simStatus.setSize(350,100);
		trashStatus.setSize(350,100);
	}

	private void setupPanels()
	{
		fl = new FlowLayout();
		fl.setAlignment(FlowLayout.LEFT);
		fl.setHgap(5);

		controlPanel = new JPanel();
		controlPanel.setLayout(fl);

		duplicatePanel = new JPanel();
		duplicatePanel.setLayout(fl);

		trashPanel = new JPanel();
		trashPanel.setLayout(fl);

		tabbedPane = new JTabbedPane();
		tabbedPane.setMinimumSize(new Dimension(1336,900));
		tabbedPane.setTabPlacement(JTabbedPane.RIGHT);

		String[] tabName = { "Similar", "Duplicate", "Trash", "Face", "Event", "Location" };
		panels_tab = new JComponent[6];
		for (int i = 0; i < 6; i++) {
			panels_tab[i] = makeTextPanel("Panel #" + i);
			JScrollPane scroll = new JScrollPane(panels_tab[i]);
			scroll.setPreferredSize(new Dimension(1330,900));
			tabbedPane.addTab(tabName[i],scroll);
			//		tabbedPane.setMnemonicAt(0,KeyEvent.VK_1);
		}
	}

	protected JComponent makeTextPanel(String text)
	{
		JPanel panel = new JPanel(false);
		panel.setAutoscrolls(true);
//		JLabel filler = new JLabel(text);
//		filler.setHorizontalAlignment(JLabel.CENTER);
		panel.setLayout(new BoxLayout(panel,BoxLayout.Y_AXIS));
//		panel.add(filler);
		return panel;
	}

	private JButton createRunButton()
	{
		JButton butt = new JButton("Run");
		butt.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e)
			{
				panels_tab[0].removeAll();
				panels_tab[1].removeAll();
				panels_tab[2].removeAll();
				panels_tab[3].removeAll();
				panels_tab[4].removeAll();
				panels_tab[5].removeAll();

//				cGrid.gridy = 1;
//				mainFrame.add(tabbedPane,cGrid);
				if (runButton.getText().equals("Pause")) {
					execute.pause();
					runButton.setText("Resume");
				}
				else if (runButton.getText().equals("Resume")) {
					execute.resume();
					runButton.setText("Pause");
				}
				else {
					if (chosenFolder != null) {
						// @todo: run 3 threads, runSim, runDup, runTrash
						String folderPath = chosenFolder;
						File folder = new File(folderPath);

						userP.folders = new File[1];
						userP.folders[0] = folder;
						sendExecute();
						//-----------------------

						imageTotal = IV.getImageFilesList(folder).size();
						status2Label.setText("");
						progressBar.setVisible(true);

						timer = new Timer(100,new ActionListener() {
							public void actionPerformed(ActionEvent evt)
							{
								progressBar.setValue((imageCount / imageTotal) * 100);

								if (imageCount == imageTotal) {
									Toolkit.getDefaultToolkit().beep();
									timer.stop();
									progressBar.setVisible(false);
									progressBar.setValue(progressBar.getMinimum());
								}
							}
						});

					} else {
						status1Label.setText("please choose a folder before running.");
						sendExecute();
					}
				}
			}
		});

		return butt;
	}

	private void prepareGUI()
	{
		try {
			UIManager.setLookAndFeel(
					UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {}

		setUpLabels();
		setupPanels();

		mainFrame = new JFrame("PhotoDoc - image management software");
		mainFrame.setMinimumSize(new Dimension(668,1000));
		mainFrame.setPreferredSize(new Dimension(668,1000));
		mainFrame.setLayout(new GridBagLayout());
		mainFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent)
			{
				System.out.println("exiting the program...");
				System.exit(0);
			}
		});
//		mainFrame.sc

		progressBar = new JProgressBar(0,100);
		progressBar.setValue(0);
		progressBar.setStringPainted(false);
		progressBar.setVisible(false);
		progressBar.setBackground(Color.CYAN);

		JScrollPane dupScroll = new JScrollPane(duplicatePanel);
		dupScroll.setPreferredSize(new Dimension(450,130));
		dupScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		dupScroll.getHorizontalScrollBar().setUnitIncrement(20);

		similarityPanel = new JPanel();
		similarityPanel.setLayout(fl);
		simScroll = new JScrollPane(similarityPanel);
		simScroll.setPreferredSize(new Dimension(450,130));
		simScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		simScroll.getHorizontalScrollBar().setUnitIncrement(20);
		simScroll.setBackground(Color.RED);

		JScrollPane trashScroll = new JScrollPane(trashPanel);
		trashScroll.setPreferredSize(new Dimension(450,130));
		trashScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		trashScroll.getHorizontalScrollBar().setUnitIncrement(20);

		runButton = createRunButton();

		cGrid = new GridBagConstraints();
		cGrid.fill = GridBagConstraints.HORIZONTAL;
		cGrid.anchor = GridBagConstraints.NORTHWEST;
		cGrid.weightx = 0.5;
		cGrid.weighty = 0;
		cGrid.gridx = 0;
		cGrid.gridy = 0;

		mainFrame.add(controlPanel,cGrid);

		cGrid.gridy = 1;
		mainFrame.add(tabbedPane,cGrid);

		/*	c.gridy = 2;
			mainFrame.add(similarityPanel,c);

			c.gridy = 3;
			mainFrame.add(dupScroll,c);

			c.gridy = 4;
			mainFrame.add(trashStatus,c);

			c.weighty = 1;
			c.gridy = 5;
			mainFrame.add(trashScroll,c);
		*/
		controlPanel.add(runButton);
		showFileChooser();
//		controlPanel.add(status1Label);
//		controlPanel.add(status2Label);
		controlPanel.add(new JLabel("sim :",JLabel.LEFT));
		textSimT = new JTextField(3);
		textSimT.setText(String.valueOf(userP.t_Similar));
		controlPanel.add(textSimT);
		controlPanel.add(new JLabel("trash :",JLabel.LEFT));

		textTrashT = new JTextField(3);
		textTrashT.setText(String.valueOf(userP.t_Trash));
		/*		textTrashT.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e)
					{
						userP.t_Trash_hist = Double.parseDouble(textSimT.getText());
					}
				});
		*/controlPanel.add(textTrashT);

		JCheckBox b = new JCheckBox("Transitive Grouping");
		b.setSelected(true);
		b.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e)
			{
				System.out.println(e.getStateChange() == ItemEvent.SELECTED
						? "transitive on" : "trasitive off");
				if (e.getStateChange() == ItemEvent.SELECTED) {
					userP.use_transitiveGrouping = true;
				}
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					userP.use_transitiveGrouping = false;
				}
			}
		});

		JCheckBox mu = new JCheckBox("MultiThreading");
		mu.setSelected(true);
		mu.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e)
			{
				System.out.println(e.getStateChange() == ItemEvent.SELECTED
						? "multithreading set" : "singleThreadin set");
				if (e.getStateChange() == ItemEvent.SELECTED) {
					userP.use_multiThreading = true;
				}
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					userP.use_multiThreading = false;
				}
			}
		});

		JCheckBox feat = new JCheckBox("Do Feature");
		feat.setSelected(userP.doSimilar_feature);
		feat.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e)
			{
				System.out.println(e.getStateChange() == ItemEvent.SELECTED
						? "feature set" : "feature not set");
				if (e.getStateChange() == ItemEvent.SELECTED) {
					userP.doSimilar_feature = true;
				}
				if (e.getStateChange() == ItemEvent.DESELECTED) {
					userP.doSimilar_feature = false;
				}
			}
		});

		controlPanel.add(b);
		controlPanel.add(mu);
		controlPanel.add(feat);
		mainFrame.setVisible(true);
	}

	private void showFileChooser()
	{

		final JFileChooser fileDialog = new JFileChooser();
		fileDialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		JButton showFileDialogButton = new JButton("Open File");

		showFileDialogButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				int returnVal = fileDialog.showOpenDialog(mainFrame);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					java.io.File file = fileDialog.getSelectedFile();
					chosenFolder = file.getAbsolutePath();
					status1Label.setText("");
				}
			}
		});

		controlPanel.add(showFileDialogButton);
	}

	//----------------------------------callback methods------------------------------------------

	//----------------------------------interface------------------------------------------

	public void progressUpdate(final double progress)
	{
//		Thread t = new Thread() {
//			public void run()
//			{
		//access using Execute.progress.progPercent;
//				return;
//			}
//		};
//		t.start();

	}

	@Override
	public void finished_All()
	{
//		runButton.setEnabled(true);
		runButton.setText("Run");
	}

	@Override
	public void finishedFacesSearch()
	{
		try {

			JPanel pan = new JPanel();
			pan.setLayout(fl);
//			pan.setPreferredSize(new Dimension(450,500));

			System.out.println("Populating Faces........");
			JScrollPane scroll = new JScrollPane(pan,
					JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
					JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scroll.setMinimumSize(new Dimension(1330,500));
			for (Map.Entry<String, ImageDetails> entry : Execute.imgDetails.entrySet()) {
				String name = entry.getKey();

				if (entry.getValue().info_Faces > 0) {
					JLabel picLabel = getImageLabel(name);
					pan.add(picLabel);

				}
			}
			System.out.println();
			cGrid.gridy++;
			panels_tab[3].add(scroll,cGrid);
		} catch (Exception e) {
			System.err.println("Error");
			e.printStackTrace();
		}

	}

	@Override
	public void finishedTrashSearch()
	{
		try {

			JPanel pan = new JPanel();
			pan.setLayout(fl);
//			pan.setPreferredSize(new Dimension(1350,500));

			System.out.println("Populating Trash...");
			JScrollPane scroll = new JScrollPane(pan,
					JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
					JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			scroll.setMinimumSize(new Dimension(1330,500));
			for (Map.Entry<String, ImageDetails> entry : Execute.imgDetails.entrySet()) {
				String name = entry.getKey();

				if (entry.getValue().cert_Trash > IV.CRThresh_High) {
					JLabel picLabel = getImageLabel(name);
					pan.add(picLabel);

				}
			}
			System.out.println();
			cGrid.gridy++;
			panels_tab[2].add(scroll,cGrid);
		} catch (Exception e) {
			System.err.println("Error");
			e.printStackTrace();
		}

	}

	@Override
	public void finishedSimilarSearch()
	{
		System.out.println("Populating Similar...");

		HashMap<Integer, HashSet<String>> groupMembers = getGroupAsMap(Execute.imgDetails,IV.SIMILAR);

		addImgToUIRow(groupMembers,panels_tab[0]);
	}

	@Override
	public void finishedDuplicateSearch()
	{
		System.out.println("Populating Duplicate...");

		HashMap<Integer, HashSet<String>> groupMembers = getGroupAsMap(Execute.imgDetails,IV.DUPLICATE);

		addImgToUIRow(groupMembers,panels_tab[1]);
	}

	@Override
	public void finishedEventTimeSearch()
	{
		System.out.println("Populating EventTime...");

		HashMap<Integer, HashSet<String>> groupMembers = getGroupAsMap(Execute.imgDetails,IV.GROUP_EVENT);

		addImgToUIRow(groupMembers,panels_tab[4]);

	}

	@Override
	public void finishedLocationSearch()
	{
		System.out.println("Populating Location...");
		HashMap<Integer, HashSet<String>> groupMembers = getGroupAsMap(Execute.imgDetails,IV.GROUP_LOC);
		addImgToUIRow(groupMembers,panels_tab[5]);
	}

	@Override
	public void exp_finishedMoving()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void exp_finishedDeleting()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void exp_imageNotFound(String image)
	{
		// TODO Auto-generated method stub

	}

	//----------------------------------UI Automatic------------------------------------------

	private HashMap<Integer, HashSet<String>> getGroupAsMap(Map<String, ImageDetails> imDetails, final int type)
	{
		HashMap<Integer, HashSet<String>> groupMembers = new HashMap<Integer, HashSet<String>>();

		for (Map.Entry<String, ImageDetails> entry : imDetails.entrySet()) {
			int imgGroup = entry.getValue().group_Sim;
			switch (type) {
			case IV.DUPLICATE:
				imgGroup = entry.getValue().group_Dup;
				break;
			case IV.SIMILAR:
				imgGroup = entry.getValue().group_Sim;
				break;
			case IV.GROUP_EVENT:
				imgGroup = entry.getValue().group_Event;
				break;
			case IV.GROUP_LOC:
				imgGroup = entry.getValue().group_Loc;
				break;
			}

			String fileName = entry.getKey();
			if (imgGroup != -1) {//&& entry.getValue().cert_similar>0.6) {
				if (groupMembers.containsKey(imgGroup)) {
					groupMembers.get(imgGroup).add(fileName);
				} else {
					HashSet<String> nameSet = new HashSet<String>();
					nameSet.add(fileName);
					groupMembers.put(imgGroup,nameSet);
				}
			}
		}
		return groupMembers;
	}

	private void addImgToUIRow(HashMap<Integer, HashSet<String>> groupMembers, JComponent panel12)
	{
		try {
			for (final Map.Entry<Integer, HashSet<String>> entry : groupMembers.entrySet()) {
				final JPanel pan = new JPanel();
				pan.setLayout(fl);

				Thread t = new Thread() {
					public void run()
					{
//						System.out.println();
						for (String name : entry.getValue()) {

							JLabel picLabel = getImageLabel(name);
							picLabel.setToolTipText((new File(name)).getName());

//								(new ImageRetriever(picLabel,imageFiles.get(name))).execute();

//							System.out.print("|");
							pan.add(picLabel);
						}
					}
				};
				t.start();

				cGrid.gridy++;
				panel12.add(pan);
//				System.out.println();
				System.gc();
			}
		} catch (Exception e) {
			System.err.println("Error");
			e.printStackTrace();
		}
	}

	private JLabel getImageLabel(String name)
	{
		ImageIcon imageIcon = new ImageIcon(name); // load the image to a imageIcon
		double w = imageIcon.getIconWidth(), h = imageIcon.getIconHeight();
		double ratio = w / h;
		h = 50;
		w = (int) (h * ratio);

		// scale it the smooth way 
		imageIcon = new ImageIcon(imageIcon.getImage().getScaledInstance(
				(int) w,
				(int) h,
				java.awt.Image.SCALE_FAST));
		return new JLabel(imageIcon);

//		imageIcon=null;
//		newimg=null;

	}

	public class ImageRetriever extends SwingWorker<Icon, Void>
	{

		public ImageRetriever(JLabel lblImage, String strImageUrl) {
			this.strImageUrl = strImageUrl;
			this.lblImage = lblImage;
		}

		@Override
		protected Icon doInBackground() throws Exception
		{
			Icon icon = retrieveImage(strImageUrl);
			return icon;
		}

		private Icon retrieveImage(String strImageUrl) throws IOException
		{
			ImageIcon imageIcon = new ImageIcon(strImageUrl); // load the image to a imageIcon
			Image image = imageIcon.getImage(); // transform it 
			Image newimg = image.getScaledInstance(100,70,java.awt.Image.SCALE_FAST); // scale it the smooth way 
			imageIcon = new ImageIcon(newimg);
			return imageIcon;
		}

		@Override
		protected void done()
		{
			Icon icon = null;
			String text = null;
			try {
				icon = get();
			} catch (Exception ignore) {
				ignore.printStackTrace();
				text = "Image unavailable";
			}
			lblImage.setIcon(icon);
			lblImage.setText(text);
		}

		private String strImageUrl;
		private JLabel lblImage;
	}

}
