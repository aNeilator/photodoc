package tests;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;

import image.common.Normalize;

import org.junit.Test;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

public class NormalizeTest {

	@Test
	/**
	 * Test for a single image. Assumes one dimensions is greater than 512.
	 */
	public void normalizeImageTest() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		File file = new File("/Users/Dave/Downloads/CS407Images/Duplicated/dog.jpg");
		Mat image = Highgui.imread(file.getAbsolutePath());
		Mat normImage = Normalize.normalize(file);
		
		int origWidth = image.width();
		int origHeight = image.height();
		int normWidth = normImage.width();
		int normHeight = normImage.height();
		
		if(origWidth > origHeight) {
			assertEquals(normWidth,512,0);
			assertEquals(normHeight,origHeight/(origWidth/normWidth),0);
		}
		else {
			assertEquals(normWidth,512,0);
			assertEquals(normHeight,origHeight/(origWidth/normWidth),0);
		}
	}
	
	@Test
	/**
	 * Test for an image smaller than 512x512.
	 */
	public void normalizeImageSmallTest() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		File file = new File("/Users/Dave/Downloads/CS407Images/Duplicated/test.jpg");
		Mat image = Highgui.imread(file.getAbsolutePath());
		Mat normImage = Normalize.normalize(file);
		
		assertEquals(image.width(), normImage.width(), 0);
		assertEquals(image.height(), normImage.height(), 0);
	}
	
	
	@Test
	/**
	 * Test for a list of images.
	 */
	public void normalizeImagesTest() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		ArrayList<File> images = new ArrayList<File>();
		images.add(new File("/Users/Dave/Downloads/CS407Images/Duplicated/dog.jpg"));
		Mat image = Highgui.imread(images.get(0).getAbsolutePath());
		
		ArrayList<Mat> normImages = Normalize.normalize(images);
		
		for(int i=0; i < normImages.size(); i++) {
			int origWidth = image.width();
			int origHeight = image.height();
			int normWidth = normImages.get(i).width();
			int normHeight = normImages.get(i).height();
			
			if(origWidth > origHeight) {
				assertEquals(normWidth,512,0);
				assertEquals(normHeight,origHeight/(origWidth/normWidth),0);
			}
			else {
				assertEquals(normWidth,512,0);
				assertEquals(normHeight,origHeight/(origWidth/normWidth),0);
			}
		}
	}
	
	@Test
	/**
	 * Test for a single image.
	 */
	public void desaturateTest() {
		File file = new File("/Users/Dave/Downloads/CS407Images/Duplicated/test.jpg");
		Mat greyImage = Normalize.desaturate(file);	
		assertEquals(greyImage.channels(), 1, 0);
	}
	
	@Test
	/**
	 * Test for a list of images.
	 */
	public void desaturateListTest() {
		File file = new File("/Users/Dave/Downloads/CS407Images/Duplicated/test.jpg");
		ArrayList<File> list = new ArrayList<File>();
		list.add(file);
		ArrayList<Mat> greyImages = Normalize.desaturate(list);
		for(int i=0; i < greyImages.size(); i++) {
			assertEquals(greyImages.get(i).channels(), 1, 0);
		}
	}
	

}
