package tests;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;

import image.execution.Export;

import org.junit.Test;

public class ExportTest {

	@Test
	public void deleteTest() {
		ArrayList<String> fileList = new ArrayList<String>();
		fileList.add("/Users/Dave/Downloads/CS407Images/Duplicated/white1x1.jpg");
		Export.Delete(fileList, null);
		File file = new File(fileList.get(0));
		assertFalse(file.exists());
	}
	
	@Test
	public void moveTest() {
		ArrayList<String> fileList = new ArrayList<String>();
		fileList.add("/Users/Dave/Downloads/CS407Images/Duplicated/biased.jpg");
		Export.Move(fileList, "/Users/Dave/Downloads/", null);
		File file = new File(fileList.get(0));
		assertFalse(file.exists());
		assertTrue(new File("/Users/Dave/Downloads/biased.jpg").exists());
	}

}
