package tests;

import static org.junit.Assert.*;
import image.execution.ExecuteListener;
import image.operation.Histogram;

import org.junit.Test;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class HistogramTest {
	
	@Test
	public void calcStdDevTest() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		Mat hist = new Mat(2,1,CvType.CV_32FC1);
		hist.put(0, 0, 15.0);
		hist.put(1, 0, 60.0);
		assertEquals(0.4, Histogram.calcStdDev(hist), 0);
	}
	
	@Test
	public void calcVarianceTest() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		Mat hist = new Mat(2,1,CvType.CV_32FC1);
		hist.put(0, 0, 15.0);
		hist.put(1, 0, 60.0);
		assertEquals(0.16, Histogram.calcVariance(hist), 0);
	}
	
	@Test
	public void calcMeanTest() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		Mat hist = new Mat(2,1,CvType.CV_32FC1);
		hist.put(0, 0, 15.0);
		hist.put(1, 0, 60.0);
		assertEquals(0.8, Histogram.calcMean(hist), 0);
	}
	
	@Test
	public void biasTestSkewed() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		ExecuteListener execls = null;
		Mat imghist = Histogram.createHistogram("/Users/Dave/Downloads/CS407Images/Duplicated/white1x1.jpg", true);
		
		assertTrue(Histogram.doTrash(0, imghist, execls));
	}
	
	@Test
	public void biasTest() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		ExecuteListener execls = null;
		Mat imghist = Histogram.createHistogram("/Users/Dave/Downloads/CS407Images/Duplicated/biased.jpg", true);
		assertTrue(Histogram.doTrash(0, imghist, execls));
	}
	
	@Test
	public void biasTestNot() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		ExecuteListener execls = null;
		Mat imghist = Histogram.createHistogram("/Users/Dave/Downloads/CS407Images/Duplicated/notbiased.jpg", true);
		assertFalse(Histogram.doTrash(0, imghist, execls));
	}
	
	@Test
	public void createHistogramTest() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		String image = "/Users/Dave/Downloads/CS407Images/Duplicated/white1x1.jpg";
		Mat normHist = new Mat(256,1,CvType.CV_32FC1);
		normHist.put(255, 0, 1.0);
		for(int i = 0; i < 255; i++) {
			normHist.put(i,0,0.0);
		}
		for(int i = 0; i < 256; i++) {
			assertEquals(normHist.get(i,0)[0],Histogram.createHistogram(image,true).get(i,0)[0], 0);
		}
	}
}