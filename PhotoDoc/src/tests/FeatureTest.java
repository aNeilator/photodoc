package tests;

import static org.junit.Assert.*;
import image.operation.Feature;

import org.junit.Test;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class FeatureTest {
	
	@Test
	public void siftTest() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		Mat image = new Mat(1,1,CvType.CV_8UC1);
		image.put(0, 0, 1);
		Mat sift = Feature.sift(image);
		double height = sift.size().height;
		double width = sift.size().width;
		assertEquals(height, 0, 0);
		assertEquals(width, 0, 0);
	}
	
/* No longer doable after refactoring.
	@Test
	public void featureDetectionTest() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		
		File file1 = new File("/Users/Dave/Downloads/CS407Images/Similar/athletics1.jpg");
		File file2 = new File("/Users/Dave/Downloads/CS407Images/Similar/athletics2.jpg");
		File file3 = new File("/Users/Dave/Downloads/CS407Images/Similar/scene.jpg");
		
		ArrayList<File> images = new ArrayList<File>();
		images.add(file1);
		images.add(file2);
		images.add(file3);
		
		ArrayList<File> result = Feature.featureDetection(images);
		assertTrue(result.get(0).equals(file1));
		assertTrue(result.get(1).equals(file2));
	}
*/
}
